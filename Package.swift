// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GlanceDefaultUI",
    defaultLocalization: "en",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GlanceDefaultUI",
            targets: ["GlanceDefaultUI"]),
    ],
    dependencies: [
        .package(name: "GlanceSDK", url: "https://gitlab.com/glance-sdk-public/glance-sdk-ios-releases.git", from: "6.9.3")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "GlanceDefaultUI",
            dependencies: [
                .product(name: "GlanceSDK", package: "GlanceSDK")
            ],
            resources: [
                Resource.process("Resources/Images.xcassets"),
                Resource.process("Resources/GlanceColors.xcassets")
            ])
    ]
)
