# GlanceDefaultUI

**GlanceDefaultUI** is a decoupled UI package for **GlanceSDK**. It serves as an out-of-the-box implementation of **GlanceSDK**, but it can be modified at the code level to your liking.

**GlanceDefaultUI** adds **GlanceSDK** automatically.


## Table of Contents
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Features](#features)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Initialization, Starting and Joining Sessions](#starting-and-ending-sessions)
- [Usage Examples](#usage-examples)
- [API Reference](#api-reference)
  - [Singleton Instance](#singleton-instance)
  - [Methods](#methods)
  - [Delegate Methods](#delegate-methods)
- [Error Handling](#error-handling)
- [Advanced Features](#advanced-features)
- [Contributing](#contributing)

## Overview

The Glance SDK enables your mobile app to be shareable to one or more viewing agents at your organization so they can offer your users a guided customer experience.

Glance is designed to run in parallel with your organization's existing voice communications systems. After a customer contacts your organization, Glance can be introduced at an agent's discretion.

Sessions start in the SDK within your organization's Glance Group. Your organization's licensed Glance users are then able to join these sessions to guide your customers in the app.

Through Glance's Presence service, your Glance users can proactively offer sessions to identified customers through CRM systems like Salesforce for a more seamless experience.

One-way or two-way video can be added to sessions within certain configurations. This is also intended to be used in parallel with your existing voice communication. Glance video does not currently transmit audio.


It includes the GlanceCoreSDK for additional backend features.

## Features

GlanceDefaultUI provides a comprehensive set of features designed to enhance the remote customer support experience within mobile applications. Here's what you can expect when integrating this UI package:

- **Viewing Mobile App-Content**: Enables agents to view a customer's app content in real-time, facilitating accurate support and guidance.

- **Agent Gesturing and Highlighting**: Agents can gesture and highlight areas on the screen during a session to guide users more effectively.

- **Video Communication**:
  - **One-Way Video**: Allows the customer to see the agent without showing their own video feed.
  - **Two-Way Video**: Facilitates a more personal interaction by enabling both parties to share video feeds.

- **1-Click Connect**: Allows agents to send session requests directly to customers with a single click, streamlining the process of starting a Glance session.

- **Sensitive Content Masking**:
  - **Native Content Masking**: Securely masks parts of the app identified as sensitive, ensuring that agents do not see private information.
  - **WebView Masking**: Extends content masking capabilities to web views, providing the same level of security for web-based content within the app.

These features are designed to create a seamless and secure environment for remote customer support and app-sharing experiences.


## Dependencies
This SDK requires the following frameworks and libraries:
- **Foundation**: Essential for all Swift applications.
- **GlanceSDK and GlanceSDKSwift**: Proprietary SDKs for interfacing with Glance services.

## Getting Started

### Installation

#### Prerequisites

1. Obtain a Glance account by contacting your organization's administrator. If you are new to Glance, please contact us at https://www.glance.cx/contact.
2. Confirm with your Glance administrator that your user or Role [has permission to Join Screen Share sessions started by customers](https://help.glance.net/account-management/manage_settings/#$manage-settings), as well as to use video if needed.
3. Obtain your Group ID from your Glance administrator. It is usually a five-digit number.

#### Add the SDK via XCode
1. In Xcode, install **GlanceDefaultUI** by navigating to **File > Add Packages**
2. In the prompt that appears, select the **GlanceDefaultUI** repository:
```
https://gitlab.com/glance-sdk-public/glance-default-ui-ios-releases
```
3. Select the version of **glance-sdk-ios-releases** you want to use. We recommend always using the newest version.

#### Add the SDK via Package.swift
1. To integrate **GlanceDefaultUI** to a Swift package via a **Package.swift** manifest, you can add **GlanceDefaultUI** to the dependencies array of your package.
```
dependencies: [

  .package(name: "GlanceDefaultUI",
           url: "https://gitlab.com/glance-sdk-public/glance-default-ui-ios-releases.git",
           from: "6.6.0"),
  // ...

],
```

#### Changing Localizable Strings

All labels/Strings are in `Localizable.strings` file in `GlanceDefaultUI > Sources > GlanceDefaultUI > Resources > Localizable` folder.

### Initialization, Starting and Joining Sessions

#### Set Visitor Params

Your first call should be `GlanceDefaultUI.setVisitorParams` to initialize the SDK and ready it for sharing.

| Parameter | Type | Description | |
|----------------|-------------------------------|-----------------------------| ---|
|groupid|`Int32`|A Glance-provided identifier for your organization| Required|
|visitorid|`String`|Customer identifier for use with Presence| Optional |
|video|`Bool`|Option to use video in sessions| Optional |

Example:

```swift
import GlanceCore
import GlanceSDK
import GlanceDefaultUI
.
.
let params = GlanceVisitorInitParams()
params.groupid = 33333
params.visitorid = visitorId

GlanceDefaultUI.setVisitorParams(params)
```

#### Starting a Session

To start a session, you will need to call GlanceDefaultUI.start(), like in this example with a button:
```swift
Button(action: {
    GlanceDefaultUI.start(GlanceVideoMode:VideoOff)
}) { 
    Text("Start Glance Session")
}
```

This will start a Glance session without video and randomize the Glance session key. The SDK will automatically obtain a unique 4-digit session key from Glance's servers. The Default UI will display it to the user to share with an agent over a voice call.


#### Joining Sessions

The SDK will automatically obtain a unique 4-digit session key from Glance's servers. The Default UI will display it to the user to share with an agent over a voice call.

To join a Glance session started in your app:

1. Go to [www.glance.net/agentjoin](www.glance.net/agentjoin).
2. Log in with your Glance address and password.
3. Enter the session key and click Join Session.

Glance doesn't include voice communication. A session is naturally suggested and started as needed during a conversation in progress. To simplify the agent side experience, Glance offers a range of [APIs and off-the-shelf integrations with workstation platforms like Salesforce and Genesys](https://help.glance.net/integrations/). For testing, you can use the join page linked above.

## Usage Examples
Detailed examples on initiating sessions with and without video, managing presence, and masking sensitive UI elements.

### Starting Sessions
- **Starting a Session with Video**
  ```swift
  func startGlanceSession() {
      let params = GlanceVisitorInitParams()
      params.groupid = 1000
      GlanceDefaultUI.setVisitorParams(params)
      GlanceDefaultUI.start(videoMode: VideoSmallVisitor) 
  }
  ```
- **Starting a Session without Video**
  ```swift
  func startGlanceSession() {
      let params = GlanceVisitorInitParams()
      params.groupid = 1000
      GlanceDefaultUI.setVisitorParams(params)
      GlanceDefaultUI.start(videoMode: VideoOff)
  }
  ```

## Hosted Sessions
Unlike normal sessions (GlanceVisitor.startSession) initiated by guests/visitors, hosted sessions are initiated by authenticated user (Agents).

### Start Hosted Session
```swift
    GlanceDefaultUI.startHostedSession(username: username, password: password)
```

To start a Hosted Session, you need:
- Agent has to login to Glance Client using a valid Glance account.  
- The guest in this case does not need to log in or have a group id.  The group ID is tied to the agent who logs into the mobile SDK.
- The ways to join a hosted sessions are: 
    ◦ using the joinssn protocol with the Glance client
    ◦ from the /visitor/join pages (agent video cannot be used in this 2nd case)
    ◦ going directly to a URL like https://www.glance.net/?username=TEST.glance.net&key=1234

### Set delegate to *GlanceDefaultUI*
Since *GlanceDefaultUI* manages everything, you need to check if delegate is properly set
```swift
    GlanceDefaultUI.setDelegate(self)
```

### Conform *GlanceUIDelegate* protocol
For *GlanceUIDelegate* protocol, these two methods for Hosted Session you need to conform:
```swift
    func hostedSessionDidStart(sessionKey: String)
    func hostedSessionDidEnd(errorMessage: String)
```
They are only for logging and no real implementation is needed, you can let them empty.

## Customizing UI
 This project's code is fully exposed. You may customize it however you need for custom branding or behavior.

## Change Localizable Strings
All labels/Strings are in `Localizable.strings` file in `GlanceDefaultUI > Sources > GlanceDefaultUI > Resources > Localizable` folder.

### Advanced Features
- **Enable 1-Click Connect via Presence**
  ```swift
  private func configureGlance() {
      var params = GlanceVisitorInitParams()
      params.groupid = 1000
      params.visitorid = "uniqueVisitorID"
      GlanceDefaultUI.setPresence(true)
      GlanceDefaultUI.setVisitorParams(params)
  }
  ```

### Content Masking with GlanceDefaultUI

GlanceDefaultUI supports content masking to protect sensitive data during sessions. This feature is particularly useful in environments where privacy is paramount. Below are the details on how to implement content masking for both UIViews and SwiftUI views.

#### Masking UIViews

The SDK allows you to mask any UIView during active sessions to ensure sensitive information is not visible to support agents. The masking functionality is in the GlanceSDK. Here’s how you can implement this:

```swift
import GlanceSDK

//Simply call the Glance masking function to the specific UIView that you want to mask.
Glance.addMaskedView(view, with: label)
```

If your project is using SwiftUI Views instead of the UIKit UIViews, you can mask those elements wrapping it with a ViewController. Below you can find an example:

```swift
import GlanceSDK

// Example: Masking the entire view of a hosting controller.
let hostingController = ... // Your hosting controller instance
Glance.addMaskedView(hostingController.view, withLabel: "Profile Info")

// Example: Masking a specific subview
if let subview = hostingController.view.findSubview(withTag: 123) {
    Glance.addMaskedView(subview, withLabel: "Specific Field")
}
```
- **WebView Masking**
  ```swift
  let config = WKWebViewConfiguration()
  let maskingQuerySelectors = ["#licenseNumber", "#ssn"]
  let maskingLabels = ["License Number", "SSN"]
  let glanceMaskContentController = GlanceMaskContentController(maskingQuerySelectors.joined(separator: ", "), labels: maskingLabels.joined(separator: ", "))
  config.userContentController = glanceMaskContentController
  let webView = WKWebView(frame: .zero, configuration: config)
  ```
  
## Localization

The localization resources for this module are organized into two files:
- *GlanceLocalizedStringsConstants.swift*: Located at GlanceDefaultUI/Resources/.
This Swift file contains constant references to the localized strings used throughout the code. 
It now attempts to load localized strings from the main bundle first. If the key is not found in the main bundle, it falls back to the module bundle (DialogsCoordinator.self).
- *Localizable.strings*: Located at GlanceDefaultUI/Resources/Localizable/.
This file contains the key-value pairs for the localized strings.

### Adding Support for New Languages

Currently, only English is supported. To add support for a new language:
1. *Create a new localization folder*: Inside the Resources directory, create a
new folder named after the language code (e.g., es.lproj for Spanish, fr.lproj for French).
2. *Copy the Localizable.strings file*: Copy the existing Localizable.strings
file into the new language folder you just created.
3. *Translate the strings*: Open the copied Localizable.strings file in the
new language folder and translate the values accordingly.

### Existing localizables
```swift
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TITLE" = "In-App Visual Call";
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TEXT" = "Let an agent guide you?";
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_NO_BUTTON_TITLE" = "No";
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_YES_BUTTON_TITLE" = "Yes";
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TERMS_TEXT" = "<b><u>Terms and Conditions</u></b>";
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TEXT_VIDEO" = "Allow the agent to start video?";

"GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_TITLE" = "Allow the agent to view this app?";
"GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_NO_BUTTON_TITLE" = "No";
"GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_YES_BUTTON_TITLE" = "Yes";
"GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_SHOW_TERMS_BUTTON_TITLE" = "Show Terms";

"GLANCE_DEFAULT_UI_START_DIALOG_TITLE" = "In-App Visual Call";
"GLANCE_DEFAULT_UI_START_DIALOG_SUBTITLE" = "You are about to start a phone call with Customer Service.";
"GLANCE_DEFAULT_UI_START_DIALOG_TITLE_AGENT" = "Your agent will be able to see your screen.";
"GLANCE_DEFAULT_UI_START_DIALOG_PHONE_TEXT" = "No need to go to your phone app";
"GLANCE_DEFAULT_UI_START_DIALOG_HEADSET_TEXT" = "Turn on the speaker or use your headphones";
"GLANCE_DEFAULT_UI_START_DIALOG_DRAW_TEXT" = "Your agent will guide you by drawing on your screen";
"GLANCE_DEFAULT_UI_START_DIALOG_VIDEO_TEXT" = "You'll see live video of your agent but the agent won't see you";
"GLANCE_DEFAULT_UI_START_DIALOG_CANCEL_BUTTON_TITLE" = "Cancel";
"GLANCE_DEFAULT_UI_START_DIALOG_ACCEPT_BUTTON_TITLE" = "Accept";
"GLANCE_DEFAULT_UI_START_DIALOG_TERMS_TEXT" = "By clicking <b>Accept</b>, you agree to our<br/><b><u>Terms and Conditions</u></b> and will be connected to an agent.";

"GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_TEXT" = "You are about to start a video call with Customer Service.\n\nWhen you tap \"Start Your Video\", your phone camera and microphone will become active, and the Customer Service agent will see your video.";
"GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_CANCEL_BUTTON_TITLE" = "Cancel";
"GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_ACCEPT_BUTTON_TITLE" = "Start Your Video »";
"GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_TERMS_TEXT" = "By tapping <b>Start Your Video</b>, you agree to our<br/><b><u>Terms and Conditions</u></b>";
"GLANCE_VIDEO_DEFAULT_UI_IN_QUEUE_TEXT" = "In Queue";

"GLANCE_KEYBOARD_MASK_LABEL" = "Keyboard is showing";
"GLANCE_BACKGROUND_SUSPEND_MASK_LABEL" = "The session has been temporarily suspended.  The visitor may have closed the app.  Waiting for the other side to reconnect.";
"GLANCE_BACKGROUND_PAUSE_MASK_LABEL" = "Screen not being shown...";

"GLANCE_DEFAULT_UI_AGENT_CODE_TEXT" = "Please give the agent this code:";
"GLANCE_DEFAULT_UI_IN_QUEUE_TEXT" = "In Queue";
"GLANCE_DEFAULT_UI_CANCEL_TEXT" = "Cancel";
"GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_TITLE" = "Are you sure?";
"GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_MESSAGE" = "This will end the Visual Call.";
"GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_END_BUTTON" = "End Call";
"GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_CANCEL_BUTTON" = "Cancel";

"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_APP_SHARE" = "Share app with the representative?";
"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_APP_SHARE_AND_VIDEO" = "Share video and app with the agent?";
"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_QUEUE" = "Waiting for the agent to connect";
"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_QUEUE_CODE" = "Please give the agent this code:";
"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_SESSION_ADD_VIDEO" = "Share your video with the agent?";
"GLANCE_SESSION_START_DIALOG_HEADER_TEXT_END_SESSION" = "Are you sure you want to end this session?";
"GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_APP_SHARE" = "The agent can guide you through this app only. You can stop sharing at any time.";
"GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_APP_SHARE_AND_VIDEO" = "The agent can guide you through this app only. You will be able to hear and see each other. You can stop sharing, turn off video, and manage your audio at any time.";
"GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_IN_SESSION_ADD_VIDEO" = "You will be able to see each other, and you can turn off video at any time.";
"GLANCE_SESSION_START_DIALOG_ACCEPT_BUTTON_TEXT" = "Accept";
"GLANCE_SESSION_START_DIALOG_DECLINE_BUTTON_TEXT" = "Decline";
"GLANCE_SESSION_START_DIALOG_YES_BUTTON_TEXT" = "Yes";
"GLANCE_SESSION_START_DIALOG_NO_BUTTON_TEXT" = "No";
"GLANCE_SESSION_START_DIALOG_TERMS_LINK_TEXT" = "Terms and Conditions";

"GLANCE_CAMERA_ALERT_TITLE" = "Camera Access";
"GLANCE_CAMERA_PERMISSION" = "Camera access is turned off in this app. To show your camera in a session, please enable the camera in Settings > Privacy > Camera > %@. You will need to restart the session.";

"GLANCE_GO_TO_SETTINGS" = "Go to Settings";

"APP_SHARE_DIALOG_TITLE" = "Share app with the agent?";
"APP_SHARE_DIALOG_VIDEO_TITLE"= "Share video and app with the agent?";
"APP_SHARE_DIALOG_SUBTITLE" = "The agent can guide you through this app only. You can stop sharing at any time.";
"APP_SHARE_DIALOG_VIDEO_SUBTITLE"= "The agent can guide you through this app only. You will be able to hear and see each other. You can stop sharing, turn off video, and manage your audio at any time.";
"APP_SHARE_DIALOG_PRIMARY_BUTTON" = "Accept";
"APP_SHARE_DIALOG_SECONDARY_BUTTON" = "Decline";
"APP_SHARE_DIALOG_URL" = "https://ww2.glance.net/";

"WAITING_DIALOG_TITLE" = "Waiting for specialist to join";
"LOGO_VIEW_TITLE" = "POWERED BY GLANCE";
```

### Modifying Existing Labels

To change the text of an existing label:
1. *Edit the Localizable.strings file*: Locate the key-value pair you wish to modify and update the value:

```swift
"GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TITLE" = "Updated text here";
```

This update will automatically reflect wherever the label is used in the app.

### Important: Localization Loading Order

With the recent changes, localized strings are now first loaded from the main bundle. If a localized string is found there, it is used directly. If the string is not found in the main bundle, the system will fall back to the DialogsCoordinator module bundle to provide the localization. This allows the main bundle to act as the primary localization source, offering more flexibility in dynamically updating localizations if needed.

## API Reference

The `GlanceDefaultUI` class offers a suite of methods to manage sessions, configure visitor parameters, and handle real-time interactions. Below is a detailed overview of its public and private methods, and their functionalities.

### Singleton Instance

- **shared**
  - A global static instance of `GlanceDefaultUI` to ensure a single entry point and uniform state throughout the application.

### Methods

#### Public Methods

- **setVisitorParams(_ params: GlanceVisitorInitParams, maxConnectAttempts: Int = 0 )**
  - Sets the initial parameters for a visitor session, including group ID and visitor identification, preparing the SDK for a session. Please note that setVisitorParams can be called more than once. If you need to change some information previously configurated, just change `GlanceVisitorInitParams` and call `setVisitorParams` again. The only property that cannot be changed is groupid. Changing this property requires relaunching the app and reinitializing the SDK.
  - **Parameters**
    - `params`: An instance of `GlanceVisitorInitParams` containing session initialization parameters such as `groupid`, `visitorid`, and whether to enable video.
    - `maxConnectAttempts`: Number of retries to make in a case of failed connection. ( Optional )

- **start(key: String?, videoMode: GlanceVideoMode)**
  - Starts a session with optional video configurations. This method configures and initiates the session environment based on provided parameters.
  - **Parameters**
    - `key`: Optional. A session-specific key that uniquely identifies the session.
    - `videoMode`: Specifies the video settings for the session, determining whether video is off, one-way, or two-way.
        - 0 - VideoOff
        - 1 - VideoSmallVisitor
        - 2 - VideoLargeVisitor
        - 3 - VideoSmallMultiway
        - 4 - VideoLargeMultiway

- **endSession()**
  - Ends the currently active session and cleans up resources, ensuring that all session data is properly terminated.

- **setPresence(_ isPresenceOn: Bool, maxConnectAttempts: Int32? = nil)**
  - Toggles the presence feature, which enables real-time session initiation from an agent to a visitor, enhancing proactive customer engagement.
  - **Parameters**
    - `isPresenceOn`: A Boolean value that activates or deactivates the presence feature.
    - `maxConnectAttempts`: Number of retries to make in a case of failed connection. ( Optional )
   
- **setDelegate(_ delegate: GlanceUIDelegate)**
  - Assigns a delegate to handle error events and other session-related notifications.
  - **Parameters**
    - `delegate`: An object that conforms to the `GlanceUIDelegate` protocol, capable of responding to errors and session changes.

#### Private Methods

- **setVisitorParams(_ params: GlanceVisitorInitParams) (private instance method)**
  - Configures visitor parameters internally and prepares the SDK to initiate a session with the correct settings. 
  - **Parameters**
    - `params`: Visitor parameters as `GlanceVisitorInitParams`.

- **start(key: String?, videoMode: GlanceVideoMode) (private instance method)**
  - Internally begins the session based on the visitor parameters set and the optional video mode.

- **showShowCodeDialog(sessionKey: String)**
  - Displays a dialog to show the session code to the user, allowing them to share it with an agent for session joining.
  - **Parameters**
    - `sessionKey`: The unique code generated for the session.

- **showWaitingAgentDialog(dismissAction: (() -> Void)?)**
  - Shows a waiting dialog while the session is being connected, optionally providing a dismissal action.
  - **Parameters**
    - `dismissAction`: An optional closure to execute upon dismissing the dialog.

- **startSession()**
  - Begins the session formally after all initial parameters and settings have been configured and the session type has been determined.

### Delegate Methods

- **onError(_ error: GlanceError)**
  - Invoked when an error occurs within the SDK's operation, delegating error handling to the assigned `GlanceUIDelegate`. 
  - **Parameters**
    - `error`: An instance of `GlanceError` detailing the nature of the error. GlanceError has two properties, code and message. The GlanceError code corresponds with the EventCode property of events fired by the SDK, for example an error with starting a session will have an error code matching the EventCode for failing sessions, EventStartSessionFailed.

- **sessionConnected(dismissAction: (() -> Void)?)**
  - Called when a session is successfully connected. Provides an option to specify an action upon dismissing the connection prompt.
  - **Parameters**
    - `dismissAction`: An optional closure to execute after the session connection is established.

- **receivedSessionCode(_ sessionCode: String)**
  - Triggered when a session code is received, which is necessary for joining sessions manually.
  - **Parameters**
    - `sessionCode`: The unique session code received from the server.

- **func sessionEnded(_ sessionKey: String?)**
  - Notifies when a session has ended, either through user action or system processes. sessionKey is the key of the session that just ended.

Additional methods and functionalities include handling video capture events, managing UI updates related to session status, and integrating agent video feeds. These are part of the deeper integration details that facilitate comprehensive session management within the GlanceDefaultUI framework.

