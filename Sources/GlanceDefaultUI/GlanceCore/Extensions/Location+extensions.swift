//
//  Location+extensions.swift
//  
//
//  Created by Glance on 10/18/23.
//

import Foundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

extension GlanceLocation {
    static func getLocation(from location: GlanceLocation) -> (horizontal: SessionViewController.ContainerLocation, vertical: SessionViewController.ContainerLocation) {
        switch location {
        case .topLeft:
            return (horizontal: .left, vertical: .top)
        case .topRight:
            return (horizontal: .right, vertical: .top)
        case .bottomLeft:
            return (horizontal: .left, vertical: .bottom)
        case .bottomRight:
            return (horizontal: .right, vertical: .bottom)
        }
    }
}
