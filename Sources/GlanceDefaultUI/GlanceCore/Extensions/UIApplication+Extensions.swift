//
//  UIApplication+Extensions.swift
//
//
//  Created by bruno on 06/02/24.
//

import UIKit

extension UIApplication {
    var currentKeyWindow: UIWindow? {
        // For iOS 13 and later
        if #available(iOS 13.0, *) {
            return connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .compactMap { $0 as? UIWindowScene }
                .flatMap { $0.windows }
                .first(where: { $0.isKeyWindow })
        } else {
            // Fallback for earlier versions of iOS
            return keyWindow
        }
    }
}
