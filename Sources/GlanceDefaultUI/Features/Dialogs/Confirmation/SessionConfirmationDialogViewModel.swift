//
//  SessionConfirmationDialogViewModel.swift
//  
//
//  Created by Glance on 9/19/23.
//

import AVFoundation
import Foundation
import Combine

final class SessionConfirmationDialogViewModel: ObservableObject {
    var onAccept = PassthroughSubject<Void, Never>()
    var onDismiss = PassthroughSubject<Void, Never>()
}
