//
//  SessionConfirmationDialogView.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI
import Combine
import AVFoundation

struct SessionConfirmationDialogView: View {

    @ObservedObject var viewModel: SessionConfirmationDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView
            }
            .padding([.all], Layout.Size.medium)
        }
    }
    
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            Text(GlanceLocalizedStringsConstants.sessionStartDialogHeaderTextEndSession)
                .glanceFont(GlanceFont.h2)
                .bold()
                .foregroundColor(.basePrimary)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            GlanceConfirmationButtons(primaryButtonText: GlanceLocalizedStringsConstants.sessionStartDialogYesButtonText,
                                      secondaryButtonText: GlanceLocalizedStringsConstants.sessionStartDialogNoButtonText,
                                      onPrimaryTapped: viewModel.onAccept,
                                      onSecondaryTapped: viewModel.onDismiss)
        }
    }
}

struct SessionConfirmationDialogView_Previews: PreviewProvider {
    static var previews: some View {
        SessionConfirmationDialogView(viewModel: SessionConfirmationDialogViewModel())
    }
}
