//
//  SessionWaitingAgentDialogView.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI

struct SessionWaitingAgentDialogView: View {

    @ObservedObject var viewModel: SessionWaitingAgentDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView
            }
            .padding([.all], Layout.Size.medium)
        }
    }
    
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            HStack {
                HStack(spacing: .zero) {
                    LoadingCircle()
                    
                    Text(GlanceLocalizedStringsConstants.waitingDialogTitle)
                        .glanceFont(GlanceFont.h4)
                        .bold()
                        .foregroundColor(.black)
                        .fixedSize(horizontal: false, vertical: true)
                }
                
                Spacer()
                
                CloseButton() {
                    viewModel.onDismiss.send()
                }
            }
            
            HStack {
                GlanceLogoView()
                    
                Spacer()
                
                CancelButton() {
                    viewModel.onDismiss.send()
                }
            }
        }
    }
}

struct SessionWaitingAgentDialogView_Previews: PreviewProvider {
    static var previews: some View {
        SessionWaitingAgentDialogView(viewModel: SessionWaitingAgentDialogViewModel())
    }
}
