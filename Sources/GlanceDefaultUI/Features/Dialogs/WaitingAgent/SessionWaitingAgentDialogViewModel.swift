//
//  SessionWaitingAgentDialogViewModel.swift
//  
//
//  Created by Glance on 9/19/23.
//

import Foundation
import Combine

class SessionWaitingAgentDialogViewModel: ObservableObject {    
    var onDismiss = PassthroughSubject<Void, Never>()
}
