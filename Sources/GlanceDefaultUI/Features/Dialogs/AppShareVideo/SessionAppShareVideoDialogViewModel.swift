//
//  SessionStartWithCameraDialogViewModel.swift
//  
//
//  Created by Glance on 9/20/23.
//

import Foundation
import AVFoundation
import SwiftUI
import Combine
#if canImport(GlanceSDK)
import GlanceSDK
#endif

final class SessionAppShareVideoDialogViewModel: ObservableObject {
    
    // MARK: - Properties
    
    @Published var isAudioEnabled: Bool = false
    @Published var isMicEnabled: Bool = false
    @Published var isVideoEnabled: Bool = true
    @Published var isBlurEnabled: Bool = false
    @Published var captureSession: AVCaptureSession?
    
    var onAccept = PassthroughSubject<Void, Never>()
    var onDismiss = PassthroughSubject<Void, Never>()
    var onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>
    
    private var service: GlanceServiceType
    private var cancelSet = Set<AnyCancellable>()
    
    init(
        service: GlanceServiceType,
        onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>
    ) {
        self.onUpdateCaptureSession = onUpdateCaptureSession
        self.service = service
        
        onUpdateCaptureSession
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] captureSession in
                guard self?.captureSession == nil else { return }
                self?.captureSession = captureSession
            }
            .store(in: &cancelSet)
        
        $isVideoEnabled
            .receive(on: DispatchQueue.global(qos: .background))
            .sink { [weak self] isEnabled in
                guard let self else { return }
                if isEnabled {
                    self.captureSession?.startRunning()
                } else {
                    self.captureSession?.stopRunning()
                }
            }
            .store(in: &cancelSet)
            
        onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                let glanceStartParams = GlanceDefaultUI.shared.glanceStartParams
                GlanceDefaultUI.shared.isVisitorVideoEnabled = self.isVideoEnabled
                let maxConnectAttempts = GlanceDefaultUI.shared.maxAgentSessionConnectAttempts
                let timeout = GlanceDefaultUI.shared.startSessionTimeout
                self.service.startSession(params: glanceStartParams, timeout: timeout, maxConnectAttempts: maxConnectAttempts)
                
            }
            .store(in: &cancelSet)
    }
    
    func startVideoPreview() {
        self.service.startVideoPreview()
    }
}
