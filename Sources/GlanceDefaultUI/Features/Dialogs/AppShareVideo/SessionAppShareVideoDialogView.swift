//
//  SessionStartWithCameraDialogView.swift
//  
//
//  Created by Glance on 9/20/23.
//

import SwiftUI
import Combine
import AVFoundation

struct SessionAppShareVideoDialogView: View {
    
    @ObservedObject var viewModel: SessionAppShareVideoDialogViewModel
    
    var body: some View {
        ZStack {
            Color.white
            
            VStack(spacing: .medium) {
                headerView
                
                contentView
                
                footerView
            }
            .padding(.all, .medium)
        }.onAppear {
            viewModel.startVideoPreview()
        }
    }
    
    var footerView: some View {
        VStack(spacing: 16) {
            LinkText(text: GlanceLocalizedStringsConstants.sessionStartDialogTermsLinkText,
                     url: GlanceLocalizedStringsConstants.shareAppDialogURL)
            
            GlanceLogoView()
        }
    }
    
    var contentView: some View {
        VStack(spacing: 16) {
            messageView
            
            GlanceConfirmationButtons(primaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogPrimaryButton,
                                      secondaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogSecondaryButton,
                                      onPrimaryTapped: viewModel.onAccept,
                                      onSecondaryTapped: viewModel.onDismiss)
        }
    }
    
    var titleView: some View {
        Text(GlanceLocalizedStringsConstants.defaultUIAgentCodeText)
            .glanceFont(GlanceFont.h2)
            .foregroundColor(.black)
            .bold()
            .multilineTextAlignment(.center)
            .fixedSize(horizontal: false, vertical: true)

    }
    
    func codeView(from sessionKey: String) -> some View {
        Text(sessionKey)
            .glanceFont(GlanceFont.title)
            .bold()
            .foregroundColor(.basePrimary)
            .multilineTextAlignment(.center)
    }
    
    var messageView: some View {
        VStack(spacing: 8) {
            Text(GlanceLocalizedStringsConstants.shareAppDialogVideoTitle)
                .glanceFont(GlanceFont.h2)
                .foregroundColor(.black)
                .bold()
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
        
            Text(GlanceLocalizedStringsConstants.shareAppDialogVideoSubtitle)
                .glanceFont(GlanceFont.bodyText)
                .foregroundColor(.black)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
    
    var headerView: some View {
        VStack(spacing: .small) {
            headerCaptureSession
            
            headerButtonsView
        }
    }
    
    var headerCaptureSession: some View {
        ZStack {
            ZStack {
                RoundedRectangle(cornerRadius: 9)
                    .foregroundColor(.black)
                
                if viewModel.isVideoEnabled {
                    if let captureSession = viewModel.captureSession {
                        PreviewView(captureSession: captureSession)
                            .opacity(viewModel.isVideoEnabled ? 1 : 0)
                    }
                } else {
                    headerCaptureSessionDisabledImage
                }
            }
            .frame(width: 140, height: 140)

            HStack {
                Spacer()
                
                VStack {
                    closeButtonView
                    
                    Spacer()
                }
            }
        }
    }
    
    var headerCaptureSessionDisabledImage: some View {
        ZStack {
            Circle()
                .frame(width: 50, height: 50)
                .foregroundColor(.basePrimary)
            
            Image(glanceImage: .glanceNewHeadphones)
                .resizableFrame(width: 24, height: 24)
                .scaledToFill()
                .foregroundColor(.white)
        }
    }
    
    var headerButtonsView: some View {
        HStack(spacing: 20) {
//            buildHeaderButton(image: .glanceNewSpeakerphoneOn, imageSelected: .glanceNewSpeakerphoneOff, isSelected: $viewModel.isAudioEnabled)
//            buildHeaderButton(image: .glanceNewMicrophoneOn, imageSelected: .glanceNewMicrophoneOff, isSelected: $viewModel.isMicEnabled)
            buildHeaderButton(image: .glanceNewVideoOn, imageSelected: .glanceNewVideoOff, isSelected: $viewModel.isVideoEnabled)
//            buildHeaderButton(image: .glanceNewBlurOn, imageSelected: .glanceNewBlurOff, isSelected: $viewModel.isBlurEnabled)
        }
    }
    
    var closeButtonView: some View {
        CloseButton {
            viewModel.onDismiss.send()
        }
        .frame(height: 140, alignment: .topTrailing)
    }
    
    func buildHeaderButton(image: Image.GlanceImages, imageSelected: Image.GlanceImages, isSelected: Binding<Bool>) -> some View {
        Button {
            withAnimation {
                isSelected.wrappedValue.toggle()
            }
        } label: {
            ZStack {
                if isSelected.wrappedValue {
                    Circle()
                        .stroke()
                        .frame(width: 49, height: 49)
                } else {
                    Circle()
                        .frame(width: 49, height: 49)
                        .shadow(color: .gray.opacity(0.1), radius: 3)
                }
                
                Circle()
                    .frame(width: 49, height: 49)
                    .foregroundColor(isSelected.wrappedValue ? .white : .basePrimary10)
                
                Image(glanceImage: isSelected.wrappedValue ? image : imageSelected)
            }
        }
    }
}

struct SessionStartWithCameraDialogView_Previews: PreviewProvider {
    static var previews: some View {
        SessionAppShareVideoDialogView(viewModel: SessionAppShareVideoDialogViewModel(service: GlanceService(), onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>()))
    }
}

final class GlanceVideoCapturePreviewView: UIView {
    
    private weak var captureVideoPreviewLayer: AVCaptureVideoPreviewLayer?
    private var isInitialized = false
    
    init(previewLayer: AVCaptureVideoPreviewLayer) {
        self.captureVideoPreviewLayer = previewLayer
        self.captureVideoPreviewLayer?.videoGravity = .resizeAspectFill
        self.captureVideoPreviewLayer?.cornerRadius = 9.0
        
        super.init(frame: .zero)
        
        self.layer.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard superview != nil else { return }
        guard let captureVideoPreviewLayer else { return }
        guard !isInitialized else { return }
        isInitialized = true
        
        layer.addSublayer(captureVideoPreviewLayer)
    }
    
    override func layoutSubviews() {
        captureVideoPreviewLayer?.frame = bounds
        super.layoutSubviews()
    }
    
}
