//
//  SessionStartVideoDialogView.swift
//
//
//  Created by bruno on 11/23/23.
//

import SwiftUI
import Combine

struct SessionStartVideoDialogView: View {

    @ObservedObject var viewModel: SessionStartVideoDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView
                footerView
            }
            .padding([.all], Layout.Size.medium)
        }
    }
    
    var footerView: some View {
        VStack(spacing: 16) {
            LinkText(text: GlanceLocalizedStringsConstants.sessionStartDialogTermsLinkText,
                     url: GlanceLocalizedStringsConstants.shareAppDialogURL)
            
            GlanceLogoView()
        }
    }
    
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            Text(GlanceLocalizedStringsConstants.shareAppDialogTitle)
                .glanceFont(GlanceFont.h3)
                .bold()
                .foregroundColor(.basePrimary)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(GlanceLocalizedStringsConstants.shareAppDialogSubtitle)
                .glanceFont(GlanceFont.bodyText)
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            GlanceConfirmationButtons(primaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogPrimaryButton,
                                      secondaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogSecondaryButton,
                                      onPrimaryTapped: viewModel.onAccept,
                                      onSecondaryTapped: viewModel.onDismiss)
        }
    }
}
