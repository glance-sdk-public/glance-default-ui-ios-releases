//
//  SessionStartVideoDialogViewModel.swift
//
//
//  Created by Glance on 11/23/23.
//

import AVFoundation
import Foundation
import Combine

final class SessionStartVideoDialogViewModel: ObservableObject {
    @Published var isVideoEnabled: Bool = true
    @Published var captureSession: AVCaptureSession?
    
    var onAccept = PassthroughSubject<Void, Never>()
    var onDismiss = PassthroughSubject<Void, Never>()
    var onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>
    private var cancelSet = Set<AnyCancellable>()
    
    init(onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>) {
        self.onUpdateCaptureSession = onUpdateCaptureSession
        
        onUpdateCaptureSession
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] captureSession in
                guard self?.captureSession == nil else { return }
                self?.captureSession = captureSession
            }
            .store(in: &cancelSet)
    }
}

