//
//  DialogsCoordinator.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 23/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import UIKit
import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

@objc final public class DialogsCoordinator: NSObject {
    
    private lazy var dialogWindow: DialogWindow = {
        DialogWindow()
    }()
    private var cancelSet = Set<AnyCancellable>([])
    public let service: GlanceServiceType
    
    var onUpdateCaptureSession = PassthroughSubject<AVCaptureSession, Never>()
    
    public init(service: GlanceServiceType = GlanceService()) {
        self.service = service
    }
    
    @objc public func presentAppShareDialog() {
        let viewModel = SessionAppShareDialogViewModel()
        let view = SessionAppShareDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        viewModel
            .onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentAppShareVideoDialog() {
        let viewModel = SessionAppShareVideoDialogViewModel(service: service,
                                                            onUpdateCaptureSession: onUpdateCaptureSession)
        let view = SessionAppShareVideoDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.endSession()
                self.service.stopVideoPreview()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        viewModel
            .onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.stopVideoPreview()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentAgentAskShareVideoDialog(
        confirmAction: (() -> Void)? = nil,
        cancelAction: (() -> Void)? = nil
    ) {
        let viewModel = AgentShareVideoDialogViewModel(service: service,
                                                       onUpdateCaptureSession: onUpdateCaptureSession,
                                                       messageManager: MessageManager())
        let view = AgentShareVideoDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.stopVideoPreview()
                self.dialogWindow.hide()
                cancelAction?()
            }.store(in: &cancelSet)
        
        viewModel
            .onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.stopVideoPreview()
                self.dialogWindow.hide()
                confirmAction?()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentWaitingAgentDialog(dismissAction: (() -> Void)?) {
        let viewModel = SessionWaitingAgentDialogViewModel()
        let view = SessionWaitingAgentDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                dismissAction?()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentConfirmationDialog(confirmAction: (() -> Void)? = nil) {
        let viewModel = SessionConfirmationDialogViewModel()
        let view = SessionConfirmationDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        viewModel
            .onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                confirmAction?()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentShowCodeDialog(code: String) {
        let viewModel = SessionShowCodeDialogViewModel(sessionKey: code)
        let view = SessionShowCodeDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.endSession()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func showHostedSessionKey(code: String) {
        let viewModel = HostedSessionKeyDialogViewModel(sessionKey: code)
        let view = HostedSessionKeyDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                self.service.endSession()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    @objc public func presentStartDialog(
        confirmAction: @escaping (() -> Void),
        cancelAction: (() -> Void)?
    ) {
        let viewModel = SessionStartVideoDialogViewModel(onUpdateCaptureSession: onUpdateCaptureSession)
        let view = SessionStartVideoDialogView(viewModel: viewModel)
        
        viewModel
            .onDismiss
            .sink { [weak self] _ in
                guard let self else { return }
                cancelAction?()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        viewModel
            .onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                confirmAction()
                self.dialogWindow.hide()
            }.store(in: &cancelSet)
        
        present(view)
    }
    
    private func present(_ view: some View) {
        let dialogView = DialogView { view }
        let hostingController = UIHostingController(rootView: dialogView)
        hostingController.modalPresentationStyle = .fullScreen
        hostingController.view.backgroundColor = .clear
        
        dialogWindow.rootViewController = hostingController
        dialogWindow.show()
    }
    
    func hide() {
        if dialogWindow.isShowing {
            dialogWindow.hide()
        }
    }
}
