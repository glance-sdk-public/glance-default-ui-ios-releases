//
//  SessionAppShareDialogView.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 24/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI
import Combine

struct SessionAppShareDialogView: View {
    
    @ObservedObject var viewModel: SessionAppShareDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView

                footerView
            }
            .padding(.all, Layout.Spacing.large)
        }
    }
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            HStack {
                Spacer()
                
                CloseButton() {
                    viewModel.onDismiss.send()
                }
            }
            
            Text(GlanceLocalizedStringsConstants.shareAppDialogTitle)
                .glanceFont(GlanceFont.h2)
                .bold()
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(GlanceLocalizedStringsConstants.shareAppDialogSubtitle)
                .glanceFont(GlanceFont.bodyText)
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            GlanceConfirmationButtons(primaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogPrimaryButton,
                                      secondaryButtonText: GlanceLocalizedStringsConstants.shareAppDialogSecondaryButton,
                                      onPrimaryTapped: viewModel.onAccept,
                                      onSecondaryTapped: viewModel.onDismiss)
        }
    }
    
    var footerView: some View {
        VStack(spacing: Layout.Spacing.large) {
            LinkText(text: GlanceLocalizedStringsConstants.sessionStartDialogTermsLinkText,
                     url: GlanceLocalizedStringsConstants.shareAppDialogURL)
            
            GlanceLogoView()
        }
    }
}

public struct SessionAppShareDialogView_Previews: PreviewProvider {
    public static var previews: some View {
        DialogView {
            SessionAppShareDialogView(viewModel: SessionAppShareDialogViewModel())
        }
    }
}
