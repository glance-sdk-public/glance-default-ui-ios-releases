//
//  SessionStartDialogViewModel.swift
//  Glance_iOS
//
//  Created by Glance on 2/27/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import Foundation
import Combine
import SwiftUI
//#if canImport(GlanceSDK)
//import GlanceSDK
//#endif

final class SessionAppShareDialogViewModel: ObservableObject {
    
    let service: GlanceServiceType
    
    // MARK: - Properties
    
    @Published var termsUrl = ""
    
    // MARK: - View Events
    var onAccept = PassthroughSubject<Void, Never>()
    var onDismiss = PassthroughSubject<Void, Never>()
    
    
    var cancelSet = Set<AnyCancellable>()
    
    init(service: GlanceServiceType = GlanceService()) {
        self.service = service
        termsUrl = service.termsUrl
        
        onAccept
            .sink { [weak self] _ in
                let glanceStartParams = GlanceDefaultUI.shared.glanceStartParams
                let maxAgentSessionConnectAttempts = GlanceDefaultUI.shared.maxAgentSessionConnectAttempts
                let timeout = GlanceDefaultUI.shared.startSessionTimeout
                self?.service.startSession(
                    params: glanceStartParams,
                    timeout: timeout,
                    maxConnectAttempts: maxAgentSessionConnectAttempts
                )
                
            }
            .store(in: &cancelSet)
    }
    
}
