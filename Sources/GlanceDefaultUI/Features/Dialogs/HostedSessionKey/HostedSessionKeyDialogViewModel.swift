//
//  HostedSessionKeyDialogViewModel.swift
//  GlanceDefaultUI
//
//  Created by bruno on 11/29/24.
//

import Foundation
import Combine

final class HostedSessionKeyDialogViewModel: ObservableObject {
    let sessionKey: String
    
    init(sessionKey: String) {
        self.sessionKey = sessionKey
    }
    
    var onDismiss = PassthroughSubject<Void, Never>()
}

