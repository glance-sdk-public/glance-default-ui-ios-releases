//
//  HostedSessionKeyDialogView.swift
//  GlanceDefaultUI
//
//  Created by bruno on 11/29/24.
//

import SwiftUI

struct HostedSessionKeyDialogView: View {
    
    typealias Constants = GlanceLocalizedStringsConstants

    @ObservedObject var viewModel: HostedSessionKeyDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView
            }
            .padding([.all], Layout.Size.medium)
        }
    }
    
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            HStack {
                Spacer()
                
                CloseButton() {
                    viewModel.onDismiss.send()
                }
            }
            
            Text(Constants.dialogHostedSessionKeyTitle)
                .glanceFont(GlanceFont.h2)
                .bold()
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(viewModel.sessionKey)
                .glanceFont(GlanceFont.title)
                .bold()
                .foregroundColor(.basePrimary)
                .multilineTextAlignment(.center)
            
            GlanceLogoView()
        }
    }
}

#Preview {
    HostedSessionKeyDialogView(viewModel: HostedSessionKeyDialogViewModel(sessionKey: "6666"))
}
