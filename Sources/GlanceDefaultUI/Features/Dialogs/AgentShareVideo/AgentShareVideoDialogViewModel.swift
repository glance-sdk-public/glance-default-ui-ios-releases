//
//  AgentShareVideoDialogViewModel.swift
//
//
//  Created by bruno on 1/31/24.
//

import Foundation
import AVFoundation
import SwiftUI
import Combine
#if canImport(GlanceSDK)
import GlanceSDK
#endif

final class AgentShareVideoDialogViewModel: ObservableObject {
    
    // MARK: - Properties
    
    @Published var isAudioEnabled: Bool = false
    @Published var isMicEnabled: Bool = false
    @Published var isVideoEnabled: Bool = true
    @Published var isBlurEnabled: Bool = false
    @Published var captureSession: AVCaptureSession?
    
    var onAccept = PassthroughSubject<Void, Never>()
    var onDismiss = PassthroughSubject<Void, Never>()
    var onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>
    
    private var service: GlanceServiceType
    private var cancelSet = Set<AnyCancellable>()
    private var messageManager: MessageManagerType
    
    init(
        service: GlanceServiceType,
        onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>,
        messageManager: MessageManagerType
    ) {
        self.onUpdateCaptureSession = onUpdateCaptureSession
        self.service = service
        self.messageManager = messageManager
        
        onUpdateCaptureSession
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] captureSession in
                guard self?.captureSession == nil else { return }
                self?.captureSession = captureSession
            }
            .store(in: &cancelSet)
        
        $isVideoEnabled
            .receive(on: DispatchQueue.global(qos: .background))
            .sink { [weak self] isEnabled in
                guard let self else { return }
                if isEnabled {
                    self.captureSession?.startRunning()
                } else {
                    self.captureSession?.stopRunning()
                }
                self.messageManager.pause(!isEnabled)
            }
            .store(in: &cancelSet)
            
        onAccept
            .sink { [weak self] _ in
                guard let self else { return }
                GlanceDefaultUI.shared.isVisitorVideoEnabled = isVideoEnabled
                self.messageManager.pause(!isVideoEnabled)
                self.service.addVideo()
            }
            .store(in: &cancelSet)
    }
    
    func startVideoPreview() {
        self.service.startVideoPreview()
    }
}
