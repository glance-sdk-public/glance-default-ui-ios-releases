//
//  AgentShareVideoDialogView.swift
//  
//
//  Created by bruno on 1/31/24.
//

import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

struct AgentShareVideoDialogView: View {
    
    @ObservedObject var viewModel: AgentShareVideoDialogViewModel
    
    var body: some View {
        ZStack {
            Color.white
            
            VStack(spacing: .medium) {
                headerView
                
                contentView
                
                footerView
            }
            .padding(.all, .medium)
        }
        .onAppear {
            viewModel.startVideoPreview()
        }
    }
    
    var footerView: some View {
        VStack(spacing: .medium) {
            LinkText(text: GlanceLocalizedStringsConstants.sessionStartDialogTermsLinkText,
                     url: GlanceLocalizedStringsConstants.shareAppDialogURL)
            
            GlanceLogoView()
        }
    }
    
    var contentView: some View {
        VStack(spacing: .medium) {
            messageView
            GlanceConfirmationButtons(primaryButtonText: GlanceLocalizedStringsConstants.sessionStartDialogAcceptButtonText,
                                      secondaryButtonText: GlanceLocalizedStringsConstants.sessionStartDialogDeclineButtonText,
                                      onPrimaryTapped: viewModel.onAccept,
                                      onSecondaryTapped: viewModel.onDismiss)
        }
    }
    
    var messageView: some View {
        VStack(spacing: .small) {
            Text(GlanceLocalizedStringsConstants.sessionStartDialogHeaderTextAppShareAndVideo)
                .glanceFont(GlanceFont.h2)
                .foregroundColor(.black)
                .bold()
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(GlanceLocalizedStringsConstants.sessionStartDialogDescriptionTextInSessionAddVideo)
                .glanceFont(GlanceFont.bodyText)
                .foregroundColor(.black)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
    
    var headerView: some View {
        VStack(spacing: .small) {
            ZStack {
                ZStack {
                    RoundedRectangle(cornerRadius: 9)
                        .foregroundColor(.black)
                    
                    if let captureSession = viewModel.captureSession {
                        PreviewView(captureSession: captureSession)
                            .opacity(viewModel.isVideoEnabled ? 1 : 0)
                    }
                }
                .frame(width: 140, height: 140)

                HStack {
                    Spacer()
                    
                    VStack {
                        closeButtonView
                        
                        Spacer()
                    }
                }
            }
                        
            headerButtonsView
        }
    }
    
    var headerButtonsView: some View {
        HStack(spacing: .extraMedium) {
            Button {
                withAnimation {
                    $viewModel.isVideoEnabled.wrappedValue.toggle()
                }
            } label: {
                ZStack {
                    if $viewModel.isVideoEnabled.wrappedValue {
                        Circle()
                            .stroke()
                            .frame(width: 49, height: 49)
                    } else {
                        Circle()
                            .frame(width: 49, height: 49)
                            .shadow(color: .gray.opacity(0.1), radius: 3)
                    }
                    
                    Circle()
                        .frame(width: 49, height: 49)
                        .foregroundColor($viewModel.isVideoEnabled.wrappedValue ? .white : .basePrimary10)
                    
                    Image(glanceImage: $viewModel.isVideoEnabled.wrappedValue ? .glanceNewVideoOn : .glanceNewVideoOff)
                }
            }
        }
    }
    
    var closeButtonView: some View {
        CloseButton {
            viewModel.onDismiss.send()
        }
        .frame(height: 140, alignment: .topTrailing)
    }
}

struct AgentShareVideoDialogView_Previews: PreviewProvider {
    static var previews: some View {
        AgentShareVideoDialogView(viewModel: AgentShareVideoDialogViewModel(
            service: GlanceService(),
            onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>(),
            messageManager: MessageManager()
        ))
    }
}
