//
//  SessionShowCodeDialogViewModel.swift
//  
//
//  Created by Glance on 9/19/23.
//

import Foundation
import Combine

class SessionShowCodeDialogViewModel: ObservableObject {
    let sessionKey: String
    
    init(sessionKey: String) {
        self.sessionKey = sessionKey
    }
    
    var onDismiss = PassthroughSubject<Void, Never>()
}
