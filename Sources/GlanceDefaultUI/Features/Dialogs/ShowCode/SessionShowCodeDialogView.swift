//
//  SessionShowCodeDialogView.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI

struct SessionShowCodeDialogView: View {

    @ObservedObject var viewModel: SessionShowCodeDialogViewModel
    
    var body: some View {
        dialogView
    }
    
    var dialogView: some View {
        ZStack {
            Color.white
            
            VStack(spacing: Layout.Spacing.veryLarge) {
                contentView
            }
            .padding([.all], Layout.Size.medium)
        }
    }
    
    var contentView: some View {
        VStack(spacing: Layout.Spacing.medium) {
            HStack {
                Spacer()
                
                CloseButton() {
                    viewModel.onDismiss.send()
                }
            }
            
            Text(GlanceLocalizedStringsConstants.defaultUIAgentCodeText)
                .glanceFont(GlanceFont.h2)
                .bold()
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(viewModel.sessionKey)
                .glanceFont(GlanceFont.title)
                .bold()
                .foregroundColor(.basePrimary)
                .multilineTextAlignment(.center)
            
            GlanceLogoView()
        }
    }
}

struct SessionShowCodeDialogView_Previews: PreviewProvider {
    static var previews: some View {
        SessionShowCodeDialogView(viewModel: SessionShowCodeDialogViewModel(sessionKey: "6666"))
    }
}
