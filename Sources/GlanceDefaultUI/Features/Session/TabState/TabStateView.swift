//
//  TabStateView.swift
//  
//
//  Created by Glance on 10/9/23.
//

import SwiftUI

struct TabStateView: View {
    let completion: () -> Void
    @ObservedObject var viewModel = TabStateViewModel()
    
    var body: some View {
        ZStack(alignment: viewModel.direction == .left ? .trailing : .leading) {
            Button {
                completion()
            } label: {
                buttonLabel
            }
        }
        .frame(width: 30, height: 111)
    }
    
    var buttonLabel: some View {
        ZStack {
            Color
                .neutral800
                .edgesIgnoringSafeArea(.all)
            
            Image(systemName: viewModel.direction == .right ? "chevron.compact.left" : "chevron.compact.right")
                .resizable()
                .frame(width: 15, height: 25)
                .foregroundColor(.neutral300)
        }
        .cornerRadius(20, corners: viewModel.direction == .right ? [.topLeft, .bottomLeft] : [.topRight, .bottomRight])
    }
}

