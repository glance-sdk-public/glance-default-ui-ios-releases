//
//  TabStateViewModel.swift
//  
//
//  Created by Glance on 10/9/23.
//

import SwiftUI

final class TabStateViewModel: ObservableObject {
    enum Direction {
        case left, right
    }
    
    @Published var direction: Direction = .left
}
