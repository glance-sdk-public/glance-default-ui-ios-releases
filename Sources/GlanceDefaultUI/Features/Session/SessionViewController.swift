//
//  SessionViewController.swift
//
//
//  Created by Felipe Melo on 18/09/23.
//

import UIKit
import SwiftUI
import Combine
#if canImport(GlanceSDK)
import GlanceSDK
#endif

final class SessionViewController: UIViewController {
    
    enum ContainerLocation {
        case top, bottom, left, right
        
        func getPosition(for state: WidgetState, horizontalPadding: CGFloat = 12) -> CGFloat {
            let verticalPadding: CGFloat = 60
            let window = UIApplication.shared.windows.first
            
            switch self {
            case .top:
                return verticalPadding
            case .bottom:
                return UIScreen.main.bounds.height - verticalPadding - state.size.height
            case .left:
                let horizontalSafeArea = window?.safeAreaInsets.left ?? 0
                return horizontalPadding + horizontalSafeArea
            case .right:
                let horizontalSafeArea = window?.safeAreaInsets.right ?? 0
                return UIScreen.main.bounds.width - horizontalPadding - state.size.width - horizontalSafeArea
            }
        }
    }
    
    enum WidgetState {
        case appShare, tab, oneWayVideo, twoWayVideo, fullScreen
        
        var size: CGSize {
            switch self {
            case .appShare:
                return CGSizeMake(68, 111)
            case .oneWayVideo:
                return CGSizeMake(134, 255)
            case .twoWayVideo:
                return CGSizeMake(134, 429)
            case .tab:
                return CGSizeMake(30, 111)
            default:
                return .zero
            }
        }
    }
    
    var widgetView: UIView
    
    var previousState: WidgetState = .twoWayVideo
    var state: WidgetState = .twoWayVideo {
        didSet {
            widgetContainerView.state = state
        }
    }
    
    lazy var widgetContainerView: WidgetContainerView = {
        let widgetContainerView = WidgetContainerView()
        
        widgetContainerView.state = state
        widgetContainerView.backgroundColor = .clear
        widgetContainerView.isUserInteractionEnabled = true
        
        return widgetContainerView
    }()
    
    lazy var fullScreenConstraints: [NSLayoutConstraint] = {
        [
            widgetContainerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 14),
            widgetContainerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 14),
            widgetContainerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -14),
            widgetContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -34)
        ]
    }()
    
    private var isInLeft = false
    private var verticalLocation: ContainerLocation = .top
    private var horizontalLocation: ContainerLocation = .right
    private var sessionType: SessionType
    private var cancelSet = Set<AnyCancellable>()
    
    var onVisibilityChanged: PassthroughSubject<SessionViewController.WidgetState?, Never>
    var onLocationChanged: PassthroughSubject<(horizontal: ContainerLocation, vertical: ContainerLocation), Never>
    var onUpdateOrientation: PassthroughSubject<UIDeviceOrientation, Never>
    var showAgentVideo: PassthroughSubject<Void, Never>
    
    init(view: some View,
         state: SessionType,
         onVisibilityChanged: PassthroughSubject<SessionViewController.WidgetState?, Never>,
         onLocationChanged: PassthroughSubject<(horizontal: ContainerLocation, vertical: ContainerLocation), Never>,
         onUpdateOrientation: PassthroughSubject<UIDeviceOrientation, Never>,
         showAgentVideo: PassthroughSubject<Void, Never>) {
        widgetView = UIHostingController(rootView: view).view ?? UIView()
        widgetView.translatesAutoresizingMaskIntoConstraints = false
        widgetView.isUserInteractionEnabled = true
        widgetView.backgroundColor = .clear
        
        self.sessionType = state
        self.onVisibilityChanged = onVisibilityChanged
        self.onLocationChanged = onLocationChanged
        self.onUpdateOrientation = onUpdateOrientation
        self.showAgentVideo = showAgentVideo
        
        super.init(nibName: nil, bundle: Bundle(for: DialogsCoordinator.self))
//#if canImport(GlanceSDK)
//        super.init(nibName: nil, bundle: Bundle.module)
//#else
//        super.init(nibName: nil, bundle: Bundle(for: DialogsCoordinator.self))
//#endif
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let view = SessionScreenView(frame: UIScreen.main.bounds)
        view.widgetView = widgetContainerView
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(widgetContainerView)
        view.backgroundColor = .clear
        view.layer.borderColor = UIColor.orange.cgColor;
        view.layer.borderWidth = 4.0;
        
        state = getState(sessionType: self.sessionType)
        
        widgetContainerView.setup(widgetView: widgetView)
        widgetContainerView.center = view.center
        updateSize()
        
        let dragGesture = UIPanGestureRecognizer(target: self, action: #selector(handleWidgetDrag(_:)))
        widgetContainerView.addGestureRecognizer(dragGesture)
        
        previousState = state
        
        onVisibilityChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                guard let self else { return }
                if state == .tab {
                    guard !self.widgetContainerView.isShowingTab else { return }
                    self.previousState = self.state
                    self.state = .tab
                    self.widgetContainerView.collapseWidget(self.isInLeft ? .left : .right)
                } else {
                    guard self.state == .tab else { return }
                    self.state = self.previousState
                    self.previousState = .tab
                    self.widgetContainerView.showWidget()
                }
                self.updateLocations()
            }.store(in: &cancelSet)
        
        onLocationChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] location in
                guard let self else { return }
                
                self.isInLeft = location.horizontal == .left
                
                if self.state == .tab {
                    self.widgetContainerView.changeTabDirection(self.isInLeft ? .left : .right)
                }
                
                self.verticalLocation = location.vertical
                self.horizontalLocation = location.horizontal
                self.updateLocations()
            }.store(in: &cancelSet)
        
        showAgentVideo
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.state = .twoWayVideo
                self?.previousState = .twoWayVideo
                self?.updateSize()
                self?.updateLocations()
            }.store(in: &cancelSet)
        
        
        widgetContainerView
            .onTabTapped
            .sink { [weak self] _ in
                guard let self else { return }
                
                self.state = self.previousState
                self.previousState = .tab
                self.widgetContainerView.showWidget()
                self.sendUserMessage(.widgetVisibility("full"))
            }
            .store(in: &cancelSet)
        
        widgetContainerView
            .onUpdateLocations
            .sink { [weak self] _ in
                guard let self else { return }
                
                self.updateLocations()
            }
            .store(in: &cancelSet)
        
        updateLocations()
        
        GlanceDefaultUI
            .shared
            .isFullScreen
            .sink { [weak self] isFullScreen in
                guard let self else { return }
                
                let glanceStartParams = GlanceDefaultUI.shared.glanceStartParams
                if isFullScreen {
                    self.showFullScreen()
                } else {
                    let startState = getState(sessionType: SessionType.build(glanceStartParams.video))
                        if startState == .fullScreen {
                            self.state = .twoWayVideo
                        } else {
                            self.state = startState
                        }
                        NSLayoutConstraint.deactivate(self.fullScreenConstraints)
                        self.widgetContainerView.translatesAutoresizingMaskIntoConstraints = true
                        self.widgetContainerView.showWidget(shouldUpdateLocations: true, force: true)
                    
                }
            }
            .store(in: &cancelSet)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateLocations()
        coordinator.animate(alongsideTransition: nil) { [weak self] _ in
            self?.onUpdateOrientation.send(UIDevice.current.orientation)
        }
        view.layoutIfNeeded()
    }
    
    func getState(sessionType: SessionType) -> WidgetState {
        switch sessionType {
        case .screenshare:
            return .appShare
        case .oneWayVideo:
            return .oneWayVideo
        case .twoWayVideo:
            return .twoWayVideo
        case .fullScreen:
            return .fullScreen
        }
    }
    
    @objc private func handleWidgetDrag(_ sender: UIPanGestureRecognizer) {
        guard self.state != .fullScreen else { return }
        
        let padding = CGFloat.zero
        let newLocation: CGPoint = sender.location(in: view)
        
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else { return }
        let minX = padding
        let minY = window.safeAreaInsets.top
        let maxY = newLocation.y + widgetContainerView.frame.height / 2.0
        
        // Set next X
        var newX = newLocation.x
        if newLocation.x > (previousState.size.width / 2) + 12
            && newLocation.x < view.frame.width - (previousState.size.width / 2) - 12 {
            // Triggers when view is between the horizontal edges and is not tab
            //            let isNotCloseToEdges = newLocation.x > widgetContainerView.frame.size.width + 12 && newLocation.x < view.frame.width - widgetView.frame.size.width - 12
            if state == .tab {
                // Set window state if it's at tab state
                state = previousState
                previousState = .tab
                widgetContainerView.showWidget(shouldUpdateLocations: false)
            }
            
            if newLocation.x > view.frame.width / 2 {
                isInLeft = false
            } else {
                isInLeft = true
            }
            
            newX = newLocation.x
        } else {
            // Triggers when view it's at the horizontal edges
            let leftDiff = newLocation.x - minX
            let rightDiff = view.frame.width - newLocation.x
            
            if (leftDiff < rightDiff) {
                // Set left edge
                newX = minX + widgetContainerView.frame.width / 2.0
                isInLeft = true
            } else {
                // Set right edge
                newX = view.frame.width - widgetContainerView.frame.width / 2.0 - padding
                isInLeft = false
            }
            
            // Set tab state
            if state != .tab {
                previousState = state
            }
            
            state = .tab
            widgetContainerView.collapseWidget(isInLeft ? .left : .right)
            setHorizontalContainerLocation(horizontalLocation: isInLeft ? .left : .right)
        }
        
        self.sendUserMessage(.widgetVisibility(nil))
        
        // Set next Y
        var newY = newLocation.y
        if (((newLocation.y - widgetContainerView.frame.height / 2) >= minY)
            && (maxY <= view.frame.height - (window.safeAreaInsets.bottom))) {
            // Triggers when view is between the vertical edges
            newY = newLocation.y
        } else {
            // Triggers when view it's at the vertical edges
            let topDiff = newLocation.y - minY
            let bottomDiff = maxY - newLocation.y
            
            if (topDiff < bottomDiff) {
                // Set top edge
                newY = minY + widgetContainerView.frame.height / 2
            } else {
                // Set bottom edge
                newY = view.frame.height - widgetContainerView.frame.height / 2 - (window.safeAreaInsets.bottom)
            }
        }
        
        widgetContainerView.center = CGPoint(x: newX, y: newY)
        
        if sender.state == .ended {
            onDragGestureEnd()
        }
    }
    
    func onDragGestureEnd() {
        let screenSize = UIScreen.main.bounds
        verticalLocation = widgetContainerView.center.y <= (screenSize.height / 2.0) ? .top : .bottom
        horizontalLocation = widgetContainerView.center.x <= (screenSize.width / 2.0) ? .left : .right
        
        sendUserMessage(.widgetLocation)
        updateLocations()
    }
    
    func setContainerLocation(verticalLocation: ContainerLocation, horizontalLocation: ContainerLocation) {
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self else { return }
            let isShowingTab = self.state == .tab
            let padding: CGFloat = isShowingTab ? .zero : .medium
            self.widgetContainerView.frame.origin = CGPoint(x: horizontalLocation.getPosition(for: self.state, horizontalPadding: padding),
                                                            y: verticalLocation.getPosition(for: self.state))
        }
    }
    
    func setHorizontalContainerLocation(horizontalLocation: ContainerLocation) {
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self else { return }
            self.widgetContainerView.frame.origin.x = horizontalLocation.getPosition(for: self.state, horizontalPadding: .zero)
        }
    }
    
    private func updateSize() {
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self else { return }
            self.widgetContainerView.frame.size = self.state.size
            self.widgetView.frame.size = self.state.size
        }
    }
    
    private func updateLocations() {
        setContainerLocation(verticalLocation: verticalLocation,
                             horizontalLocation: horizontalLocation)
    }
    
    private func showFullScreen() {
        state = .fullScreen
        widgetContainerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(fullScreenConstraints)
        
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}

final class SessionScreenView: UIView {
    var widgetView: UIView? = nil
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let widgetView, widgetView.frame.contains(point) {
            let pointWithinView = CGPoint(x: point.x - widgetView.frame.origin.x, y: point.y - widgetView.frame.origin.y)
            return widgetView.hitTest(pointWithinView, with: event)
        }
        
        return nil
    }
}

extension SessionViewController {
    
    enum UserMessage {
        case widgetLocation
        case widgetVisibility(String?)
    }
    
    var widgetLocation: String {
        switch (verticalLocation, horizontalLocation) {
        case (.top, .left):
            return "topleft"
        case (.top, .right):
            return "topright"
        case (.bottom, .left):
            return "bottomleft"
        case (.bottom, .right):
            return "bottomright"
        default:
            return "topright"
        }
    }
    
    var widgetVisibility: String {
        widgetContainerView.isShowingTab ? "tab" : "full"
    }
    
    func sendUserMessage(_ message: UserMessage) {
        switch message {
        case .widgetLocation:
            GlanceDefaultUI.shared.messageManager.widgetLocationUpdated(widgetLocation)
        case let .widgetVisibility(visibility):
            GlanceDefaultUI.shared.messageManager.widgetVisibilityUpdated(visibility ?? widgetVisibility)
        }
    }
}
