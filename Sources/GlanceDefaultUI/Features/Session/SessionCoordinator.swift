//
//  SessionCoordinator.swift
//  
//
//  Created by Felipe Melo on 18/09/23.
//

import UIKit
import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

@objc final public class SessionCoordinator: NSObject {
    
    private var cancelSet = Set<AnyCancellable>([])
    private lazy var sessionWindow: SessionWindow = {
        SessionWindow()
    }()
    private let dialogsCoordinator: DialogsCoordinator
    private let service: GlanceServiceType
    private let messageManager: MessageManagerType
    
    var onUpdateImage = PassthroughSubject<UIImage, Never>()
    var onUpdateAgentViewer = PassthroughSubject<UIView?, Never>()
    var onUpdateCaptureSession = PassthroughSubject<AVCaptureSession, Never>()
    var onUpdateOrientation = PassthroughSubject<UIDeviceOrientation, Never>()
    var showAgentVideo = PassthroughSubject<Void, Never>()
    
    var onPausedVisitorVideo = PassthroughSubject<Void, Never>()
    var onVisibilityChanged = PassthroughSubject<SessionViewController.WidgetState?, Never>()
    var onLocationChanged = PassthroughSubject<(horizontal: SessionViewController.ContainerLocation, vertical: SessionViewController.ContainerLocation), Never>()
    var onSizeChanged = PassthroughSubject<String, Never>()
    
    public init(
        dialogsCoordinator: DialogsCoordinator,
        service: GlanceServiceType,
        messageManager: MessageManagerType
    ) {
        self.dialogsCoordinator = dialogsCoordinator
        self.service = service
        self.messageManager = messageManager
    }
    
    public func start(sessionType: SessionType = .screenshare) {
        let viewModel = SessionViewModel(sessionType: sessionType,
                                         onPausedVisitorVideo: onPausedVisitorVideo,
                                         onUpdateAgentViewer: onUpdateAgentViewer,
                                         onUpdateCaptureSession: onUpdateCaptureSession,
                                         onUpdateImage: onUpdateImage,
                                         onUpdateOrientation: onUpdateOrientation,
                                         onSizeChanged: onSizeChanged,
                                         showAgentVideo: showAgentVideo,
                                         messageManager: messageManager)
        let view = SessionView(viewModel: viewModel)
        
        viewModel
            .events
            .sink { [weak self] event in
                switch event {
                case .onFinishedSession:
                    self?.dialogsCoordinator.presentConfirmationDialog { [weak self] in
                        self?.endSession()
                    }
                }
            }
            .store(in: &cancelSet)
        
        present(view: view, state: sessionType)
    }
    
    func endSession() {
        if sessionWindow.isShowing {
            service.endSession()
            sessionWindow.hide()
        }
    }
    
    private func present(view: some View, state: SessionType) {
        sessionWindow.rootViewController = SessionViewController(view: view,
                                                                 state: state,
                                                                 onVisibilityChanged: onVisibilityChanged,
                                                                 onLocationChanged: onLocationChanged,
                                                                 onUpdateOrientation: onUpdateOrientation,
                                                                 showAgentVideo: showAgentVideo)
        sessionWindow.show()
    }
    
}
