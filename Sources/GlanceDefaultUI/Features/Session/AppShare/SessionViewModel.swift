//
//  SessionAppShareViewModel.swift
//  
//
//  Created by Felipe Melo on 19/09/23.
//

import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
import GlanceCore
#endif

public enum SessionType {
    case screenshare, twoWayVideo, oneWayVideo, fullScreen
    
    static func build(_ videoMode: GlanceVideoMode) -> SessionType {
        switch videoMode {
        case VideoOff:
            return .screenshare
        case VideoSmallVisitor:
            return .oneWayVideo
        case VideoSmallMultiway:
            return .twoWayVideo
        case VideoLargeVisitor, VideoLargeMultiway:
            return .fullScreen
        default:
            return .screenshare
        }
    }
}

final class SessionViewModel: ObservableObject {
    
    enum Event {
        case onFinishedSession
    }
    
    @Published var sessionType: SessionType = .screenshare
    @Published var agentViewer: UIView? = nil
    @Published var captureSession: AVCaptureSession?
    @Published var previewLayer: AVCaptureVideoPreviewLayer?
    @Published var visitorImage: UIImage = UIImage()
    @Published var isVideoEnabled = GlanceDefaultUI.shared.isVisitorVideoEnabled
    @Published var isFullScreen = false
    
    var events = PassthroughSubject<Event, Never>()
    var onPausedVisitorVideo: PassthroughSubject<Void, Never>
    var onUpdateAgentViewer: PassthroughSubject<UIView?, Never>
    var onUpdateOrientation: PassthroughSubject<UIDeviceOrientation, Never>
    var onUpdateCaptureSession = PassthroughSubject<AVCaptureSession, Never>()
    var onUpdateImage = PassthroughSubject<UIImage, Never>()
    var onSizeChanged = PassthroughSubject<String, Never>()
    
    var canShowAgentVideo: Bool {
        sessionType != .screenshare && sessionType != .oneWayVideo
    }
    
    private let messageManager: MessageManagerType

    private var cancelSet = Set<AnyCancellable>()
    
    init(sessionType: SessionType,
         onPausedVisitorVideo: PassthroughSubject<Void, Never>,
         onUpdateAgentViewer: PassthroughSubject<UIView?, Never>,
         onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>,
         onUpdateImage: PassthroughSubject<UIImage, Never>,
         onUpdateOrientation: PassthroughSubject<UIDeviceOrientation, Never>,
         onSizeChanged: PassthroughSubject<String, Never>,
         showAgentVideo: PassthroughSubject<Void, Never>,
         messageManager: MessageManagerType) {
        self.sessionType = sessionType
        self.onPausedVisitorVideo = onPausedVisitorVideo
        self.onUpdateOrientation = onUpdateOrientation
        self.onUpdateAgentViewer = onUpdateAgentViewer
        self.onUpdateImage = onUpdateImage
        self.onSizeChanged = onSizeChanged
        self.messageManager = messageManager
        
        if sessionType == .fullScreen {
            isFullScreen = true
        }
        
        $isVideoEnabled
            .delay(for: 0.3, scheduler: DispatchQueue.global())
            .sink { [weak self] isEnabled in
                guard let self else { return }
                self.messageManager.pause(!isEnabled)
                if isEnabled {
                    self.captureSession?.startRunning()
                } else {
                    self.captureSession?.stopRunning()
                }
            }.store(in: &cancelSet)
        
        $isFullScreen
            .dropFirst()
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .map({ [weak self] value in
                guard let self else { return value }
                self.sessionType = value ? .fullScreen : .twoWayVideo
                self.messageManager.visitorSizeUpdated(value ? .large : .small)
                return value
            })
            .subscribe(GlanceDefaultUI.shared.isFullScreen)
            .store(in: &cancelSet)
        
        onPausedVisitorVideo
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let self else { return }
                self.isVideoEnabled = false
            }.store(in: &cancelSet)
        
        onUpdateAgentViewer
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] agentViewer in
                guard let self else { return }
                if self.sessionType == .screenshare {
                    isVideoEnabled = GlanceDefaultUI.shared.isVisitorVideoEnabled
                    self.isFullScreen = false
                }
                self.agentViewer = agentViewer
            }.store(in: &cancelSet)
        
        onUpdateCaptureSession
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] captureSession in
                guard self?.captureSession == nil else { return }
                self?.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                self?.captureSession = captureSession
            }
            .store(in: &cancelSet)
        
        onUpdateOrientation
            .sink { [weak self] orientation in
                guard let self else { return }
                
                let previewOrientation: AVCaptureVideoOrientation = switch orientation {
                case UIDeviceOrientation.landscapeLeft:
                    AVCaptureVideoOrientation.landscapeRight
                case UIDeviceOrientation.landscapeRight:
                    AVCaptureVideoOrientation.landscapeLeft
                default:
                    AVCaptureVideoOrientation.portrait
                }
                
                previewLayer?.connection?.videoOrientation = previewOrientation
            }
            .store(in: &cancelSet)
        
        onUpdateImage
            .delay(for: 0.1, scheduler: DispatchQueue.main)
            .sink { [weak self] image in
                self?.visitorImage = image.withHorizontallyFlippedOrientation()
            }
            .store(in: &cancelSet)
        
        onSizeChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] size in
                if size == "large" {
                    self?.isFullScreen = true
                } else if size == "small" {
                    self?.isFullScreen = false
                }
            }
            .store(in: &cancelSet)
        
        showAgentVideo
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.sessionType = .twoWayVideo
            }
            .store(in: &cancelSet)
    }
    
    func didTapEndSession() {
        events.send(.onFinishedSession)
    }
}
