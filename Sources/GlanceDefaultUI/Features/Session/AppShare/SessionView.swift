//
//  SessionAppShareView.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 03/04/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

struct SessionView: View {
    @ObservedObject var viewModel: SessionViewModel
    
    var body: some View {
        switch viewModel.sessionType {
        case .screenshare:
            screenShareView
        case .oneWayVideo, .twoWayVideo:
            videoView
        case .fullScreen:
            fullScreenView
        }
    }
    
    @State private var customViewSize: CGSize = CGSize(width: 124, height: 195)
    
    var screenShareView: some View {
        ZStack {
            Color.neutral800
            
            VStack(spacing: Layout.Spacing.extraSmall) {
                visitorDefaultView
                
                HangupButton {
                    viewModel.didTapEndSession()
                }
            }
            .padding(4)
        }
        .cornerRadius(8)
        .frame(width: 68, height: 111)
    }
    
    var agentDefaultView: some View {
        Image(glanceImage: .glanceNewHeadphones)
            .resizableFrame(width: 59, height: 59)
            .foregroundColor(.white)
            .background(
                RoundedRectangle(cornerRadius: 6)
                    .foregroundColor(.basePrimary)
            )
    }
    
    var visitorDefaultView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 6)
                .foregroundColor(.basePrimary)
            
            Image(glanceImage: .glanceNewHeadphones)
                .resizableFrame(width: 26, height: 24)
                .foregroundColor(.white)
        }
    }
        
    var videoView: some View {
        SessionVideoView(viewModel: viewModel)
    }
        
    var fullScreenView: some View {
        FullScreenView(viewModel: viewModel)
    }
}

struct SessionView_Previews: PreviewProvider {
    static var previews: some View {
        SessionView(viewModel: .mock())
        
        SessionView(viewModel: .mock())
    }
}
