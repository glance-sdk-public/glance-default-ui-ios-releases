//
//  SessionTwoWayVideoView.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 03/04/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI
import Combine
import UIKit
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

struct SessionVideoView: View {
    enum Constants {
        static let videosVerticalSpacing: CGFloat = 10
        static let controlButtonsOffset: CGFloat = -20
        static let frameWidth: CGFloat = 134
        static let minimizeButton: (oppacity: CGFloat, circleFrame: CGFloat) = (0.5, 44)
        static let controlButtonFrame: CGFloat = 44
    }
    
    @ObservedObject var viewModel: SessionViewModel
    
    @State var isAudioAdjust = false
    @State var isAudioMuted = false
    @State var isMicMuted = false
    
    @State var hasMicAndAudio = false
    @State var hasSelect = false
    @State var hasVideo = true
    
    @State var removeButtons = true
    
    @State private var customViewSize: CGSize = CGSize(width: 124, height: 195)
    
    private var hasControlButtons: Binding<Bool> {
        Binding (
            get: { hasMicAndAudio || hasSelect || hasVideo },
            set: { _ in }
        )
    }
    
    private var visitorViewHeight: Binding<CGFloat> {
        Binding (
            get: { viewModel.sessionType == .oneWayVideo ? 195 : 124 },
            set: { _ in }
        )
    }
    
    var body: some View {
        VStack(spacing: .zero) {
            Group {
                if viewModel.canShowAgentVideo {
                    agentVideoView
                }
                
                visitorVideoView
            }
            .onTapGesture {
                withAnimation {
                    removeButtons.toggle()
                }
            }
            
            
            if removeButtons {
                if hasControlButtons.wrappedValue {
                    buildControlButtonsView
                        .offset(y: Constants.controlButtonsOffset)
                }
                
                HangupButton {
                    viewModel.didTapEndSession()
                }
                .padding(.vertical, hasControlButtons.wrappedValue ? .zero : .small)
                .offset(y: hasControlButtons.wrappedValue ? .small.negative() : .zero)
            }
        }
        .padding(.extraSmall)
        .frame(width: Constants.frameWidth)
        .background(
            RoundedRectangle(cornerRadius: .small)
                .fill(.black)
        )
    }
    
    @ViewBuilder
    var agentVideoView: some View {
        ZStack(alignment: .topTrailing) {
            if let agentViewer = viewModel.agentViewer {
                AgentViewerView(frameSize: $customViewSize, agentViewer: agentViewer)
                    .frame(width: 124, height: 195)
            } else {
                agentDefaultView
            }
            
            minimizeOrMaximizeButton(isActivate: $viewModel.isFullScreen,
                                     glanceImageHighlighted: .glanceNewMaximize,
                                     glanceImage: .glanceNewMinimize) {
                withAnimation {
                    viewModel.isFullScreen.toggle()
                }
            }
        }
    }
    
    var agentDefaultView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: .extraSmall)
                .foregroundColor(.secondaryColor)
            
            Circle()
                .frame(width: 60, height: 60)
                .foregroundColor(.basePrimary)
            
            Image(glanceImage: .glanceNewHeadphones)
                .resizableFrame(width: 24, height: 24)
                .foregroundColor(.white)
        }
        .frame(width: 124, height: 195)
    }
    
    @ViewBuilder
    var visitorVideoView: some View {
        if viewModel.isVideoEnabled,
           let captureSession = viewModel.captureSession {
            ZStack {
                Image(uiImage: viewModel.visitorImage)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 124,
                           height: visitorViewHeight.wrappedValue)
                    .cornerRadius(.small)
                
                PreviewView(captureSession: captureSession, previewLayer: viewModel.previewLayer)
                    .opacity(captureSession.isRunning ? 1 : 0)
                    .frame(width: 124, height: visitorViewHeight.wrappedValue)
            }
            .padding(.top, Constants.videosVerticalSpacing)
        } else {
            visitorDefaultView
                .padding(.top, Constants.videosVerticalSpacing)
        }
    }
    
    var visitorDefaultView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: .extraSmall)
                .foregroundColor(.secondaryColor)
            
            Circle()
                .frame(width: 60, height: 60)
                .foregroundColor(.basePrimary)
            
            Image(glanceImage: .glanceNewVisitor)
                .resizableFrame(width: 24, height: 24)
                .foregroundColor(.white)
        }
        .frame(width: 124, height: visitorViewHeight.wrappedValue)
    }
    
    private func minimizeOrMaximizeButton(isActivate: Binding<Bool>,
                                          glanceImageHighlighted: Image.GlanceImages, glanceImage: Image.GlanceImages,
                                          completion: @escaping () -> ()) -> some View {
        Button {
            completion()
        } label: {
            ZStack {
                Circle()
                    .frame(width: Constants.minimizeButton.circleFrame,
                           height: Constants.minimizeButton.circleFrame)
                    .foregroundColor(.black)
                    .opacity(Constants.minimizeButton.oppacity)
                
                Image(glanceImage: isActivate.wrappedValue ? glanceImage : glanceImageHighlighted)
                    .foregroundColor(.white)
            }
        }
        
    }
    
    private var buildControlButtonsView: some View {
        VStack(spacing: .extraSmall) {
            if hasSelect || hasVideo {
                HStack(spacing: .medium) {
                    if hasSelect {
                        ControlButton(isActivate: $isAudioAdjust,
                                      glanceImageHighlighted: .glanceNewBlurOn,
                                      glanceImage: .glanceNewBlurOff) {
                            isAudioAdjust.toggle()
                        }
                    }
                    
                    if hasVideo {
                        ControlButton(isActivate: $viewModel.isVideoEnabled,
                                      glanceImageHighlighted: .glanceNewVideoOn,
                                      glanceImage: .glanceNewVideoOff) {
                            viewModel.isVideoEnabled.toggle()
                        }
                    }
                }
                .frame(height: Constants.controlButtonFrame)
            }
            
            if hasMicAndAudio {
                HStack(spacing: .medium) {
                    ControlButton(isActivate: $isAudioMuted,
                                  glanceImageHighlighted: .glanceNewSpeakerphoneOn,
                                  glanceImage: .glanceNewSpeakerphoneOff) {
                        isAudioMuted.toggle()
                    }
                    
                    ControlButton(isActivate: $isMicMuted,
                                  glanceImageHighlighted: .glanceNewMicrophoneOn,
                                  glanceImage: .glanceNewMicrophoneOff) {
                        isMicMuted.toggle()
                    }
                }
                .frame(height: Constants.controlButtonFrame)
            }
        }
    }
}


struct SessionTwoWayVideoView_Previews: PreviewProvider {
    static var previews: some View {
        SessionVideoView(viewModel: .mock())
    }
}

struct AgentViewerView: UIViewRepresentable {
    @Binding var frameSize: CGSize
    var agentViewer: UIView
    
    func makeUIView(context: Context) -> some UIView {
        let customView = UIView(frame: .zero)
        return customView
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        uiView.addSubview(agentViewer)
        uiView.translatesAutoresizingMaskIntoConstraints = false
        uiView.frame.size = CGSizeMake(frameSize.width, frameSize.height)
        uiView.contentMode = .scaleToFill
        uiView.clipsToBounds = true
        agentViewer.frame.size = CGSizeMake(frameSize.width, frameSize.height)
    }
}

struct PreviewView: UIViewRepresentable {
    weak var captureSession: AVCaptureSession?
    weak var previewLayer: AVCaptureVideoPreviewLayer?
    
    func makeUIView(context: Context) -> some UIView {
        let captureSession = self.captureSession ?? AVCaptureSession()
        let previewLayer = self.previewLayer ?? AVCaptureVideoPreviewLayer(session: captureSession)
        let preview = GlanceVideoCapturePreviewView(previewLayer: previewLayer)
        let frameSize = preview.superview?.frame.size ?? .zero
        preview.frame.size = CGSizeMake(frameSize.width, frameSize.height)
        
        return preview
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}
