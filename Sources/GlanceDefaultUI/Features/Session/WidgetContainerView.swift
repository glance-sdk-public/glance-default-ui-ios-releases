//
//  WidgetContainerView.swift
//  
//
//  Created by bruno on 2/6/24.
//

import UIKit
import SwiftUI
import Combine

final class WidgetContainerView: UIView {
    
    // MARK: - Events
    
    let onTabTapped = PassthroughSubject<Void, Never>()
    let onUpdateLocations = PassthroughSubject<Void, Never>()
    
    // MARK: - Properties
    
    var widgetView: UIView?
    var state: SessionViewController.WidgetState?
    
    lazy var tabView: TabStateView = {
        let view = TabStateView() { [weak self] in
            self?.onTabTapped.send()
        }
        
        return view
    }()
    
    lazy var tabUIView: UIView = {
        let uiView = UIHostingController(rootView: tabView).view ?? UIView()
        uiView.translatesAutoresizingMaskIntoConstraints = false
        uiView.isUserInteractionEnabled = true
        uiView.backgroundColor = .clear
        return uiView
    }()
    
    var isShowingTab: Bool {
        !tabUIView.isHidden
    }
    
    private var direction: TabStateViewModel.Direction = .left
    
    func showWidget(shouldUpdateLocations: Bool = true, force: Bool = false) {
        guard isShowingTab || force else { return }
        
        self.widgetView?.alpha = 0
        self.widgetView?.isHidden = false
        
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self,
                  let state = self.state else { return }
            
            self.widgetView?.alpha = 1
            self.tabUIView.alpha = 0
            self.widgetView?.frame.size = state.size
            self.frame.size = state.size
            self.layoutIfNeeded()
        } completion: { [weak self] _ in
            guard let self else { return }
            
            self.tabUIView.isHidden = true
        }
        
        
        if shouldUpdateLocations {
            onUpdateLocations.send()
        }
    }
    
    func changeTabDirection(_ direction: TabStateViewModel.Direction) {
        self.direction = direction
        self.tabView.viewModel.direction = direction
    }
    
    func collapseWidget(_ direction: TabStateViewModel.Direction) {
        guard !isShowingTab else { return }
        
        self.direction = direction
        self.tabView.viewModel.direction = direction
        
        self.tabUIView.isHidden = false
        self.tabUIView.alpha = 0
        
        UIView.animate(withDuration: 0.25) { [weak self] in
            guard let self,
                  let state = self.state else { return }
            self.widgetView?.alpha = 0
            self.tabUIView.alpha = 1
            self.frame.size = state.size
        } completion: { [weak self] _ in
            guard let self,
                  let state = self.state else { return }
            if( state == .tab ){
                self.widgetView?.isHidden = true
            }
        }
    }
    
    func setup(widgetView: UIView) {
        self.widgetView = widgetView
        addSubview(widgetView)
        addSubview(tabUIView)
        tabUIView.isHidden = true
        
        NSLayoutConstraint.activate([
            widgetView.topAnchor.constraint(equalTo: topAnchor),
            widgetView.leadingAnchor.constraint(equalTo: leadingAnchor),
            widgetView.trailingAnchor.constraint(equalTo: trailingAnchor),
            widgetView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tabUIView.topAnchor.constraint(equalTo: topAnchor),
            tabUIView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tabUIView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tabUIView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        setNeedsLayout()
    }
}
