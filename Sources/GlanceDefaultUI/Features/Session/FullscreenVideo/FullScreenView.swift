//
//  FullScreenView.swift
//  
//
//  Created by Glance on 9/29/23.
//

import SwiftUI
import Combine
import AVFoundation
#if canImport(GlanceSDK)
import GlanceSDK
#endif

struct FullScreenView: View {
    enum Constants {
        static let minimizeButton: (oppacity: CGFloat, circleFrame: CGFloat) = (0.5, 44)
    }
    
    @ObservedObject var viewModel: SessionViewModel
    
    @State var isVideo = true
    @State var isBlur = false
    @State var isAudioAdjust = false
    @State var isCameraRotated = true
    
    @State var isAudioMuted = false
    @State var isMicMuted = false
    
    @State var isShowingAgent = true
    
    @State private var customViewSize: CGSize = CGSize(width: 314, height: 628)
    
    var body: some View {
        VStack(spacing: .zero) {
            minimizeOrMaximizeButton(isActivate: $viewModel.isFullScreen,
                                     glanceImageHighlighted: .glanceNewMaximize,
                                     glanceImage: .glanceNewMinimize) {
                withAnimation {
                    viewModel.isFullScreen.toggle()
                }
            }
            .frame(maxWidth: .infinity, alignment: .trailing)
            
            
            buildCamerasView
                .padding(.vertical)
            
            HStack {
                Spacer()
                
                ControlButton(isActivate: $viewModel.isVideoEnabled, glanceImageHighlighted: .glanceNewVideoOn, glanceImage: .glanceNewVideoOff) {
                    viewModel.isVideoEnabled.toggle()
                }
                
                Spacer()
                
                ControlButton(isActivate: $isCameraRotated, glanceImageHighlighted: .glanceNewFlipCamera, glanceImage: .glanceNewFlipCamera) {
                    isCameraRotated.toggle()
                }
                .opacity(0)
                
                Spacer()
                
                HangupButton {
                    viewModel.didTapEndSession()
                }
                
                Spacer()
            }
            .frame(height: 44)
            .padding(.bottom, 15)
        }
        .background(RoundedRectangle(cornerRadius: 10)
            .fill(.gray))
        .padding(.horizontal, 16)
    }
    
    @ViewBuilder
    private var buildCamerasView: some View {
        if isShowingAgent {
            ZStack(alignment: .bottomLeading) {
                agentDefaultView
                
                visitorDefaultView
            }
            .padding(8)
        } else {
            ZStack(alignment: .topLeading) {
                Image("CameraImage")
                    .resizable()
                
                Image("AgentImage")
                    .resizable()
                    .frame(width: 105, height: 105)
                    .offset(y: -30)
                    .onTapGesture {
                        withAnimation(.easeInOut) {
                            isShowingAgent.toggle()
                        }
                    }
            }
            .padding(8)
        }
    }
    
    private var buildShowAudioDetails: some View {
        ZStack {
            if isAudioAdjust {
                VStack(spacing: 16) {
                    ControlButton(isActivate: $isBlur, glanceImageHighlighted: .glanceNewBlurOff, glanceImage: .glanceNewBlurOn) {
                        isBlur.toggle()
                    }
                    
                    ControlButton(isActivate: $isAudioMuted, glanceImageHighlighted: .glanceNewSpeakerphoneOff, glanceImage: .glanceNewSpeakerphoneOff) {
                        isAudioMuted.toggle()
                    }
                    
                    ControlButton(isActivate: $isMicMuted, glanceImageHighlighted: .glanceNewMicrophoneOff, glanceImage: .glanceNewMicrophoneOn) {
                        isMicMuted.toggle()
                    }
                }
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(.black)
                        .padding(-10)
                )
                .offset(y: -120)
            }
            
            ControlButton(isActivate: $isAudioAdjust, glanceImageHighlighted: .glanceNewSettings,  glanceImage: .glanceNewSettings) {
                isAudioAdjust.toggle()
            }
            .frame(height: 44)
        }
    }
    
    private func minimizeOrMaximizeButton(isActivate: Binding<Bool>,
                                          glanceImageHighlighted: Image.GlanceImages, glanceImage: Image.GlanceImages,
                                          completion: @escaping () -> ()) -> some View {
        Button {
            completion()
        } label: {
            ZStack {
                Circle()
                    .frame(width: Constants.minimizeButton.circleFrame,
                           height: Constants.minimizeButton.circleFrame)
                    .foregroundColor(.black)
                    .opacity(Constants.minimizeButton.oppacity)
                
                Image(glanceImage: isActivate.wrappedValue ? glanceImage : glanceImageHighlighted)
                    .foregroundColor(.white)
            }
        }

    }

    private var agentDefaultView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: .extraSmall)
                .foregroundColor(.secondaryColor)
            
            if let agentViewer = viewModel.agentViewer {
                AgentViewerView(frameSize: $customViewSize, agentViewer: agentViewer)
            } else {
                agentImage
            }
        }
    }
    
    private var agentImage: some View {
        ZStack {
            Circle()
                .frame(width: 150, height: 150)
                .foregroundColor(.basePrimary)
            
            Image(glanceImage: .glanceNewHeadphones)
                .resizableFrame(width: 50, height: 50)
                .scaledToFill()
                .foregroundColor(.white)
        }
    }
    
    private var visitorDefaultView: some View {
        ZStack {
            if viewModel.isVideoEnabled,
               let captureSession = viewModel.captureSession {
                ZStack {
                    Image(uiImage: viewModel.visitorImage)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 105, height: 105)
                        .cornerRadius(.small)
                    
                    PreviewView(captureSession: captureSession)
                        .opacity(captureSession.isRunning ? 1 : 0)
                        .frame(width: 105, height: 105)
                }
            } else {
                RoundedRectangle(cornerRadius: .extraSmall)
                    .foregroundColor(.secondaryColor)
                
                Circle()
                    .frame(width: 60, height: 60)
                    .foregroundColor(.basePrimary)
                
                Image(glanceImage: .glanceNewVisitor)
                    .resizableFrame(width: 24, height: 24)
                    .foregroundColor(.white)
            }
        }
        .frame(width: 105, height: 105)
        .offset(y: 30)
    }
    
}

struct FullscreenView_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenView(viewModel: .mock())
    }
}

struct ControlButton: View {
    
    @Binding var isActivate: Bool
    
    let glanceImageHighlighted: Image.GlanceImages
    let glanceImage: Image.GlanceImages
    let completion: () -> Void
    
    init(isActivate: Binding<Bool>, glanceImageHighlighted: Image.GlanceImages, glanceImage: Image.GlanceImages, completion: @escaping () -> Void) {
        self._isActivate = isActivate
        self.glanceImageHighlighted = glanceImageHighlighted
        self.glanceImage = glanceImage
        self.completion = completion
    }
    
    var body: some View {
        Button {
            completion()
        } label: {
            ZStack {
                Circle()
                    .frame(width: 44, height: 44)
                    .foregroundColor($isActivate.wrappedValue ? .secondaryColor : .white)
                
                Image(glanceImage: $isActivate.wrappedValue ? glanceImageHighlighted : glanceImage)
                    .foregroundColor($isActivate.wrappedValue ? .white : .secondaryColor)
            }
        }
    }
}

extension Color {
    static let secondaryColor = Color(red: 61 / 255, green: 61 / 255, blue: 61 / 255)
    static let primaryRed = Color(red: 153 / 255, green: 27 / 255, blue: 27 / 255)
}
