//
//  SessionWindow.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 28/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import UIKit

final class SessionWindow: UIWindow {
    
    init() {
        if let scene = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.windowScene {
            super.init(windowScene: scene)
        } else {
            super.init(frame: UIScreen.main.bounds)
        }
        
        
        windowLevel = UIWindow.Level.alert
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if let presentedViewController = rootViewController?.presentedViewController {
            let alertFrame = presentedViewController.view.frame
            if alertFrame.contains(point) {
                let pointWithinView = CGPoint(x: point.x - alertFrame.origin.x,
                                              y: point.y - alertFrame.origin.y)
                return presentedViewController.view.hitTest(pointWithinView, with: event)
            }
        }
        
        return rootViewController?.view.hitTest(point, with: event)
    }
    
    var isShowing: Bool {
        !isHidden
    }
    
    func show() {
        alpha = 0
        isHidden = false
        makeKeyAndVisible()
        
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.alpha = 1
        } completion: { [weak self] finished in
            guard let self else { return }

            if finished {
                UIAccessibility.post(notification: UIAccessibility.Notification.screenChanged, argument: self.rootViewController)
            }
        }
    }
    
    func hide(completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.25) { [weak self] in
            self?.alpha = 0
        } completion: { [weak self] finished in
            if finished {
                self?.resignKey()
                self?.isHidden = true
                completion?()
            }
        }
    }
    
}
