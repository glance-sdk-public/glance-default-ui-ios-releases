//
//  GlanceLocalizedStringsConstants.swift
//
//
//  Created by bruno on 2/8/24.
//

import Foundation

public struct GlanceLocalizedStringsConstants {
    static func localizedString(forKey key: String) -> String {
        var moduleBundle: Bundle
        
        
        let localizedStringFromMainBundle = NSLocalizedString(key, comment: "Localizable")
        if localizedStringFromMainBundle != key {
            return localizedStringFromMainBundle
        }
        #if canImport(GlanceFramework)
        moduleBundle = Bundle(for: DialogsCoordinator.self)
        #else
        moduleBundle = Bundle.module
        #endif
        
        return NSLocalizedString(key, bundle: moduleBundle, comment: "Localizable")
    }
    
    public static let defaultUIPresenceDialogTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TITLE")
    public static let defaultUIPresenceDialogText = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TEXT")
    public static let defaultUIPresenceDialogNoButtonTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_NO_BUTTON_TITLE")
    public static let defaultUIPresenceDialogYesButtonTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_YES_BUTTON_TITLE")
    public static let defaultUIPresenceDialogTermsText = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TERMS_TEXT")
    public static let defaultUIPresenceDialogTextVideo = localizedString(forKey: "GLANCE_DEFAULT_UI_PRESENCE_DIALOG_TEXT_VIDEO")
    public static let nonDefaultUIPresenceDialogTitle = localizedString(forKey: "GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_TITLE")
    public static let nonDefaultUIPresenceDialogNoButtonTitle = localizedString(forKey: "GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_NO_BUTTON_TITLE")
    public static let nonDefaultUIPresenceDialogYesButtonTitle = localizedString(forKey: "GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_YES_BUTTON_TITLE")
    public static let nonDefaultUIPresenceDialogShowTermsButtonTitle = localizedString(forKey: "GLANCE_NON_DEFAULT_UI_PRESENCE_DIALOG_SHOW_TERMS_BUTTON_TITLE")
    public static let defaultUIStartDialogTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_TITLE")
    public static let defaultUIStartDialogSubtitle = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_SUBTITLE")
    public static let defaultUIStartDialogTitleAgent = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_TITLE_AGENT")
    public static let defaultUIStartDialogPhoneText = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_PHONE_TEXT")
    public static let defaultUIStartDialogHeadsetText = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_HEADSET_TEXT")
    public static let defaultUIStartDialogDrawText = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_DRAW_TEXT")
    public static let defaultUIStartDialogVideoText = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_VIDEO_TEXT")
    public static let defaultUIStartDialogCancelButtonTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_CANCEL_BUTTON_TITLE")
    public static let defaultUIStartDialogAcceptButtonTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_ACCEPT_BUTTON_TITLE")
    public static let defaultUIStartDialogTermsText = localizedString(forKey: "GLANCE_DEFAULT_UI_START_DIALOG_TERMS_TEXT")
    public static let videoDefaultUIStartDialogText = localizedString(forKey: "GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_TEXT")
    public static let videoDefaultUIStartDialogCancelButtonTitle = localizedString(forKey: "GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_CANCEL_BUTTON_TITLE")
    public static let videoDefaultUIStartDialogAcceptButtonTitle = localizedString(forKey: "GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_ACCEPT_BUTTON_TITLE")
    public static let videoDefaultUIStartDialogTermsText = localizedString(forKey: "GLANCE_VIDEO_DEFAULT_UI_START_DIALOG_TERMS_TEXT")
    public static let videoDefaultUIInQueueText = localizedString(forKey: "GLANCE_VIDEO_DEFAULT_UI_IN_QUEUE_TEXT")
    public static let keyboardMaskLabel = localizedString(forKey: "GLANCE_KEYBOARD_MASK_LABEL")
    public static let backgroundSuspendMaskLabel = localizedString(forKey: "GLANCE_BACKGROUND_SUSPEND_MASK_LABEL")
    public static let backgroundPauseMaskLabel = localizedString(forKey: "GLANCE_BACKGROUND_PAUSE_MASK_LABEL")
    public static let defaultUIAgentCodeText = localizedString(forKey: "GLANCE_DEFAULT_UI_AGENT_CODE_TEXT")
    public static let defaultUIInQueueText = localizedString(forKey: "GLANCE_DEFAULT_UI_IN_QUEUE_TEXT")
    public static let defaultUICancelText = localizedString(forKey: "GLANCE_DEFAULT_UI_CANCEL_TEXT")
    public static let defaultUIVisitorEndDialogTitle = localizedString(forKey: "GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_TITLE")
    public static let defaultUIVisitorEndDialogMessage = localizedString(forKey: "GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_MESSAGE")
    public static let defaultUIVisitorEndDialogEndButton = localizedString(forKey: "GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_END_BUTTON")
    public static let defaultUIVisitorEndDialogCancelButton = localizedString(forKey: "GLANCE_DEFAULT_UI_VISITOR_END_DIALOG_CANCEL_BUTTON")
    public static let sessionStartDialogHeaderTextAppShare = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_APP_SHARE")
    public static let sessionStartDialogHeaderTextAppShareAndVideo = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_APP_SHARE_AND_VIDEO")
    public static let sessionStartDialogHeaderTextInQueue = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_QUEUE")
    public static let sessionStartDialogHeaderTextInQueueCode = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_QUEUE_CODE")
    public static let sessionStartDialogHeaderTextInSessionAddVideo = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_IN_SESSION_ADD_VIDEO")
    public static let sessionStartDialogHeaderTextEndSession = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_HEADER_TEXT_END_SESSION")
    public static let sessionStartDialogDescriptionTextAppShare = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_APP_SHARE")
    public static let sessionStartDialogDescriptionTextAppShareAndVideo = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_APP_SHARE_AND_VIDEO")
    public static let sessionStartDialogDescriptionTextInSessionAddVideo = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_DESCRIPTION_TEXT_IN_SESSION_ADD_VIDEO")
    public static let sessionStartDialogAcceptButtonText = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_ACCEPT_BUTTON_TEXT")
    public static let sessionStartDialogDeclineButtonText = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_DECLINE_BUTTON_TEXT")
    public static let sessionStartDialogYesButtonText = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_YES_BUTTON_TEXT")
    public static let sessionStartDialogNoButtonText = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_NO_BUTTON_TEXT")
    public static let sessionStartDialogTermsLinkText = localizedString(forKey: "GLANCE_SESSION_START_DIALOG_TERMS_LINK_TEXT")
    public static let cameraAlertTitle = localizedString(forKey: "GLANCE_CAMERA_ALERT_TITLE")
    public static let cameraPermission = localizedString(forKey: "GLANCE_CAMERA_PERMISSION")
    public static let goToSettings = localizedString(forKey: "GLANCE_GO_TO_SETTINGS")
    public static let shareAppDialogTitle = localizedString(forKey: "APP_SHARE_DIALOG_TITLE")
    public static let shareAppDialogVideoTitle = localizedString(forKey: "APP_SHARE_DIALOG_VIDEO_TITLE")
    public static let shareAppDialogSubtitle = localizedString(forKey: "APP_SHARE_DIALOG_SUBTITLE")
    public static let shareAppDialogVideoSubtitle = localizedString(forKey: "APP_SHARE_DIALOG_VIDEO_SUBTITLE")
    public static let shareAppDialogPrimaryButton = localizedString(forKey: "APP_SHARE_DIALOG_PRIMARY_BUTTON")
    public static let shareAppDialogSecondaryButton = localizedString(forKey: "APP_SHARE_DIALOG_SECONDARY_BUTTON")
    public static let shareAppDialogURL = localizedString(forKey: "APP_SHARE_DIALOG_URL")
    public static let waitingDialogTitle = localizedString(forKey: "WAITING_DIALOG_TITLE")
    public static let logoViewTitle = localizedString(forKey: "LOGO_VIEW_TITLE")
    public static let dialogHostedSessionKeyTitle = localizedString(forKey: "DIALOG_HOSTED_SESSION_KEY_TITLE")
    public static let dialogErrorTitle = localizedString(forKey: "DIALOG_ERROR_TITLE")
    public static let dialogErrorPrimaryButton = localizedString(forKey: "DIALOG_ERROR_PRIMARY_BUTTON")
    public static let dialogErrorSecondaryButton = localizedString(forKey: "DIALOG_ERROR_SECONDARY_BUTTON")
}
