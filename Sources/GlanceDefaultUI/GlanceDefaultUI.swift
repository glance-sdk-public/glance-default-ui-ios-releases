//
//  GlanceDefaultUI.swift
//
//
//  Created by Felipe Melo on 14/09/23.
//

import Foundation
import GlanceCore
#if canImport(GlanceSDK)
import GlanceSDK
#endif
import Combine

public protocol GlanceUIDelegate {
    func sessionConnected(dismissAction: (() -> Void)?)
    func receivedSessionCode(_ sessionCode: String)
    func sessionAgentConnected()
    func onGuestCountChange(guests: [GlanceAgent], response: String?)
    func sessionEnded(_ sessionKey: String?)
    
    func presenceDidConnect()
    func presenceDidDisconnect()
    func presenceSessionStarted()
    
    func receivedVideoCaptureImage(_ image: UIImage)
    func didStartVideoCapture(_ captureSession: AVCaptureSession)
    func didStopVideoCapture(_ captureSession: AVCaptureSession)
    
    func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession)
    func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession)
    
    func didPauseVisitorVideo()
    
    func didStartAgentViewer(_ glanceAgentViewer: UIView)
    func didStopAgentViewer()
    
    func onUpdateVisibility(to visibility: GlanceVisibility)
    func onUpdateLocation(to location: GlanceLocation)
    func onUpdateSize(size: String)
    func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?)
    func agentAskedForVideoSession()
    func onUpdateAgentVideo()
    
    func initTimeoutExpired()
    func startTimeoutExpired()
    func presenceTimeoutExpired()
    
    func hostedSessionDidStart(sessionKey: String)
    func hostedSessionDidEnd(errorMessage: String)
    
    func onEvent(_ event: GlanceEvent)
    func onError(_ error: GlanceError)
}

public final class GlanceDefaultUI {
    static let shared = GlanceDefaultUI()
    let dialogsCoordinator: DialogsCoordinator
    let sessionCoordinator: SessionCoordinator
    let service: GlanceServiceType = GlanceService()
    let messageManager: MessageManagerType
    
    var glanceInitParams: GlanceVisitorInitParams = GlanceVisitorInitParams()
    var glanceStartParams: GlanceStartParams = GlanceStartParams()
    
    var initialized = false
    var isPresenceOn = false
    var isVisitorVideoEnabled = false
    var sessionStarted = false
    var isFullScreen = CurrentValueSubject<Bool, Never>(false)
    var delegate: GlanceUIDelegate?
    var maxAgentSessionConnectAttempts: Int = 0
    
    var initTimeout: Double?
    var startSessionTimeout: GlanceTimeout?
    var presenceTimeout: Double?
    
    init() {
        messageManager = MessageManager()
        dialogsCoordinator = DialogsCoordinator()
        sessionCoordinator = SessionCoordinator(dialogsCoordinator: dialogsCoordinator,
                                                service: service,
                                                messageManager: messageManager)
        
        Glance.setDelegate(self)
        Glance.setHostedSessionDelegate(self)
    }
    
    public static func setVisitorParams(
        _ params: GlanceVisitorInitParams,
        timeout: Double?,
        maxConnectAttempts: Int = 0
    ) {
        shared.setVisitorParams(params)
        shared.maxAgentSessionConnectAttempts = maxConnectAttempts
        shared.initTimeout = timeout
        shared.glanceStartParams.presenceStart = false
        shared.glanceStartParams.showTerms = false
    }
    
    public static func start(key: String? = nil, timeout: GlanceTimeout?, videoMode: GlanceVideoMode) {
        shared.startSessionTimeout = timeout
        shared.start(key: key, videoMode: videoMode)
    }
    
    public static func endSession() {
        shared.service.endSession()
    }
    
    public static func setPresence(_ isPresenceOn: Bool, timeout: Double? = nil, maxConnectAttempts: Int32? = nil) {
        shared.isPresenceOn = isPresenceOn
        shared.presenceTimeout = timeout
        shared.service.setPresence(isPresenceOn, timeout: shared.presenceTimeout, maxConnectAttempts: maxConnectAttempts ?? -1)

        
        if shared.initialized && isPresenceOn {
            shared.glanceInitParams.screenshare = isPresenceOn
        }
    }
    
    public static func setDelegate(_ delegate: GlanceUIDelegate) {
        shared.delegate = delegate
    }
    
    func setVisitorParams(_ params: GlanceVisitorInitParams) {
        initialized = true
        glanceInitParams = params
        glanceInitParams.screenshare = isPresenceOn
        service.setVisitorParams(params, timeout: initTimeout)
    }
    
    func start(key: String? = nil, videoMode: GlanceVideoMode) {
        glanceStartParams.key = key
        if glanceInitParams.video {
            glanceStartParams.video = videoMode
            dialogsCoordinator.presentAppShareVideoDialog()
        } else {
            glanceStartParams.video = VideoOff
            dialogsCoordinator.presentAppShareDialog()
        }
    }
    
    func showShowCodeDialog(sessionKey: String) {
        DispatchQueue.main.async { [weak self] in
            self?.dialogsCoordinator.presentShowCodeDialog(code: sessionKey)
        }
    }
    
    func showWaitingAgentDialog(dismissAction: (() -> Void)?) {
        DispatchQueue.main.async {
            self.dialogsCoordinator.presentWaitingAgentDialog(dismissAction: dismissAction)
        }
    }
    
    func startSession() {
        sessionStarted = true
        messageManager.pause(!isVisitorVideoEnabled)
        
        let sessionType: SessionType
        sessionType = SessionType.build(glanceStartParams.video)
        
        let isFullScreen = sessionType == .fullScreen
        messageManager.visitorSizeUpdated(isFullScreen ? .large : .small)
        GlanceDefaultUI.shared.isFullScreen.value = isFullScreen
        
        sessionCoordinator.start(sessionType: sessionType)
    }
    
    public static func startHostedSession(username: String, password: String) {
        shared.startHostedSession(username: username, password: password)
    }
    
    public static func stopHostedSession() {
        shared.stopHostedSession()
    }
    
    private func checkVisitorVideoState(_ captureSession: AVCaptureSession) {
        if sessionStarted && !isVisitorVideoEnabled {
            captureSession.stopRunning()
        }
    }
    
    func startHostedSession(username: String, password: String) {
        service.startHostedSession(username: username, password: password)
    }
    
    func stopHostedSession() {
        service.stopHostedSession()
    }
}

extension GlanceDefaultUI: GlanceDelegate, GlanceHostedDelegate {
    public func sessionConnected(dismissAction: (() -> Void)?) {
        showWaitingAgentDialog(dismissAction: dismissAction)
        
        delegate?.sessionConnected(dismissAction: dismissAction)
    }
    
    public func presenceConnected(dismissAction: (() -> Void)?) {
        glanceStartParams.presenceStart = true
        glanceStartParams.showTerms = true
        glanceStartParams.key = glanceInitParams.visitorid
        
        service.startSession(
            params: glanceStartParams,
            timeout: startSessionTimeout,
            maxConnectAttempts: maxAgentSessionConnectAttempts
        )
    }
    
    public func presenceDidConnect() {
        print("presenceDidConnect")
        delegate?.presenceDidConnect()
    }
    
    public func presenceDidDisconnect() {
        print("presenceDidDisconnect")
        delegate?.presenceDidDisconnect()
    }
    
    public func presenceSessionStarted() {
        print("presenceSessionStarted")
        delegate?.presenceSessionStarted()
    }
    
    public func receivedSessionCode(_ sessionCode: String) {
        showShowCodeDialog(sessionKey: sessionCode)
        
        delegate?.receivedSessionCode(sessionCode)
    }
    
    public func sessionEnded(_ sessionKey: String?) {
        sessionStarted = false
        DispatchQueue.main.async { [weak self] in
            self?.dialogsCoordinator.hide()
            self?.sessionCoordinator.endSession()
        }
        
        delegate?.sessionEnded(sessionKey)
    }
    
    public func sessionAgentConnected() {
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            
            self.dialogsCoordinator.hide()
            self.startSession()
        }
        
        delegate?.sessionAgentConnected()
    }
    
    public func onGuestCountChange(guests: [GlanceAgent], response: String?) {
        delegate?.onGuestCountChange(
            guests: guests,
            response: response
        )
    }
    
    public func receivedVideoCaptureImage(_ image: UIImage) {
        sessionCoordinator.onUpdateImage.send(image)
        
        delegate?.receivedVideoCaptureImage(image)
    }
    
    public func didStartVideoCapture(_ captureSession: AVCaptureSession) {
        sessionCoordinator.onUpdateCaptureSession.send(captureSession)
        checkVisitorVideoState(captureSession)
        
        delegate?.didStartVideoCapture(captureSession)
    }
    
    public func didStopVideoCapture(_ captureSession: AVCaptureSession) {
        sessionCoordinator.onUpdateCaptureSession.send(captureSession)
        
        delegate?.didStopVideoCapture(captureSession)
    }
    
    public func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession) {
        dialogsCoordinator.onUpdateCaptureSession.send(captureSession)
        
        delegate?.didStartPreviewVideoCapture(captureSession)
    }
    
    public func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession) {
        dialogsCoordinator.onUpdateCaptureSession.send(captureSession)
        
        delegate?.didStopPreviewVideoCapture(captureSession)
    }
    
    public func didPauseVisitorVideo() {
        sessionCoordinator.onPausedVisitorVideo.send()
        
        delegate?.didPauseVisitorVideo()
    }
    
    public func didStartAgentViewer(_ glanceAgentViewer: UIView) {
        sessionCoordinator.onUpdateAgentViewer.send(glanceAgentViewer)
        
        delegate?.didStartAgentViewer(glanceAgentViewer)
    }
    
    public func didStopAgentViewer() {
        sessionCoordinator.onUpdateAgentViewer.send(nil)
        
        delegate?.didStopAgentViewer()
    }
    
    public func onUpdateVisibility(to visibility: GlanceVisibility) {
        if visibility == .tab {
            sessionCoordinator.onVisibilityChanged.send(.tab)
        } else {
            sessionCoordinator.onVisibilityChanged.send(nil)
        }
        
        delegate?.onUpdateVisibility(to: visibility)
    }
    
    public func onUpdateLocation(to location: GlanceLocation) {
        sessionCoordinator.onLocationChanged.send(GlanceLocation.getLocation(from: location))
        
        delegate?.onUpdateLocation(to: location)
    }
    
    public func onUpdateSize(size: String) {
        sessionCoordinator.onSizeChanged.send(size)
        
        delegate?.onUpdateSize(size: size)
    }
    
    public func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?) {
        if glanceInitParams.video {
            dialogsCoordinator.presentAgentAskShareVideoDialog(confirmAction: confirmAction, cancelAction: cancelAction)
        } else {
            dialogsCoordinator.presentStartDialog(confirmAction: confirmAction, cancelAction: cancelAction)
        }
        
        delegate?.askForVideoSession(
            confirmAction: confirmAction,
            cancelAction: cancelAction
        )
    }
    
    public func agentAskedForVideoSession() {
        dialogsCoordinator.presentAgentAskShareVideoDialog()
        
        delegate?.agentAskedForVideoSession()
    }
    
    public func onUpdateAgentVideo() {
        sessionCoordinator.showAgentVideo.send()
        
        delegate?.onUpdateAgentVideo()
    }
    
    public func initTimeoutExpired() {
        print("initTimeoutExpired")
        delegate?.initTimeoutExpired()
    }
    
    public func startTimeoutExpired() {
        print("startTimeoutExpired")
        delegate?.startTimeoutExpired()
    }
    
    public func presenceTimeoutExpired() {
        print("presenceTimeoutExpired")
        delegate?.presenceTimeoutExpired()
    }
    
    public func onEvent(_ event: GlanceEvent) {
        delegate?.onEvent(event)
    }
    
    public func onError(_ error: GlanceError) {
        delegate?.onError(error)
    }
    
    public func hostedSessionDidStart(sessionKey: String) {
        dialogsCoordinator.showHostedSessionKey(code: sessionKey)
        delegate?.hostedSessionDidStart(sessionKey: sessionKey)
    }
    
    public func hostedSessionDidEnd(errorMessage: String) {
        delegate?.hostedSessionDidEnd(errorMessage: errorMessage)
    }
}
