//
//  GlanceUI.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import Foundation

enum Layout {
    enum Spacing {
        static let extraVerySmall: CGFloat = 2.0
        static let verySmall: CGFloat = 4.0
        static let extraSmall: CGFloat = 8.0
        static let small: CGFloat = 10.0
        static let medium: CGFloat = 12.0
        static let large: CGFloat = 16.0
        static let extraLarge: CGFloat = 20.0
        static let veryLarge: CGFloat = 24.0
    }
    
    enum Size {
        static let extraExtraSmall: CGFloat = 12.0
        static let extraSmall: CGFloat = 16.0
        static let small: CGFloat = 24.0
        static let medium: CGFloat = 32.0
        static let extraMedium: CGFloat = 36.0
        static let large: CGFloat = 40.0
        static let extraLarge: CGFloat = 48.0
        static let extraExtraLarge: CGFloat = 54.0
        static let veryLarge: CGFloat = 60.0
        static let extraVeryLarge: CGFloat = 72.0
    }
    
    enum Radius {
        static let small: CGFloat = 8.0
        static let medium: CGFloat = 12.0
        static let large: CGFloat = 20.0
    }
}

extension CGFloat {
    static let extraSmall: CGFloat = 4.0
    static let small: CGFloat = 8.0
    static let medium: CGFloat = 16.0
    static let extraMedium: CGFloat = 20.0
    static let large: CGFloat = 24.0
    static let extraLarge: CGFloat = 32.0
    
    func negative() -> CGFloat {
        return (-self)
    }
}
