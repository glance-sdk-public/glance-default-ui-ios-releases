//
//  GlanceFont.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

extension UIFont {
    static func glanceFont(named font: String, size: CGFloat) -> UIFont {
        UIFont(name: font, size: size) ?? UIFont.systemFont(ofSize: size)
    }
}

@available(iOS 13.0, *)
extension Text {
    func glanceFont(_ glanceFont: UIFont) -> Self {
        font(Font(glanceFont))
    }
}

enum GlanceFont {
    static let title = UIFont.glanceFont(named: "Inter-SemiBold", size: 36.0)
    static let h1 = UIFont.glanceFont(named: "Inter-SemiBold", size: 30.0)
    static let h2 = UIFont.glanceFont(named: "Inter-SemiBold", size: 24.0)
    static let h3 = UIFont.glanceFont(named: "Inter-SemiBold", size: 20.0)
    static let h4 = UIFont.glanceFont(named: "Inter-Bold", size: 18.0)
    static let h5 = UIFont.glanceFont(named: "Inter-Bold", size: 16.0)
    static let bodyLarge = UIFont.glanceFont(named: "Inter-Regular", size: 18.0)
    static let bodyText = UIFont.glanceFont(named: "Inter-Regular", size: 16.0)
    static let bodySmall = UIFont.glanceFont(named: "Inter-Regular", size: 14.0)
    static let bodyExtraSmall = UIFont.glanceFont(named: "Inter-SemiBold", size: 10.0)
    static let capitalsSmall = UIFont.glanceFont(named: "Inter-Bold", size: 10.0)
    static let button = UIFont.glanceFont(named: "Inter-Bold", size: 16.0)
}
