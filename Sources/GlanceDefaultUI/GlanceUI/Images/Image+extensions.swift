//
//  Image+extensions.swift
//  
//
//  Created by Glance on 10/6/23.
//

import SwiftUI

@available(iOS 13.0, *)
public extension Image {
    enum GlanceImages: String {
        case glanceAgentCursor = "Glance_AgentCursor"
        case glanceClose = "Glance_Close"
        case glanceConnecting = "Glance_Connecting"
        case glanceDraw = "Glance_Draw"
        case glanceFlipCamera = "Glance_FlipCamera"
        case glanceHangup = "Glance_Hangup"
        case glanceHeadset = "Glance_Headset"
        case glanceHeadsetMan = "Glance_HeadsetMan"
        case glanceKeypad = "Glance_Keypad"
        case glanceLogo = "Glance_Logo"
        case glanceMicrophone = "Glance_Microphone"
        case glanceMicrophoneOff = "Glance_MicrophoneOff"
        case glanceNewBlurOff = "Glance_New_BlurOff"
        case glanceNewBlurOn = "Glance_New_BlurOn"
        case glanceNewCamera = "Glance_New_Camera"
        case glanceCaratDown = "Glance_New_CaratDown"
        case glanceNewCaratUp = "Glance_New_CaratUp"
        case glanceNewClose = "Glance_New_Close"
        case glanceNewDivider = "Glance_New_Divider"
        case glanceNewFlipCamera = "Glance_New_FlipCamera"
        case glanceNewHangup = "Glance_New_Hangup"
        case glanceNewHeadphones = "Glance_New_Headphones"
        case glanceNewLogo = "Glance_New_Logo"
        case glanceNewMaximize = "Glance_New_Maximize"
        case glanceNewMicrophoneOff = "Glance_New_MicrophoneOff"
        case glanceNewMicrophoneOn = "Glance_New_MicrophoneOn"
        case glanceNewMinimize = "Glance_New_Minimize"
        case glanceNewSettings = "Glance_New_Settings"
        case glanceNewSpeakerphoneOff = "Glance_New_SpeakerphoneOff"
        case glanceNewSpeakerphoneOn = "Glance_New_SpeakerphoneOn"
        case glanceNewVideoFullscreenMinimizeDarkDark = "Glance_New_Video_Fullscreen_Minimize_Dark_Dark"
        case glanceNewVideo_Fullscreen_Minimize_Dark = "Glance_New_Video_Fullscreen_Minimize_Dark"
        case glanceNewVideo_Fullscreen = "Glance_New_Video_Fullscreen"
        case glanceNewVideoOff = "Glance_New_VideoOff"
        case glanceNewVideoOn = "Glance_New_VideoOn"
        case glanceNewVisitor = "Glance_New_Visitor"
        case glancePhone = "Glance_Phone"
        case glanceSpeakerphone = "Glance_Speakerphone"
        case glanceSpeakerphoneOff = "Glance_SpeakerphoneOff"
        case glanceTabArrow = "Glance_Tab_Arrow"
        case glanceVideo = "Glance_Video"
        case glanceVideoDialogClose = "Glance_VideoDialogClose"
        case glanceVideoInfoImage = "Glance_VideoInfoImage"
        case noImage = ""
    }
    
    init(glanceImage: GlanceImages) {
        #if canImport(GlanceFramework)
        self.init(glanceImage.rawValue, bundle: Bundle(for: DialogsCoordinator.self))
        #else
        self.init(glanceImage.rawValue, bundle: Bundle.module)
        #endif
    }
    
    func resizableFrame(width: CGFloat, height: CGFloat) -> some View {
        self
            .resizable()
            .frame(width: width, height: height)
    }
}
