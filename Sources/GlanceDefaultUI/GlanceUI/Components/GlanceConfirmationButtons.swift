//
//  GlanceConfirmationButtons.swift
//  
//
//  Created by Glance on 9/20/23.
//

import SwiftUI
import Combine

struct GlanceConfirmationButtons: View {
    let primaryButtonText: String
    let secondaryButtonText: String
    
    let onPrimaryTapped: PassthroughSubject<Void, Never>
    let onSecondaryTapped: PassthroughSubject<Void, Never>
    
    var body: some View {
        HStack(alignment: .center, spacing: 9) {
            primaryButton
            
            secondaryButton
        }
    }
    
    var primaryButton: some View {
        Button {
            onPrimaryTapped.send()
        } label: {
            Group {
                Text(primaryButtonText)
                    .glanceFont(GlanceFont.h3)
                    .foregroundColor(.white)
                    .frame(height: 24)
                    .padding(.horizontal, 18)
                    .padding(.vertical, 12)
            }
            .background(
                RoundedRectangle(cornerRadius: .infinity)
                    .fill(Color.basePrimary)
            )
        }
    }
    
    var secondaryButton: some View {
        Button {
            onSecondaryTapped.send()
        } label: {
            Group {
                Text(secondaryButtonText)
                    .glanceFont(GlanceFont.h3)
                    .foregroundColor(.basePrimary)
                    .frame(height: 24)
                    .padding(.horizontal, 18)
                    .padding(.vertical, 12)
            }
            .background(
                RoundedRectangle(cornerRadius: .infinity)
                    .stroke(Color.basePrimary)
            )
        }
    }
}

struct GlanceConfirmationButtons_Previews: PreviewProvider {
    static var previews: some View {
        GlanceConfirmationButtons(
            primaryButtonText: GlanceLocalizedStringsConstants.defaultUIPresenceDialogYesButtonTitle,
            secondaryButtonText: GlanceLocalizedStringsConstants.defaultUIPresenceDialogNoButtonTitle,
            onPrimaryTapped: PassthroughSubject<Void,
            Never>(),
            onSecondaryTapped: PassthroughSubject<Void,
            Never>()
        )
    }
}
