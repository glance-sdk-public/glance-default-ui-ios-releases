//
//  LoadingCircle.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI

struct LoadingCircle: View {
    @State var scale: CGFloat = 0.3
    
    var repeatingAnimation: Animation {
        Animation
            .easeInOut(duration: 1)
            .repeatForever()
    }
    
    var body: some View {
        ZStack {
            Circle()
                .frame(width: 49, height: 49)
                .foregroundColor(.baseSecondary10)
                .scaleEffect(scale)
                .onAppear() {
                    withAnimation(self.repeatingAnimation) { self.scale = 1 }
            }
            
            Circle()
                .frame(width: 12, height: 12)
                .foregroundColor(.baseSecondary)
        }
    }
}

struct LoadingCircle_Previews: PreviewProvider {
    static var previews: some View {
        LoadingCircle()
    }
}
