//
//  CloseButton.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI

struct CloseButton: View {
    let action: () -> Void
    let color: Color
    
    init(action: @escaping () -> Void, color: Color = .black) {
        self.action = action
        self.color = color
    }
    
    var body: some View {
        Button {
            action()
        } label: {
            Image(glanceImage: .glanceNewClose)
                .foregroundColor(color)
                .frame(width: 24, height: 24)
        }
    }
}

struct CloseButton_Previews: PreviewProvider {
    static var previews: some View {
        CloseButton() {
            
        }
    }
}
