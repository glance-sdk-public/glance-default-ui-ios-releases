//
//  StartDialogButtonsView.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 02/03/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI
import Combine

@available(iOS 13.0, *)
public struct StartDialogButtonsView: View {
    var onAccept = PassthroughSubject<Void, Never>()
    var onDecline = PassthroughSubject<Void, Never>()
    
    public var body: some View {
        HStack(spacing: Layout.Spacing.large) {
            GlanceButton(
                title: GlanceLocalizedStringsConstants.shareAppDialogPrimaryButton,
                buttonColor: .basePrimary
            ) {
                onAccept.send()
            }
            
            GlanceButton(
                title: GlanceLocalizedStringsConstants.shareAppDialogSecondaryButton,
                buttonColor: .neutral500
            ) {
                onDecline.send()
            }
        }
        .frame(maxWidth: .infinity)
        .padding([.leading, .trailing], Layout.Size.extraMedium)
    }
}

struct StartDialogButtonsView_Previews: PreviewProvider {
    static var previews: some View {
        StartDialogButtonsView()
            .previewLayout(.fixed(width: 337, height: 100))
    }
}
