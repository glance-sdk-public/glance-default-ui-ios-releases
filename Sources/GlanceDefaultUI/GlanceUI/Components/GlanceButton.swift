//
//  GlanceButton.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct GlanceButton: View {
    private let title: String
    private let buttonColor: Color
    private let dismissAction: () -> (Void)
    
    public init(title: String, buttonColor: Color, dismissAction: @escaping () -> (Void)) {
        self.title = title
        self.buttonColor = buttonColor
        self.dismissAction = dismissAction
    }
    
    var body: some View {
        Button {
            dismissAction()
        } label: {
            Text(title)
                .foregroundColor(.white)
                .frame(height: Layout.Size.extraLarge)
                .frame(maxWidth: .infinity)
                .background(
                    RoundedRectangle(cornerRadius: Layout.Radius.large)
                        .foregroundColor(buttonColor)
                )
        }
    }
}

@available(iOS 13.0, *)
struct GlanceButton_Previews: PreviewProvider {
    static var previews: some View {
        GlanceButton(
            title: GlanceLocalizedStringsConstants.shareAppDialogSecondaryButton,
            buttonColor: .neutral500
        ) {}
            .previewLayout(.fixed(width: 200, height: Layout.Size.extraLarge))
    }
}
