//
//  HangupButton.swift
//  
//
//  Created by Glance on 10/6/23.
//

import SwiftUI

struct HangupButton: View {
    let action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            ZStack {
                RoundedRectangle(cornerRadius: 50)
                    .foregroundColor(.red700)
                
                Image(glanceImage: .glanceNewHangup)
                    .resizableFrame(width: 12, height: 12)
                    .foregroundColor(.white)
            }
            .frame(width: 56, height: 32)
        }
    }
}

struct HangoutButton_Previews: PreviewProvider {
    static var previews: some View {
        HangupButton() { }
    }
}
