//
//  DialogView.swift
//  Glance_iOS
//
//  Created by Glance on 2/27/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI
import Combine

@available(iOS 13.0, *)
struct DialogView<T: View>: View {
    private let dialogContent: T
    public init(@ViewBuilder builder: () -> T) {
        self.dialogContent = builder()
    }
    var body: some View {
        ZStack {
            Color
                .neutral800
                .opacity(0.5)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                Spacer().layoutPriority(1)
                
                dialogContent
                    .cornerRadius(Layout.Radius.medium)
                    .shadow(color: .neutral600, radius: Layout.Radius.medium)
                
                Spacer().layoutPriority(1)
            }
            .padding()
        }
    }
}

@available(iOS 13.0, *)
struct DialogView_Previews: PreviewProvider {
    static var previews: some View {
        DialogView {
            SessionAppShareDialogView(viewModel: SessionAppShareDialogViewModel())
        }
    }
}
