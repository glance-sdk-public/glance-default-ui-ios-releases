//
//  GlanceLogoView.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct GlanceLogoView: View {
    var body: some View {
        Text(GlanceLocalizedStringsConstants.logoViewTitle)
            .foregroundColor(.neutral400)
            .glanceFont(GlanceFont.capitalsSmall)
    }
}

@available(iOS 13.0, *)
struct GlanceLogoView_Previews: PreviewProvider {
    static var previews: some View {
        GlanceLogoView()
            .previewLayout(.fixed(width: 200, height: 40))
    }
}
