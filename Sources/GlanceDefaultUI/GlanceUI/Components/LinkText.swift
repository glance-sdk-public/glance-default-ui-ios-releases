//
//  LinkText.swift
//  Glance_iOSVideo
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
struct LinkText: View {
    @State var isPresentingWebView = false
    
    private var text: String
    private var url: String
    
    public init(text: String,
                url: String) {
        self.text = text
        self.url = url
    }
    
    var body: some View {
        Text(text)
            .foregroundColor(.basePrimary80)
            .underline()
            .onTapGesture {
                isPresentingWebView.toggle()
            }
            .sheet(isPresented: $isPresentingWebView) {
                VStack {
                    HStack {
                        Spacer()
                        
                        CloseButton(action: {
                            isPresentingWebView.toggle()
                        }, color: .neutral300)
                        .padding()
                    }
                    
                    WebView(url: URL(string: url)!)
                        .edgesIgnoringSafeArea(.all)
                }
            }
    }
}

@available(iOS 13.0, *)
struct LinkText_Previews: PreviewProvider {
    static var previews: some View {
        LinkText(text: GlanceLocalizedStringsConstants.sessionStartDialogTermsLinkText,
                 url: GlanceLocalizedStringsConstants.shareAppDialogURL)
            .previewLayout(.fixed(width: 200, height: 40))
    }
}
