//
//  CancelButton.swift
//  
//
//  Created by Glance on 9/19/23.
//

import SwiftUI

struct CancelButton: View {
    let completion: () -> Void
    
    var body: some View {
        Button {
            completion()
        } label: {
            VStack(spacing: 4) {
                Text(GlanceLocalizedStringsConstants.defaultUICancelText)
                    .foregroundColor(.baseSecondary)
                    .glanceFont(GlanceFont.bodyExtraSmall)
                    .bold()
                
                Rectangle()
                    .frame(width: 38, height: 1)
            }
        }
    }
}

struct CancelButton_Previews: PreviewProvider {
    static var previews: some View {
        CancelButton() {
            
        }
    }
}
