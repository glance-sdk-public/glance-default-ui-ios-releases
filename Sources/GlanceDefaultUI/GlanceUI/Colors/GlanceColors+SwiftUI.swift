//
//  GlanceColors+SwiftUI.swift
//  Glance_iOS
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import SwiftUI

@available(iOS 13.0, *)
extension Color {
    private static func glanceColor(named color: String) -> Color {
        #if canImport(GlanceFramework)
        Color(color, bundle: Bundle(for: DialogsCoordinator.self))
        #else
        Color(color, bundle: Bundle.module)
        #endif
    }
}

// MARK: - Primary

@available(iOS 13.0, *)
extension Color {
    static let basePrimary = Color.glanceColor(named: "basePrimary")
    static let basePrimary5 = Color.glanceColor(named: "basePrimary5")
    static let basePrimary10 = Color.glanceColor(named: "basePrimary10")
    static let basePrimary20 = Color.glanceColor(named: "basePrimary20")
    static let basePrimary30 = Color.glanceColor(named: "basePrimary30")
    static let basePrimary40 = Color.glanceColor(named: "basePrimary40")
    static let basePrimary60 = Color.glanceColor(named: "basePrimary60")
    static let basePrimary70 = Color.glanceColor(named: "basePrimary70")
    static let basePrimary80 = Color.glanceColor(named: "basePrimary80")
    static let basePrimary90 = Color.glanceColor(named: "basePrimary90")
    static let basePrimary100 = Color.glanceColor(named: "basePrimary100")
    static let basePrimary110 = Color.glanceColor(named: "basePrimary110")
    static let basePrimary120 = Color.glanceColor(named: "basePrimary120")
    static let basePrimary130 = Color.glanceColor(named: "basePrimary130")
    static let basePrimary140 = Color.glanceColor(named: "basePrimary140")
}

// MARK: - Secondary

@available(iOS 13.0, *)
extension Color {
    static let baseSecondary = Color.glanceColor(named: "baseSecondary")
    static let baseSecondary5 = Color.glanceColor(named: "baseSecondary5")
    static let baseSecondary10 = Color.glanceColor(named: "baseSecondary10")
    static let baseSecondary20 = Color.glanceColor(named: "baseSecondary20")
    static let baseSecondary30 = Color.glanceColor(named: "baseSecondary30")
    static let baseSecondary40 = Color.glanceColor(named: "baseSecondary40")
    static let baseSecondary60 = Color.glanceColor(named: "baseSecondary60")
    static let baseSecondary70 = Color.glanceColor(named: "baseSecondary70")
    static let baseSecondary80 = Color.glanceColor(named: "baseSecondary80")
    static let baseSecondary90 = Color.glanceColor(named: "baseSecondary90")
    static let baseSecondary100 = Color.glanceColor(named: "baseSecondary100")
    static let baseSecondary110 = Color.glanceColor(named: "baseSecondary110")
    static let baseSecondary120 = Color.glanceColor(named: "baseSecondary120")
    static let baseSecondary130 = Color.glanceColor(named: "baseSecondary130")
    static let baseSecondary140 = Color.glanceColor(named: "baseSecondary140")
}

// MARK: - Default

@available(iOS 13.0, *)
extension Color {
    static let alertDanger = Color.glanceColor(named: "alertDanger")
    static let alertInfo = Color.glanceColor(named: "alertInfo")
    static let alertSuccess = Color.glanceColor(named: "alertSuccess")
    static let alertWarning = Color.glanceColor(named: "alertWarning")
}

// MARK: - Neutral

@available(iOS 13.0, *)
extension Color {
    static let neutral50 = Color.glanceColor(named: "neutral50")
    static let neutral100 = Color.glanceColor(named: "neutral100")
    static let neutral200 = Color.glanceColor(named: "neutral200")
    static let neutral300 = Color.glanceColor(named: "neutral300")
    static let neutral400 = Color.glanceColor(named: "neutral400")
    static let neutral500 = Color.glanceColor(named: "neutral500")
    static let neutral600 = Color.glanceColor(named: "neutral600")
    static let neutral700 = Color.glanceColor(named: "neutral700")
    static let neutral800 = Color.glanceColor(named: "neutral800")
    static let neutral850 = Color.glanceColor(named: "neutral850")
    static let neutral900 = Color.glanceColor(named: "neutral900")
    static let neutral950 = Color.glanceColor(named: "neutral950")
}

// MARK: - Slate

@available(iOS 13.0, *)
extension Color {
    static let slate50 = Color.glanceColor(named: "slate50")
    static let slate100 = Color.glanceColor(named: "slate100")
    static let slate200 = Color.glanceColor(named: "slate200")
    static let slate300 = Color.glanceColor(named: "slate300")
    static let slate400 = Color.glanceColor(named: "slate400")
    static let slate500 = Color.glanceColor(named: "slate500")
    static let slate600 = Color.glanceColor(named: "slate600")
    static let slate700 = Color.glanceColor(named: "slate700")
    static let slate800 = Color.glanceColor(named: "slate800")
    static let slate900 = Color.glanceColor(named: "slate900")
}

// MARK: - Red

@available(iOS 13.0, *)
extension Color {
    static let red50 = Color.glanceColor(named: "red50")
    static let red100 = Color.glanceColor(named: "red100")
    static let red200 = Color.glanceColor(named: "red200")
    static let red300 = Color.glanceColor(named: "red300")
    static let red400 = Color.glanceColor(named: "red400")
    static let red500 = Color.glanceColor(named: "red500")
    static let red600 = Color.glanceColor(named: "red600")
    static let red700 = Color.glanceColor(named: "red700")
    static let red800 = Color.glanceColor(named: "red800")
    static let red900 = Color.glanceColor(named: "red900")
}

// MARK: - Yellow

@available(iOS 13.0, *)
extension Color {
    static let yellow50 = Color.glanceColor(named: "yellow50")
    static let yellow100 = Color.glanceColor(named: "yellow100")
    static let yellow200 = Color.glanceColor(named: "yellow200")
    static let yellow300 = Color.glanceColor(named: "yellow300")
    static let yellow400 = Color.glanceColor(named: "yellow400")
    static let yellow500 = Color.glanceColor(named: "yellow500")
    static let yellow600 = Color.glanceColor(named: "yellow600")
    static let yellow700 = Color.glanceColor(named: "yellow700")
    static let yellow800 = Color.glanceColor(named: "yellow800")
    static let yellow900 = Color.glanceColor(named: "yellow900")
}
