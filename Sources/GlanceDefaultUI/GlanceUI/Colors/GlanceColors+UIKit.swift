//
//  GlanceColors.swift
//  Glance_iOS
//
//  Created by Felipe Melo on 27/02/23.
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
extension UIColor {
    private static func glanceColor(named color: String) -> UIColor {
        UIColor(named: color, in: Bundle(for: DialogsCoordinator.self), compatibleWith: nil) ?? .clear
    }
}

@available(iOS 13.0, *)
extension UIColor {
    static let black = UIColor.glanceColor(named: "black")
    static let white = UIColor.glanceColor(named: "white")
}

// MARK: - Primary

@available(iOS 13.0, *)
extension UIColor {
    static let basePrimary = UIColor.glanceColor(named: "basePrimary")
    static let basePrimary5 = UIColor.glanceColor(named: "basePrimary5")
    static let basePrimary10 = UIColor.glanceColor(named: "basePrimary10")
    static let basePrimary20 = UIColor.glanceColor(named: "basePrimary20")
    static let basePrimary30 = UIColor.glanceColor(named: "basePrimary30")
    static let basePrimary40 = UIColor.glanceColor(named: "basePrimary40")
    static let basePrimary60 = UIColor.glanceColor(named: "basePrimary60")
    static let basePrimary70 = UIColor.glanceColor(named: "basePrimary70")
    static let basePrimary80 = UIColor.glanceColor(named: "basePrimary80")
    static let basePrimary90 = UIColor.glanceColor(named: "basePrimary90")
    static let basePrimary100 = UIColor.glanceColor(named: "basePrimary100")
    static let basePrimary110 = UIColor.glanceColor(named: "basePrimary110")
    static let basePrimary120 = UIColor.glanceColor(named: "basePrimary120")
    static let basePrimary130 = UIColor.glanceColor(named: "basePrimary130")
    static let basePrimary140 = UIColor.glanceColor(named: "basePrimary140")
}

// MARK: - Secondary

@available(iOS 13.0, *)
extension UIColor {
    static let baseSecondary = UIColor.glanceColor(named: "baseSecondary")
    static let baseSecondary5 = UIColor.glanceColor(named: "baseSecondary5")
    static let baseSecondary10 = UIColor.glanceColor(named: "baseSecondary10")
    static let baseSecondary20 = UIColor.glanceColor(named: "baseSecondary20")
    static let baseSecondary30 = UIColor.glanceColor(named: "baseSecondary30")
    static let baseSecondary40 = UIColor.glanceColor(named: "baseSecondary40")
    static let baseSecondary60 = UIColor.glanceColor(named: "baseSecondary60")
    static let baseSecondary70 = UIColor.glanceColor(named: "baseSecondary70")
    static let baseSecondary80 = UIColor.glanceColor(named: "baseSecondary80")
    static let baseSecondary90 = UIColor.glanceColor(named: "baseSecondary90")
    static let baseSecondary100 = UIColor.glanceColor(named: "baseSecondary100")
    static let baseSecondary110 = UIColor.glanceColor(named: "baseSecondary110")
    static let baseSecondary120 = UIColor.glanceColor(named: "baseSecondary120")
    static let baseSecondary130 = UIColor.glanceColor(named: "baseSecondary130")
    static let baseSecondary140 = UIColor.glanceColor(named: "baseSecondary140")
}

// MARK: - Default

@available(iOS 13.0, *)
extension UIColor {
    static let alertDanger = UIColor.glanceColor(named: "alertDanger")
    static let alertInfo = UIColor.glanceColor(named: "alertInfo")
    static let alertSuccess = UIColor.glanceColor(named: "alertSuccess")
    static let alertWarning = UIColor.glanceColor(named: "alertWarning")
}

// MARK: - Neutral

@available(iOS 13.0, *)
extension UIColor {
    static let neutral50 = UIColor.glanceColor(named: "neutral50")
    static let neutral100 = UIColor.glanceColor(named: "neutral100")
    static let neutral200 = UIColor.glanceColor(named: "neutral200")
    static let neutral300 = UIColor.glanceColor(named: "neutral300")
    static let neutral400 = UIColor.glanceColor(named: "neutral400")
    static let neutral500 = UIColor.glanceColor(named: "neutral500")
    static let neutral600 = UIColor.glanceColor(named: "neutral600")
    static let neutral700 = UIColor.glanceColor(named: "neutral700")
    static let neutral800 = UIColor.glanceColor(named: "neutral800")
    static let neutral850 = UIColor.glanceColor(named: "neutral850")
    static let neutral900 = UIColor.glanceColor(named: "neutral900")
    static let neutral950 = UIColor.glanceColor(named: "neutral950")
}

// MARK: - Slate

@available(iOS 13.0, *)
extension UIColor {
    static let slate50 = UIColor.glanceColor(named: "slate50")
    static let slate100 = UIColor.glanceColor(named: "slate100")
    static let slate200 = UIColor.glanceColor(named: "slate200")
    static let slate300 = UIColor.glanceColor(named: "slate300")
    static let slate400 = UIColor.glanceColor(named: "slate400")
    static let slate500 = UIColor.glanceColor(named: "slate500")
    static let slate600 = UIColor.glanceColor(named: "slate600")
    static let slate700 = UIColor.glanceColor(named: "slate700")
    static let slate800 = UIColor.glanceColor(named: "slate800")
    static let slate900 = UIColor.glanceColor(named: "slate900")
}

// MARK: - Red

@available(iOS 13.0, *)
extension UIColor {
    static let red50 = UIColor.glanceColor(named: "red50")
    static let red100 = UIColor.glanceColor(named: "red100")
    static let red200 = UIColor.glanceColor(named: "red200")
    static let red300 = UIColor.glanceColor(named: "red300")
    static let red400 = UIColor.glanceColor(named: "red400")
    static let red500 = UIColor.glanceColor(named: "red500")
    static let red600 = UIColor.glanceColor(named: "red600")
    static let red700 = UIColor.glanceColor(named: "red700")
    static let red800 = UIColor.glanceColor(named: "red800")
    static let red900 = UIColor.glanceColor(named: "red900")
}

// MARK: - Yellow

@available(iOS 13.0, *)
extension UIColor {
    static let yellow50 = UIColor.glanceColor(named: "yellow50")
    static let yellow100 = UIColor.glanceColor(named: "yellow100")
    static let yellow200 = UIColor.glanceColor(named: "yellow200")
    static let yellow300 = UIColor.glanceColor(named: "yellow300")
    static let yellow400 = UIColor.glanceColor(named: "yellow400")
    static let yellow500 = UIColor.glanceColor(named: "yellow500")
    static let yellow600 = UIColor.glanceColor(named: "yellow600")
    static let yellow700 = UIColor.glanceColor(named: "yellow700")
    static let yellow800 = UIColor.glanceColor(named: "yellow800")
    static let yellow900 = UIColor.glanceColor(named: "yellow900")
}
