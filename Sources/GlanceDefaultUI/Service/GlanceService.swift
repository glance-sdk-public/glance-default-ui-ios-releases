//
//  GlanceService.swift
//
//
//  Created by Felipe Melo on 19/09/23.
//

import Foundation
#if canImport(GlanceSDK)
import GlanceSDK
import GlanceCore
#endif

public protocol GlanceServiceType {
    var termsUrl: String { get }
    
    func setVisitorParams(_ params: GlanceVisitorInitParams, timeout: Double?)
    func setPresence(_ isPresenceOn: Bool, timeout: Double?, maxConnectAttempts: Int32)
    func startSession(params: GlanceStartParams, timeout: GlanceTimeout?, maxConnectAttempts: Int)
    func endSession()
    
    func startVideoPreview()
    func stopVideoPreview()
    
    func addVideo()
    
    func send(message: String, properties: String)

    func startHostedSession(username: String, password: String)
    func stopHostedSession()
}

@objc public final class GlanceService: NSObject, GlanceServiceType {
    let randomKey = "GLANCE_KEYTYPE_RANDOM"
    let visitorId = ""
    public var termsUrl: String
    
    public init(termsUrl: String = GlanceLocalizedStringsConstants.shareAppDialogURL) {
        self.termsUrl = termsUrl
    }
    
    public func setVisitorParams(_ params: GlanceVisitorInitParams, timeout: Double? = nil) {
        params.visitorid = params.visitorid ?? randomKey
        Glance.initVisitor(params: params, timeout: timeout)
    }
    
    public func setPresence(_ isPresenceOn: Bool, timeout: Double?, maxConnectAttempts: Int32) {
        Glance.setPresence(isPresenceOn, timeout: timeout, maxConnectAttempts: maxConnectAttempts)
    }
    
    public func startSession(params: GlanceStartParams, timeout: GlanceTimeout? = nil, maxConnectAttempts: Int) {
        if params.key == nil || params.key!.isEmpty {
            params.key = randomKey
        }
        
        Glance.startSession(params: params, timeout: timeout, maxConnectAttempts: maxConnectAttempts)
    }
    
    @objc public func endSession() {
        Glance.endSession()
    }
    
    @objc public func startVideoPreview() {
        Glance.startVideoPreview()
        
    }
    
    @objc public func stopVideoPreview() {
        Glance.stopVideoPreview()
    }
    
    @objc public func send(message: String, properties: String) {
        Glance.send(message: message, properties: properties)
    }
    
    @objc public func addVideo() {
        GlanceDefaultUI.shared.glanceStartParams.video = VideoSmallMultiway
        Glance.startVideoSession()
    }
    
    @objc public func startHostedSession(username: String, password: String) {
        Glance.startHostedSession(username: username, password: password)
    }
    
    @objc public func stopHostedSession() {
        Glance.stopHostedSession()
    }
}
