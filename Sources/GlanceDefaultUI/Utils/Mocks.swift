//
//  File.swift
//  
//
//  Created by Felipe Melo on 07/03/24.
//

import Foundation
import Combine
import UIKit
import AVFoundation

#if canImport(GlanceSDK)
import GlanceSDK
#endif

extension SessionViewModel {
    static func mock() -> SessionViewModel {
        SessionViewModel(sessionType: .twoWayVideo,
                         onPausedVisitorVideo: PassthroughSubject<Void, Never>(),
                         onUpdateAgentViewer: PassthroughSubject<UIView?, Never>(),
                         onUpdateCaptureSession: PassthroughSubject<AVCaptureSession, Never>(),
                         onUpdateImage: PassthroughSubject<UIImage, Never>(),
                         onUpdateOrientation: PassthroughSubject<UIDeviceOrientation, Never>(),
                         onSizeChanged: PassthroughSubject<String, Never>(),
                         showAgentVideo: PassthroughSubject<Void, Never>(),
                         messageManager: MessageManager())
    }
}
