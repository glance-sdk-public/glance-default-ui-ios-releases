//
//  TestAppView.swift
//  glance-sample-app
//
//  Created by Felipe Melo on 14/09/23.
//

import SwiftUI
import AVFoundation
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
#endif

struct TestAppView: View {
    @ObservedObject var viewModel: TestAppViewModel
    @State var showAlert: Bool = false
    @State var callId: String = ""
    @State var showTestAlert: Bool = false
    @State private var renderMethodSelected = 0
    @State var webMaskingURL: String = "https://d2e93a2oavc15x.cloudfront.net"
    let coordinator: AppCoordinator
    
    var appVersion: String {
        "Version: \(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")"
    }
    
    var body: some View {
        TabView {
            testView
                .tabItem {
                    Image(systemName: "house")
                    Text("Test App")
                }
            
            LogView(viewModel: viewModel)
                .tabItem {
                    Image(systemName: "doc.text")
                    Text("Log")
                }
            HostedView()
                .tabItem {
                    Image(systemName: "headphones")
                    Text("Hosted")
                }
        }
        .background(Color(UIColor.systemGroupedBackground).ignoresSafeArea())
    }
    
    private var testView: some View {
        NavigationView {
            VStack(alignment: .center, spacing: 8) {
                buildHeaderTitleView
                    .padding(.bottom, 20)
                
                ScrollView {
                    VStack(spacing: 16) {
                        buildHeaderView
                        
                        buildPresenceView
                        
                        buildVideoModeView
                        
                        buildRenderModeView
                        
                        buildFooterView
                    }
                    
                    Spacer()
                }
                .padding(.horizontal, 20)
                .overlay(
                    // AlertView is a UIViewRepresentable that displays a UIAlertView for UIKit Testing.
                    Group {
                        if showTestAlert {
                            AlertView(title: "Alert Title", message: "This is the alert message.") {
                                showTestAlert = false 
                            }
                        }
                    }
                )
            }
            .onAppear {
                Glance.maskKeyboard(true)
                if viewModel.renderOptionPicked == "drawViewInHierarchy" {
                    Glance.setDrawViewHierarchy(true)
                }
                viewModel.setDelegate()
                if viewModel.isPresenceOn {
                    viewModel.onPresenceTapped()
                }
                if viewModel.isVideoOn {
                    viewModel.onVideoTapped(video: true)
                }
                
                let status = AVCaptureDevice.authorizationStatus(for: .video)
                
                switch status {
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for: .video) { authorized in
                        if !authorized {
                            showAlert = true
                        }
                    }
                default:
                    print("Must handle authorization case")
                }
            }
            .alert(isPresented: $showAlert) {
                Alert(title: Text("Error"),
                      message: Text("You haven't authorized camera permission"))
            }
            .alert(isPresented: $viewModel.showAlertDialog) {
                Alert(
                    title: Text(viewModel.errorCode),
                    message: Text(viewModel.errorMessage),
                    primaryButton: .default(Text("Ok")),
                    secondaryButton: .default(Text("Cancel"))
                )
            }
        }
    }
    
    private var pauseView: some View {
        VStack(alignment: .leading) {
            buildTitleOfTheSectionView(text: "INIT SESSION")
            
            buildToggleView(sideText: "Is Paused: ",
                            isOn: $viewModel.isPaused)
            .onChange(of: viewModel.isPaused) { newValue in
                viewModel.pause(newValue)
            }
            
            Button(action: {
                viewModel.togglePause()
            }, label: {
                Text("Toggle Pause")
                    .foregroundColor(.white)
                    .frame(height: 40)
                    .frame(maxWidth: .infinity)
                    .background(Color.blue)
                    .cornerRadius(12)
            })
        }
    }
    
    private var buildHeaderView: some View {
        VStack(alignment: .leading, spacing: 8) {
            buildTitleOfTheSectionView(text: "INIT SESSION")
            
            buildTextfieldView(placeholder: "Glance Server",
                               text: $viewModel.glanceServer)
            
            buildTextfieldView(placeholder: "Session Key",
                               text: $viewModel.sessionKey)
            
            buildTextfieldView(placeholder: "Group ID",
                               text: $viewModel.groupId)
            
            buildTextfieldView(placeholder: "Visitor ID",
                               text: $viewModel.visitorId)
            
            pauseView
            
            HStack {
                buildToggleView(sideText: "Video Enabled: ",
                                isOn: $viewModel.isVideoOn)
                .onChange(of: viewModel.isVideoOn) { newValue in
                    viewModel.onVideoTapped(video: newValue)
                }
            }
        }
    }
    
    private var buildPresenceView: some View {
        VStack(spacing: 12) {
            buildTitleOfTheSectionView(text: "PRESENCE")
            
            HStack {
                buildToggleView(sideText: "Presence Enabled",
                                isOn: $viewModel.isPresenceOn)
                .onChange(of: viewModel.isPresenceOn) { _ in
                    viewModel.onPresenceTapped()
                }
                
                Spacer()
            }
            
            Button(action: {
                viewModel.pausePresence()
            }, label: {
                Text(viewModel.isPresenceConnected ? "Pause" : "Join")
                    .foregroundColor(.white)
                    .frame(height: 40)
                    .frame(maxWidth: .infinity)
                    .background(viewModel.isPresenceConnected ? Color.orange : Color.blue)
                    .cornerRadius(12)
            })
            
            ConnectionStatusView(isConnected: $viewModel.isPresenceConnected)
        }
    }
    
    private var buildVideoModeView: some View {
        VStack(alignment: .leading, spacing: 12) {
            buildTitleOfTheSectionView(text: "Video Modes:")
            
            RoundedRectangle(cornerRadius: 2)
                .strokeBorder()
                .frame(height: 35)
                .foregroundColor(.gray.opacity(0.2))
                .overlay (
                    Menu(viewModel.videoOptionPicked) {
                        ForEach(viewModel.videoPickerOptions, id: \.self) { option in
                            Button("\(option)") {
                                viewModel.videoOptionPicked = option
                            }
                        }
                    }
                        .padding(.leading, 6)
                        .foregroundColor(.primary)
                )
        }
    }
    
    private var buildRenderModeView: some View {
        VStack(alignment: .leading, spacing: 12) {
            buildTitleOfTheSectionView(text: "Agent Display Modes:")
            
            RoundedRectangle(cornerRadius: 2)
                .strokeBorder()
                .frame(height: 35)
                .foregroundColor(.gray.opacity(0.2))
                .overlay (
                    Menu(viewModel.renderOptionPicked) {
                        ForEach(viewModel.renderPickerOptions, id: \.self) { option in
                            Button("\(option)") {
                                viewModel.renderOptionPicked = option
                                switch( option ){
                                case "renderInContext":
                                    Glance.setDrawViewHierarchy(false)
                                case "drawViewInHierarchy":
                                    Glance.setDrawViewHierarchy(true)
                                default:
                                    Glance.setDrawViewHierarchy(false)
                                }
                                Glance.setDrawViewHierarchy( true )
                            }
                        }
                    }
                        .padding(.leading, 6)
                        .foregroundColor(.primary)
                )
        }
    }
    
    
    private var buildFooterView: some View {
        VStack(alignment: .center, spacing: 20) {
            Button("Start Session Random Key") {
                viewModel.startSessionRandomKey()
            }
            
            Button("Start Session") {
                viewModel.startSession()
            }
            
            if #available(iOS 15.0, *) {
                TextField(text: $webMaskingURL) {
                    Text("WebMasking URL")
                }
                .textFieldStyle(.roundedBorder)
            } else {
                EmptyView()
            }
            
            Button("WebMasking") {
                coordinator.showTestWebMaskingView(webMaskingURL)
            }
            .disabled(webMaskingURL.isEmpty)
            
            Button("Masking") {
                coordinator.showTestMaskingView()
            }
            
            
            NavigationLink(
                destination: {
                    TestMaskingView()
                },
                label: {
                    Text("Masking with SwiftUI")
                }
            )
            Button("Update Session Info") {
                let callId: UInt = Glance.getCallId()
                
                if callId != 0 {
                    self.callId = "Session Call Id: \(callId)"
                    showTestAlert = true
                } else{
                    self.callId = ""
                }
               
            }
           
        }
        .padding(.top, 10)
    }
    
    
    private var buildHeaderTitleView: some View {
        VStack(alignment: .center) {
            Text("iOS Test App")
                .bold()
            
            Text(appVersion)
                .bold()
            if( callId != ""){
                Text(callId).bold()
            }
        }
    }
    
    private func buildPickerView(optionPicked: Binding<String>, pickerOptions: [String]) -> some View {
        VStack() {
            Picker("", selection: optionPicked) {
                ForEach(pickerOptions, id: \.self) {
                    Text($0)
                }
            }
            .pickerStyle(.segmented)
        }
        .padding(.horizontal, 60)
    }
    
    private func buildTextfieldView(placeholder: String, text: Binding<String>) -> some View {
        VStack {
            TextField(placeholder, text: text)
                .textFieldStyle(.roundedBorder)
                .autocapitalization(.none)
        }
    }
    
    private func buildToggleView(sideText: String, isOn: Binding<Bool>) -> some View {
        HStack {
            Text(sideText)
            Toggle("", isOn: isOn).labelsHidden()
        }
    }
    
    private func buildTitleOfTheSectionView(text: String) -> some View {
        HStack {
            Text(text)
                .foregroundColor(.gray)
            Spacer()
        }
    }
}

struct TestAppView_Previews: PreviewProvider {
    static var previews: some View {
        TestAppView(viewModel: TestAppViewModel(),
                    coordinator: AppCoordinator(window: nil))
    }
}

struct ConnectionStatusView: View {
    @Binding var isConnected: Bool
    
    var body: some View {
        VStack {
            Image(systemName: isConnected ? "checkmark.circle.fill" : "xmark.circle.fill")
                .foregroundColor(isConnected ? .green : .red)
                .font(.largeTitle)
                .transition(.scale)
            Text(isConnected ? "Connected" : "Not Connected")
                .foregroundColor(isConnected ? .green : .red)
                .font(.headline)
        }
        .onTapGesture {
            withAnimation {
                isConnected.toggle()
            }
        }
    }
}
