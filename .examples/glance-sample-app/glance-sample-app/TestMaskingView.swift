//
//  TestMaskingView.swift
//  glance-sample-app
//
//  Created by bruno on 4/25/24.
//

#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
#endif
import SwiftUI

struct TestMaskingView: View {
    var body: some View {
        VStack(spacing: 20) {
            TextField("This place holder should be masked", text: .constant(""))
                .padding()
                .background(Color.white)
                .cornerRadius(8)
                .border(Color.black)
                .addMaskedView(withLabel: "Masked text field")
            
            Text("This label should be masked")
                .padding()
                .background(Color.white)
                .cornerRadius(8)
                .border(Color.black)
                .addMaskedView(withLabel: "Masked label")
            
            Button("Must be masked") {
                // Button action
            }
            .padding()
            .frame(maxWidth: .infinity)
            .background(Color.blue)
            .foregroundColor(.white)
            .cornerRadius(8)
            .padding(.horizontal)
            .addMaskedView(withLabel: "Masked button")
        }
        .navigationTitle("SwiftUI Masking")
        .padding()
    }
}

#Preview {
    TestMaskingView()
}
