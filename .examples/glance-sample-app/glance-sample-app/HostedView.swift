//
//  HostedView.swift
//  glance-sample-app
//
//  Created by bruno on 11/25/24.
//

import SwiftUI
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
import GlanceDefaultUI
#endif

struct HostedView: View {
    
    typealias Constants = GlanceLocalizedStringsConstants
    
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var server: String = ""
    
    @StateObject private var viewModel: HostedViewModel = HostedViewModel()

    var body: some View {
        VStack(spacing: 20) {
            TextField("Hosted Username", text: $username)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .autocapitalization(.none)
                .padding(.horizontal)

            SecureField("Hosted Password", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal)

            TextField("Hosted Server (e.g. www.glance.net)", text: $server)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .keyboardType(.URL)
                .autocapitalization(.none)
                .padding(.horizontal)

            Button(action: {
                viewModel.toggleHostedSession(username: username, password: password)
            }, label: {
                Text(viewModel.isHostedSessionActive ? "Stop" : "Start")
                    .foregroundColor(.white)
                    .frame(height: 40)
                    .frame(maxWidth: .infinity)
                    .background(viewModel.isHostedSessionActive ? Color.orange : Color.blue)
                    .cornerRadius(12)
            })
            
            if let sessionKey = viewModel.sessionKey {
                Text("Session Key: \(sessionKey)")
            }
        }
        .padding()
        .onAppear {
            viewModel.setDelegate()
        }
        .alert(isPresented: $viewModel.showAlertDialog) {
            Alert(
                title: Text(Constants.dialogErrorTitle),
                message: Text(viewModel.errorMessage),
                primaryButton: .default(Text(Constants.dialogErrorPrimaryButton)),
                secondaryButton: .default(Text(Constants.dialogErrorSecondaryButton))
            )
        }
    }
}

#Preview {
    HostedView()
}
