//
//  TestAppViewModel.swift
//  glance-sample-app
//
//  Created by Glance on 10/3/23.
//

import SwiftUI
import Combine
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
import GlanceDefaultUI
#endif
import GlanceCore

struct Log: Hashable {
    let type: LogType
    let message: String
}

enum LogType {
    case all, sdk, events, errors
    
    var value: String {
        switch self {
        case .all:
            "All"
        case .sdk:
            "SDK"
        case .events:
            "Events"
        case .errors:
            "Errors"
        }
    }
}

final class TestAppViewModel: ObservableObject {
    
    let maxRetriesForPresence: Int32 = 4
    let maxRetriesForAgentSession: Int = 4
    
    @AppStorage("glanceServer") var glanceServer: String = "" // "beta.glance.net" set this to use beta (or other) environment
    @AppStorage("sessionKey") var sessionKey: String = ""
    @AppStorage("groupId") var groupId: String = ""
    @AppStorage("visitorId") var visitorId: String = ""
    
    @Published var allMessages = [Log]()
    @Published var sdkMessages = [String]()
    @Published var eventMessages = [String]()
    @Published var errorMessages = [String]()
    
    @Published var logOptions = [LogType.all, LogType.sdk, LogType.events, LogType.errors]
    @Published var selectedOption = LogType.all
    
    //Presence
    
    @Published var isPresenceOn: Bool = false
    @Published var isPresenceConnected: Bool = false
    @AppStorage("isVideoOn") var isVideoOn: Bool = false
    @AppStorage("isPaused") var isPaused: Bool = false
    
    //Video
    
    @AppStorage("videoOptionPicked") var videoOptionPicked: String = "Off"
    var videoPickerOptions = ["Off", "Small Visitor", "Small Multiway", "Large Visitor", "Large Multiway"]
    
    @AppStorage("renderOptionPicked") var renderOptionPicked: String = "renderInContext"
    var renderPickerOptions = ["renderInContext", "drawViewInHierarchy"]
    
    
    
    var videoMode: GlanceVideoMode {
        switch videoOptionPicked {
        case "Small Visitor":
            return VideoSmallVisitor
        case "Small Multiway":
            return VideoSmallMultiway
        case "Large Visitor":
            return VideoLargeVisitor
        case "Large Multiway":
            return VideoLargeMultiway
        default :
            return VideoOff
        }
    }
    
    //Alert
    @Published var showAlertDialog: Bool = false
    var errorCode: String = ""
    var errorMessage: String = ""
    
    var currentTime: String {
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        return formatter.string(from: Date())
    }
    
    private var cancelSet = Set<AnyCancellable>()
    
    init() {
        GlanceDefaultUI.setDelegate(self)
    }
    
    func startSession() {
        GlanceManager.shared.setSetting(name: kGlanceSettingGlanceServer, value: glanceServer)
        
        let params = GlanceVisitorInitParams()
        params.groupid = Int32(groupId) ?? 0
        params.visitorid = visitorId
        params.video = isVideoOn
        
        GlanceManager.shared.setVisitorParams(params, maxConnectAttempts: maxRetriesForAgentSession)
        GlanceManager.shared.startSession(key: sessionKey, videoMode: videoMode)
        
    }
    
    func startSessionRandomKey() {
        GlanceManager.shared.setSetting(name: kGlanceSettingGlanceServer, value: glanceServer)
        
        let params = GlanceVisitorInitParams()
        params.groupid = Int32(groupId) ?? 0
        params.visitorid = visitorId
        params.video = isVideoOn
        
        GlanceManager.shared.setVisitorParams(params, maxConnectAttempts: maxRetriesForAgentSession)
        GlanceManager.shared.startSession(videoMode: videoMode)
    }
    
    func onPresenceTapped() {
        let params = GlanceVisitorInitParams()
        params.groupid = Int32(groupId) ?? 0
        params.visitorid = visitorId
        params.screenshare = isPresenceOn
        params.video = isVideoOn
        
        GlanceManager.shared.setPresence(isPresenceOn: isPresenceOn, maxConnectAttempts: maxRetriesForPresence)
        GlanceManager.shared.setVisitorParams(params)
    }
    
    func onVideoTapped(video: Bool) {
        let params = GlanceVisitorInitParams()
        params.groupid = Int32(groupId) ?? 0
        params.visitorid = visitorId
        params.video = video
        
        GlanceManager.shared.setVisitorParams(params, maxConnectAttempts: maxRetriesForAgentSession)
    }
    
    func pausePresence() {
        isPresenceOn.toggle()
        GlanceManager.shared.pausePresence(isPresenceOn)
    }
    
    func clearLogs() {
        allMessages = []
        sdkMessages = []
        eventMessages = []
        errorMessages = []
    }
    
    func setDelegate() {
        GlanceDefaultUI.setDelegate(self)
    }
    
    func pause(_ isPaused: Bool) {
        self.isPaused = isPaused
        GlanceManager.shared.pause(isPaused)
    }
    
    func togglePause() {
        isPaused.toggle()
        GlanceManager.shared.togglePause()
    }
    
    private func logEventMessage(_ event: GlanceEvent) {
        let event: GlanceSDKEvent = .init(event)
        let properties = event.properties.map({ "\($0.key): \($0.value)" }).joined(separator: ",\n       ")
        let message = """
        {
            type: Event,
            time: \(currentTime),
            code: \(event.code),
            message: \(event.message),
            properties: {
               \(properties)
            }
        }
        """
        let log = Log(type: .events, message: message)
        
        Task { @MainActor in
            allMessages.append(log)
            eventMessages.append(message)
        }
    }
    
    private func logSDKMessage(method: String, params: [String: Any]) {
        let formattedParams = params.map { "\($0): \($1)" }.joined(separator: ",\n")
        
        let message = """
            {
                type: SDK,
                time: \(currentTime),
                method: \(method),
                params: {
                    \(formattedParams)
                }
            }
            """
        let log = Log(type: .sdk, message: message)
        
        Task { @MainActor in
            allMessages.append(log)
            sdkMessages.append(message)
        }
    }
}

extension TestAppViewModel: GlanceUIDelegate {
    func sessionConnected(dismissAction: (() -> Void)?) {
        logSDKMessage(method: "sessionConnected", params: [:])
    }
    
    func receivedSessionCode(_ sessionCode: String) {
        logSDKMessage(method: "receivedSessionCode", params: ["sessionCode": sessionCode])
    }
    
    func sessionAgentConnected() {
        logSDKMessage(method: "sessionAgentConnected", params: [:])
    }
    
    func onGuestCountChange(guests: [GlanceAgent], response: String?) {
        logSDKMessage(method: "onGuestCountChange", params: ["guests": guests, "response": response ?? "nil"])
    }
    
    func sessionEnded(_ sessionKey: String?) {
        logSDKMessage(method: "sessionEnded", params: ["sessionKey": sessionKey ?? "nil"])
    }
    
    func presenceDidConnect() {
        Task { @MainActor in
            isPresenceConnected = true
        }
        logSDKMessage(method: "presenceDidConnect", params: [:])
    }
    
    func presenceDidDisconnect() {
        Task { @MainActor in
            isPresenceConnected = false
        }
        logSDKMessage(method: "presenceDidDisconnect", params: [:])
    }
    
    func presenceSessionStarted() {
        logSDKMessage(method: "presenceSessionStarted", params: [:])
    }
    
    func receivedVideoCaptureImage(_ image: UIImage) {
        logSDKMessage(method: "receivedVideoCaptureImage", params: [:])
    }
    
    func didStartVideoCapture(_ captureSession: AVCaptureSession) {
        logSDKMessage(method: "didStartVideoCapture", params: [:])
    }
    
    func didStopVideoCapture(_ captureSession: AVCaptureSession) {
        logSDKMessage(method: "didStopVideoCapture", params: [:])
    }
    
    func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession) {
        logSDKMessage(method: "didStartPreviewVideoCapture", params: [:])
    }
    
    func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession) {
        logSDKMessage(method: "didStopPreviewVideoCapture", params: [:])
    }
    
    func didPauseVisitorVideo() {
        logSDKMessage(method: "didPauseVisitorVideo", params: [:])
    }
    
    func didStartAgentViewer(_ glanceAgentViewer: UIView) {
        logSDKMessage(method: "didStartAgentViewer", params: [:])
    }
    
    func didStopAgentViewer() {
        logSDKMessage(method: "didStopAgentViewer", params: [:])
    }
    
    func onUpdateVisibility(to visibility: GlanceVisibility) {
        logSDKMessage(method: "onUpdateVisibility", params: ["visibility": visibility])
    }
    
    func onUpdateLocation(to location: GlanceLocation) {
        logSDKMessage(method: "onUpdateLocation", params: ["location": location])
    }
    
    func onUpdateSize(size: String) {
        logSDKMessage(method: "onUpdateSize", params: ["size": size])
    }
    
    func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?) {
        logSDKMessage(method: "askForVideoSession", params: [:])
    }
    
    func agentAskedForVideoSession() {
        logSDKMessage(method: "agentAskedForVideoSession", params: [:])
    }
    
    func onUpdateAgentVideo() {
        logSDKMessage(method: "onUpdateAgentVideo", params: [:])
    }
    
    func initTimeoutExpired() {
        logSDKMessage(method: "initTimeoutExpired", params: [:])
    }
    
    func startTimeoutExpired() {
        logSDKMessage(method: "startTimeoutExpired", params: [:])
    }
    
    func presenceTimeoutExpired() {
        logSDKMessage(method: "presenceTimeoutExpired", params: [:])
    }
    
    func onEvent(_ event: GlanceEvent) {
        guard event.code != EventPresenceConnectFail else { return }
        logEventMessage(event)
    }
    
    func onError(_ error: GlanceError) {
        errorCode = "\(error.code)"
        errorMessage = error.message
        Task { @MainActor in
            showAlertDialog = true
            
            errorMessages.append(
                """
                {
                    type: Error,
                    time: \(currentTime),
                    title: \(error.code),
                    message: \(error.message)
                }
                """
            )
        }
    }
    
    func hostedSessionDidStart(sessionKey: String) {}
    func hostedSessionDidEnd(errorMessage: String) {}
}
