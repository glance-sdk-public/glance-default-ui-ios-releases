//
//  HostedViewModel.swift
//  glance-sample-app
//
//  Created by bruno on 11/25/24.
//

import SwiftUI
import Combine
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
import GlanceDefaultUI
#endif
import GlanceCore

final class HostedViewModel: ObservableObject {
    @Published var isHostedSessionActive: Bool = false
    
    private(set) var sessionKey: String?
    
    //Alert
    @Published var showAlertDialog: Bool = false
    var errorMessage: String = ""
    
    init () {
        GlanceDefaultUI.setDelegate(self)
    }
    
    func toggleHostedSession(username: String, password: String) {
        if isHostedSessionActive {
            GlanceManager.shared.stopHostedSession()
        } else {
            GlanceManager.shared.startHostedSession(username: username, password: password)
        }
    }
    
    func setDelegate() {
        GlanceDefaultUI.setDelegate(self)
    }
}

extension HostedViewModel: GlanceUIDelegate {
    func sessionConnected(dismissAction: (() -> Void)?) {}
    func receivedSessionCode(_ sessionCode: String) {}
    func sessionAgentConnected() {}
    func onGuestCountChange(guests: [GlanceAgent], response: String?) {}
    func sessionEnded(_ sessionKey: String?) {}
    func presenceDidConnect() {}
    func presenceDidDisconnect() {}
    func presenceSessionStarted() {}
    func receivedVideoCaptureImage(_ image: UIImage) {}
    func didStartVideoCapture(_ captureSession: AVCaptureSession) {}
    func didStopVideoCapture(_ captureSession: AVCaptureSession) {}
    func didStartPreviewVideoCapture(_ captureSession: AVCaptureSession) {}
    func didStopPreviewVideoCapture(_ captureSession: AVCaptureSession) {}
    func didPauseVisitorVideo() {}
    func didStartAgentViewer(_ glanceAgentViewer: UIView) {}
    func didStopAgentViewer() {}
    func onUpdateVisibility(to visibility: GlanceVisibility) {}
    func onUpdateLocation(to location: GlanceLocation) {}
    func onUpdateSize(size: String) {}
    func askForVideoSession(confirmAction: @escaping (() -> Void), cancelAction: (() -> Void)?) {}
    func agentAskedForVideoSession() {}
    func onUpdateAgentVideo() {}
    func initTimeoutExpired() {}
    func startTimeoutExpired() {}
    func presenceTimeoutExpired() {}
    
    func hostedSessionDidStart(sessionKey: String) {
        self.sessionKey = sessionKey
        isHostedSessionActive = true
    }
    
    func hostedSessionDidEnd(errorMessage: String) {
        guard errorMessage.isEmpty else {
            self.errorMessage = errorMessage
            self.showAlertDialog = true
            return
        }
        self.sessionKey = ""
        isHostedSessionActive = false
    }
    
    func onEvent(_ event: GlanceEvent) {}
    
    func onError(_ error: GlanceError) {}
}
