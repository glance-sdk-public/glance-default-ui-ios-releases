//
//  LogView.swift
//  glance-sample-app
//
//  Created by Glance on 02/10/24.
//

import SwiftUI

final class LogViewModel: ObservableObject {
    init() {
        
    }
}

struct LogView: View {
    @ObservedObject var viewModel: TestAppViewModel
    @State var searchText = ""
    
    var body: some View {
        VStack(spacing: .zero) {
            headerView
            
            listView
        }
        .background(Color(UIColor.systemGroupedBackground).ignoresSafeArea())
    }
    
    private var headerView: some View {
        VStack {
            HStack {
                ForEach(viewModel.logOptions, id: \.self) { option in
                    HStack {
                        Image(systemName: viewModel.selectedOption == option ? "checkmark.circle.fill" : "circle")
                            .resizable()
                            .frame(width: 25, height: 25)
                            .foregroundColor(viewModel.selectedOption == option ? .blue : .gray)
                            .onTapGesture {
                                withAnimation {
                                    viewModel.selectedOption = option
                                }
                            }
                        
                        Text(option.value).font(.headline)
                    }
                }
                
                Button(action: {
                    viewModel.clearLogs()
                }, label: {
                    Image(systemName: "trash.fill")
                })
            }
                
            TextField("Search", text: $searchText)
                .textFieldStyle(.roundedBorder)
        }
        .padding()
    }
    
    private var listView: some View {
        List {
            switch viewModel.selectedOption {
            case .all:
                ForEach(filteredOptions(viewModel.allMessages), id: \.self) { option in
                    Text(option.message)
                        .foregroundColor(getColor(for: option))
                }
            case .events:
                ForEach(filteredMessages(viewModel.eventMessages), id: \.self) { message in
                    Text(message)
                        .foregroundColor(.blue)
                }
            case .sdk:
                ForEach(filteredMessages(viewModel.sdkMessages), id: \.self) { message in
                    Text(message)
                        .foregroundColor(.orange)
                }
            case .errors:
                ForEach(filteredMessages(viewModel.errorMessages), id: \.self) { message in
                    Text(message)
                        .foregroundColor(.red)
                }
            }
        }.listStyle(.plain)
    }
    
    private func filteredMessages(_ events: [String]) -> [String] {
        if searchText.isEmpty {
            return events.reversed()
        }
        
        return events.filter { $0.localizedCaseInsensitiveContains(searchText) }.reversed()
    }
    
    private func filteredOptions(_ events: [Log]) -> [Log] {
        if searchText.isEmpty {
            return events.reversed()
        }
        
        return events.filter { $0.message.localizedCaseInsensitiveContains(searchText) }.reversed()
    }
    
    private func getColor(for option: Log) -> Color {
        switch option.type {
        case .all:
            .gray
        case .sdk:
            .orange
        case .events:
            .blue
        case .errors:
            .red
        }
    }
}

#Preview {
    LogView(viewModel: .init())
}
