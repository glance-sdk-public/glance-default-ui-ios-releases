import UIKit
import WebKit
import SafariServices
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
#endif
import GlanceCore

class MaskingTestView: UIView {
    
    var viewLevel: Int = 0
    var showingWebView: Bool = false
    var subView: MaskingTestView?
    private var glanceMaskContentController: GlanceMaskContentController!
    private var maskingWebView: WKWebView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect, viewLevel: Int) {
        self.init(frame: frame)
        self.viewLevel = viewLevel
        self.backgroundColor = .white
        self.setupView()
        
    }
    
    func setupView() {
        // Create the scroll view
        let scrollView = UIScrollView(frame: .zero)
        scrollView.frame = MaskingTestView.frameWithOffsetX(0, offsetY: 0.1, widthPercentage: 1, heightPercentage: 0.5)
        scrollView.backgroundColor = .white
        scrollView.contentSize = CGSize(width: self.bounds.size.width, height: 1000)
        self.addSubview(scrollView)

        // Get the screen's width and height
        let label = UILabel(frame: .zero)
        label.text = "View Level: \(self.viewLevel)"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 0.02 * UIScreen.main.bounds.size.height)
        label.frame = MaskingTestView.frameWithOffsetX(0.25, offsetY: 0.075, widthPercentage: 0.5, heightPercentage: 0.05)
        label.textAlignment = .center
        self.addSubview(label)

        // Create a button
        let button1 = UIButton(type: .roundedRect)
        applyButtonStyle(button1)
        button1.frame = MaskingTestView.frameWithOffsetX(0.1, offsetY: 0.5, widthPercentage: 0.4, heightPercentage: 0.1)
        button1.setTitle("Show Web View", for: .normal)
        button1.addTarget(self, action: #selector(toggleWebView(_:)), for: .touchUpInside)
        self.addSubview(button1)

        let button2 = UIButton(type: .roundedRect)
        applyButtonStyle(button2)
        button2.frame = MaskingTestView.frameWithOffsetX(0.55, offsetY: 0.5, widthPercentage: 0.4, heightPercentage: 0.1)
        button2.setTitle("Unused Button", for: .normal)
            //button2.addTarget(self, action: #selector(toggleWebView(_:)), for: .touchUpInside)
        self.addSubview(button2)

        if self.viewLevel > 1 {
            let button3 = UIButton(type: .roundedRect)
            applyButtonStyle(button3)
            button3.frame = MaskingTestView.frameWithOffsetX(0.1, offsetY: 0.7, widthPercentage: 0.4, heightPercentage: 0.1)
            button3.setTitle("Remove View", for: .normal)
            button3.addTarget(self, action: #selector(removeView(_:)), for: .touchUpInside)
            self.addSubview(button3)
        }

        let button4 = UIButton(type: .roundedRect)
        applyButtonStyle(button4)
        button4.frame = MaskingTestView.frameWithOffsetX(0.55, offsetY: 0.7, widthPercentage: 0.4, heightPercentage: 0.1)
        button4.setTitle("Add View", for: .normal)
        button4.addTarget(self, action: #selector(addAnotherView(_:)), for: .touchUpInside)
        self.addSubview(button4)

        let label1 = UILabel(frame: .zero)
        label1.text = "Secret Password: Password1!"
        label1.textColor = .black
        let randomFloat = 0.05 + drand48() * 0.2
        label1.font = UIFont.systemFont(ofSize: 0.02 * UIScreen.main.bounds.size.height)
        label1.frame = MaskingTestView.frameWithOffsetX(0.1, offsetY: randomFloat + 0.1, widthPercentage: 0.8, heightPercentage: 0.05)
        label1.textAlignment = .center
        self.addSubview(label1)
        GlanceVisitor.addMaskedView(label1, withLabel: "Secret Password")

        let label2 = UILabel(frame: .zero)
        label2.text = "Social: 123-432-3093"
        label2.textColor = .black
        label2.font = UIFont.systemFont(ofSize: 0.02 * UIScreen.main.bounds.size.height)
        label2.frame = MaskingTestView.frameWithOffsetX(0.1, offsetY: randomFloat + 0.2, widthPercentage: 0.8, heightPercentage: 0.05)
        label2.textAlignment = .center
        self.addSubview(label2)
        GlanceVisitor.addMaskedView(label2, withLabel: "Social Security")

        let containerView = UIView(frame: .zero)
        containerView.frame = MaskingTestView.frameWithOffsetX(0, offsetY: 0.5, widthPercentage: 1, heightPercentage: 0.5)
        containerView.backgroundColor = .clear
        self.addSubview(containerView)
        containerView.isUserInteractionEnabled = false

        let label3 = UILabel(frame: .zero)
        label3.text = "Container View Test"
        label3.frame = MaskingTestView.frameWithOffsetX(0.1, offsetY: randomFloat + 0.1, widthPercentage: 0.8, heightPercentage: 0.05)
        containerView.addSubview(label3)
        label3.textColor = .black
        // Create the content view
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height))
        scrollView.addSubview(contentView)

        // Add labels every 100 pixels
        for i in 0...10 {
            let label = UILabel(frame: CGRect(x: 20, y: i * 100, width: Int(contentView.bounds.size.width) - 40, height: 30))
            label.text = "Scrollable Masked Label \(i + 1)"
            label.textAlignment = .center
            label.backgroundColor = .white
            label.textColor = .black
            contentView.addSubview(label)
            GlanceVisitor.addMaskedView(label, withLabel: "Scrollable Label \(i + 1)")
        }

        GlanceVisitor.addMaskedView(label3, withLabel: "Social Security")

    }
    
    @objc func removeView(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    
    @objc func addAnotherView(_ sender: UIButton) {
        let view = MaskingTestView(frame: self.bounds, viewLevel: self.viewLevel + 1)
        self.subView = view
        self.addSubview(self.subView!)
    }
    
    func applyButtonStyle(_ button: UIButton) {
        // Set button background color
        button.backgroundColor = UIColor(red: 0.2, green: 0.6, blue: 0.8, alpha: 1.0)
        
        // Set button title color
        button.setTitleColor(UIColor.white, for: .normal)
        
        // Set button corner radius
        button.layer.cornerRadius = 10.0
        
        // Set button border color and width
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 2.0
        
        // Add shadow to button
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowOpacity = 0.5
        button.layer.shadowRadius = 2.0
    }
    
    @objc func toggleWebView(_ sender: UIButton) {
        if self.showingWebView {
            self.showingWebView = false
            sender.setTitle("Show Web View", for: .normal)
            self.removeWebView()
        } else {
            self.showingWebView = true
            sender.setTitle("Hide Web View", for: .normal)
            self.addWebView()
        }
    }
    
    func addWebView() {
        // Webview Masking
            
       
            if self.maskingWebView == nil {
                let maskingQuerySelectors = [".mask_1", ".mask_2", "#mask_3", ".mask_4", "span", "#hplogo"]
                let maskingLabels = ["mask 1", "mask 2", "mask 3", "mask 4", "span", "LOGO"]
                glanceMaskContentController = GlanceMaskContentController(
                    maskingQuerySelectors.joined(separator: ", "),
                    labels: maskingLabels.joined(separator: ", ")
                )
                let config = WKWebViewConfiguration()
                config.userContentController = glanceMaskContentController

                maskingWebView = WKWebView(frame:  MaskingTestView.frameWithOffsetX(0, offsetY:0.15, widthPercentage:1.0, heightPercentage:0.25), configuration: config)
                maskingWebView.load(URLRequest(url: URL(string: "https://d2e93a2oavc15x.cloudfront.net")!))
                glanceMaskContentController.setWebView(maskingWebView)
            }
            if let maskingWebView = self.maskingWebView {
                print("ADDING WEBVIEW");
                self.addSubview(maskingWebView)
                if let glanceMaskContentController = self.glanceMaskContentController {
                    glanceMaskContentController.setWebView(maskingWebView)
                }
            }
        }

    
    
    func removeWebView() {
        self.maskingWebView?.removeFromSuperview()
    }
    
    static func frameWithOffsetX(_ offsetX: CGFloat, offsetY: CGFloat, widthPercentage: CGFloat, heightPercentage: CGFloat) -> CGRect {
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        let frameWidth = screenWidth * widthPercentage
        let frameHeight = screenHeight * heightPercentage
        let frameX = screenWidth * offsetX
        let frameY = screenHeight * offsetY
        return CGRect(x: frameX, y: frameY, width: frameWidth, height: frameHeight)
    }
}
