//
//  AppCoordinator.swift
//  glance-sample-app
//
//  Created by Felipe Melo on 14/09/23.
//

import UIKit
import SwiftUI
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
#endif

final class AppCoordinator {
    var window: UIWindow?
    var nav: UINavigationController?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        let viewModel = TestAppViewModel()
        let view = TestAppView(viewModel: viewModel, coordinator: self)
        let hostingController = UIHostingController(rootView: view)
        nav = UINavigationController(rootViewController: hostingController)
        nav?.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }
    
    func showTestWebMaskingView(_ url: String) {
        let vc = TestWebViewController(url: url)
        nav?.present(vc, animated: true)
    }
    
    func showTestMaskingView() {
        let vc = TestMaskingViewController()
        nav?.present(vc, animated: true)
    }
}
