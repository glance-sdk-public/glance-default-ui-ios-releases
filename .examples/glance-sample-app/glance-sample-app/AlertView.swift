//
//  AlertView.swift
//  
//
//  Created by Daniel Perez on 10/28/24.
//

import SwiftUI

struct AlertView: UIViewControllerRepresentable {
    var title: String
    var message: String
    var dismissAction: () -> Void

    func makeUIViewController(context: Context) -> UIViewController {
        let vc = UIViewController() // Create a blank UIViewController
        return vc
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            dismissAction()
        })
        if uiViewController.presentedViewController == nil {
            if let rootViewController = UIApplication.shared.windows.first?.rootViewController {
                rootViewController.present(alert, animated: true, completion: nil)
            }
        }
    }
}
