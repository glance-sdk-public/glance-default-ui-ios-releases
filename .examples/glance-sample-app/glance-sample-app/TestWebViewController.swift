//
//  TestWebViewController.swift
//  glance-sample-app
//
//  Created by bruno on 1/8/24.
//

import Foundation
import UIKit
import GlanceCore

final class TestWebViewController: UIViewController {
    
    private var glanceMaskContentController: GlanceMaskContentController!
    private var maskingWebView: WKWebView!
    private let url: String
    
    init(url: String) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let maskingQuerySelectors = [".mask_1", ".mask_2", "#mask_3", ".mask_4", "span", "#hplogo"]
        let maskingLabels = ["mask 1", "mask 2", "mask 3", "mask 4", "span", "LOGO"]
        glanceMaskContentController = GlanceMaskContentController(
            maskingQuerySelectors.joined(separator: ", "),
            labels: maskingLabels.joined(separator: ", ")
        )
        let config = WKWebViewConfiguration()
        config.userContentController = glanceMaskContentController

        maskingWebView = WKWebView(frame: self.view.bounds, configuration: config)
        maskingWebView.load(URLRequest(url: URL(string: url)!))
        glanceMaskContentController.setWebView(maskingWebView)
        self.view.addSubview(maskingWebView)
    }
}
