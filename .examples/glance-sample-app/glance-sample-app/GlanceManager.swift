//
//  GlanceManager.swift
//  glance-sample (iOS)
//
//  Created by Felipe Melo on 23/09/22.
//

import Foundation
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
import GlanceDefaultUI
#endif
import GlanceCore

final class GlanceManager: NSObject, ObservableObject {
    static let shared = GlanceManager()
    
    private let settings: GlanceSettings = GlanceSettings()
    
    override init() {
        super.init()
    }
    
    func setSetting(name: String, value: String) {
        settings.set(name, value: value)
    }
    
    func setVisitorParams(_ params: GlanceVisitorInitParams, maxConnectAttempts: Int = 0) {
        GlanceDefaultUI.setVisitorParams(params, maxConnectAttempts: maxConnectAttempts)
    }
    
    func setPresence(isPresenceOn: Bool, maxConnectAttempts: Int32? = nil) {
        GlanceDefaultUI.setPresence(isPresenceOn, maxConnectAttempts: maxConnectAttempts)
    }

    func startSession(key: String? = nil, videoMode: GlanceVideoMode) {
        
        GlanceDefaultUI.start(key: key, videoMode: videoMode)
    }
    
    func pausePresence(_ isPaused: Bool) {
        Glance.pausePresence(isPaused: isPaused)
    }

    func endSession() {
        GlanceDefaultUI.endSession()
    }
    
    func startHostedSession(username: String, password: String) {
        GlanceDefaultUI.startHostedSession(username: username, password: password)
    }
    
    func stopHostedSession() {
        GlanceDefaultUI.stopHostedSession()
    }
    
    func pause(_ isPaused: Bool) {
        Glance.pause(isPaused)
    }

    func togglePause() {
        Glance.togglePause()
    }
}
