//
//  TestMaskingViewController.swift
//  glance-sample-app
//
//  Created by bruno on 3/20/24.
//

import Foundation
import UIKit
#if canImport(GlanceFramework)
import GlanceFramework
#else
import GlanceSDK
#endif

final class TestMaskingViewController: UIViewController {
    let textField = UITextField()
    let label = UILabel()
    let button = UIButton()
    let button2 = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupViews()
        layoutViews()
        setupMasking()
    }
    
    private func setupViews() {
        textField.borderStyle = .roundedRect
        textField.placeholder = "This place holder should be masked"
        textField.attributedPlaceholder = NSAttributedString(string: "This place holder should be masked", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
              
        textField.backgroundColor = .white
        textField.textColor = .black
        label.text = "This label should be masked"
        label.textAlignment = .center
        label.textColor = .black
        
        button.setTitle("Must be masked", for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        button2.setTitle("Add Test Mask View", for: .normal)
        button2.backgroundColor = .systemBlue
        button2.layer.cornerRadius = 5
        button2.addTarget(self, action: #selector(addMaskView), for: .touchUpInside)
        
        view.addSubview(textField)
        
        view.addSubview(label)
        view.addSubview(button)
        view.addSubview(button2)
        
    }
    
    private func layoutViews() {
        textField.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button2.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            textField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            textField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 20),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 20),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            button2.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 20),
            button2.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button2.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func setupMasking() {
        Glance.addMaskedView(textField, withLabel: "Masked text field")
        Glance.addMaskedView(label, withLabel: "Masked label")
        Glance.addMaskedView(button, withLabel: "Masked button")
    }
    
    @objc func buttonTapped() {
        label.text = textField.text
    }
    
    @objc func addMaskView() {
        let maskingTestView = MaskingTestView(frame: self.view.bounds, viewLevel: 1)
        self.view.addSubview(maskingTestView)
    }
}
