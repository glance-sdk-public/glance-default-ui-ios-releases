//
//  FilesExistenceTests.swift
//  glance-sample-app-tests
//
//  Created by bruno on 12/5/23.
//

@testable import glance_sample_app
@testable import GlanceSDK
@testable import GlanceCore
import XCTest

final class FilesExistenceTests: XCTestCase {

    func testWebviewMaskingExists() {
        let fileManager = FileManager.default
        let path = Bundle(for: GlanceMaskContentController.self).path(forResource: "webviewMasking", ofType: "js")!
        let fileExists = fileManager.fileExists(atPath: path)

        XCTAssertTrue(fileExists,
                      "webviewMasking.js doesn't exists, you should put it inside Glance_iOS" +
                      " folder before creating GlanceCore.")
    }
    
    func testPrivacyManifestExists() {
        let fileManager = FileManager.default
        let path = Bundle(for: GlanceCore.self).path(forResource: "PrivacyInfo", ofType: "xcprivacy")!
        let fileExists = fileManager.fileExists(atPath: path)

        XCTAssertTrue(fileExists,
                      "PrivacyInfo.xcprivacy doesn't exists, you should put it inside Glance_iOS" +
                      " folder before creating GlanceCore.")
    }
}
