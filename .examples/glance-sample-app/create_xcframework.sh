xcodebuild archive \
 -scheme glance-sample-app \
 -archivePath ./glance-sample-app-Simulator.xcarchive \
 -destination "generic/platform=iOS Simulator" \
 SKIP_INSTALL=NO

xcodebuild archive \
 -scheme glance-sample-app \
 -archivePath ./glance-sample-app-Device.xcarchive \
 -destination "generic/platform=iOS" \
 SKIP_INSTALL=NO

xcodebuild -create-xcframework \
 -framework ./glance-sample-app-Device.xcarchive/Products/Library/Frameworks/GlanceSDK.framework \
 -framework ./glance-sample-app-Simulator.xcarchive/Products/Library/Frameworks/GlanceSDK.framework \
 -output ./GlanceSDK.xcframework
