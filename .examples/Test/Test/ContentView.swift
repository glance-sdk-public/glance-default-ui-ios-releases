//
//  ContentView.swift
//  Test
//
//  Created by Felipe Melo on 20/03/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Glance Custom UI")
            
            Button("Start Session") {
                GlanceManager.shared.startSession()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
