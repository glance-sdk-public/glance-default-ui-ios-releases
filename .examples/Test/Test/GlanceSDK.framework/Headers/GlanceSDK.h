//
//  GlanceSDK.h
//  Glance_iOS
//
//  Copyright © 2023 Glance Networks, Inc. All rights reserved.
//

#ifndef GlanceSDK_h
#define GlanceSDK_h



#import <Foundation/Foundation.h>

//! Project version number for GlanceDefaultUserInterface.
FOUNDATION_EXPORT double Glance_iOSVersionNumber;

//! Project version string for GlanceDefaultUserInterface.
FOUNDATION_EXPORT const unsigned char Glance_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GlanceSDK/PublicHeader.h>

#import <GlanceSDK/Glance_iOS.h>
#import <GlanceSDK/GlanceMask.h>
#import <GlanceSDK/GlanceMaskContentController.h>
#import <GlanceSDK/GlanceVideoSession.h>
#import <GlanceSDK/GlanceSDK.h>
#import <GlanceSDK/SessionUICameraManager.h>
#import <GlanceSDK/GlanceVideoSessionDevice.h>
#endif /* GlanceSDK_h */
