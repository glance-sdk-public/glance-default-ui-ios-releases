//
//  GlanceMask.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface GlanceMask : NSObject

-(id)init:(int)left top:(int)top width:(int)width height:(int)height label:(NSString*)label;

-(int)left;
-(int)top;
-(int)width;
-(int)height;
-(NSString *)label;
@end
