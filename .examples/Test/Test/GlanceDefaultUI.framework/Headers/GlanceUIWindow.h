//
//  GlanceUIWindow.h
//  GlanceMobileDemo
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlanceUIView.h>

@interface GlanceUIWindow : UIWindow
@property (readonly) GlanceUIView* glanceUIView;
-(void)show;
-(void)hide;
@end
