//
//  GlancePresenceDefaultUIWindow.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlancePresenceDefaultViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlancePresenceDefaultUIWindow : UIWindow
@property (strong) GlancePresenceDefaultViewController* glancePresenceDefaultViewController;
@end

NS_ASSUME_NONNULL_END
