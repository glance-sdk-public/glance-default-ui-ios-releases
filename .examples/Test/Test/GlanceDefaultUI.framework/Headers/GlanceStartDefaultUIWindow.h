//
//  GlanceStartDefaultUIWindow.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlanceStartDefaultViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlanceStartDefaultUIWindow : UIWindow
@property (strong) GlanceStartDefaultViewController* glanceStartDefaultViewController;
@end

NS_ASSUME_NONNULL_END
