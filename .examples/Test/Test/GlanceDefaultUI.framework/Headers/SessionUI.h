//
//  SessionUI.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//
#import <GlanceDefaultUI/SessionStartDialogUIView.h>
#import <GlanceDefaultUI/SessionUIWindow.h>
#import <GlanceDefaultUI/SessionUIView.h>
#import <GlanceDefaultUI/SessionStartDialogWindow.h>

@protocol SessionUIDelegate;
@class GlanceStartParams;
@interface SessionUI : NSObject <SessionUIViewDelegate>

@property (weak) id<SessionUIDelegate> delegate;

+(SessionUI*)sharedDefaultUI;

-(void)start:(GlanceStartParams*)sparams groupId:(int)groupId server:(NSString*)glanceServer;
-(void)showStartDialog:(GlanceStartParams*)sparams delegate:(id<SessionStartDialogUIViewDelegate>)delegate;
-(void)setStartDialogDelegate:(id<SessionStartDialogUIViewDelegate>)delegate;
-(void)hideStartDialog;
-(void)showAddVideoStartDialog;
-(void)showStartDialogWithState:(SessionStartDialogUIViewState)state;
-(void)processUserMessage:(NSDictionary*)properties;
-(void)sessionConnected:(NSString*)sessionKey;
-(void)sessionAgentConnected:(BOOL)hideDialog;
-(void)sessionAgentConnected;
-(void)sessionEnded;

-(void)pauseVisitorVideo;
-(void)unpauseVisitorVideo;

@end
