//
//  GlanceUIView.h
//  GlanceMobileDemo
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GlanceUIViewState) {
    GlanceUIViewStateHidden,
    GlanceUIViewStateSessionKey,
    GlanceUIViewStateConnecting,
    GlanceUIViewStateOpen,
    GlanceUIViewStateMinimized,
    GlanceUIViewStateVideoOpen,
    GlanceUIViewStateVideoMinimized
};

typedef NS_ENUM(NSInteger, GlanceUIViewColors) {
    GlanceUIViewCancelButtonColor,
    GlanceUIViewCircleViewColor,
    GlanceUIViewSessionLabelTextColor,
    GlanceUIViewSessionKeyBackgroundColor,
    GlanceUIViewSessionKeyLabelColor,
    GlanceUIViewCloseButtonBackgroundColor,
    GlanceUIViewBorderColor
};

@protocol GlanceUIViewDelegate
@required
-(void)glanceUIViewDidTransitionToState:(GlanceUIViewState)state;
-(void)glanceUIViewDidClose;
@end

@interface GlanceUIView : UIView
@property (nonatomic, strong) id<GlanceUIViewDelegate> delegate;
@property (readwrite) GlanceUIViewState state;
-(void)resetAgentViewer;
-(void)setAgentViewer:(UIView*)agentView;
-(void)setAgentViewer:(UIView*)agentView aspect:(BOOL)aspect transition:(BOOL)transition;
-(void)setSessionKey:(NSString*)sessionKey;
-(void)setMuted:(BOOL)muted;
-(void)setSpeakerphone:(BOOL)speakerphone;
-(void)setVoiceEnabled:(BOOL)voiceEnabled;
-(void)setVideoEnabled:(BOOL)videoEnabled;
-(void)externalTouch;
-(void)setColor:(GlanceUIViewColors)constant color:(UIColor *)color;
-(void)reset;
-(void)drawBorder;
-(void)clearBorder;
-(void)showLogo;
-(void)hideLogo;
+(UIImage*)imageNamed:(NSString*)name;
@end
