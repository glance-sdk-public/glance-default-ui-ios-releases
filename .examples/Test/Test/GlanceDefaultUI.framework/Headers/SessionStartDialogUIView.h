//
//  SessionStartDialogUIView.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
//#import "Glance_iOS.h"
@class GlanceDisplayParams;
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, SessionStartDialogUIViewState) {
    SessionStartDialogUIViewStateHidden,
    SessionStartDialogUIViewStateAppShareOnly,
    SessionStartDialogUIViewStateAppShareAndVideo,
    SessionStartDialogUIViewStateTerms,
    SessionStartDialogUIViewStateInQueue,
    SessionStartDialogUIViewStateInQueueAndCode,
    SessionStartDialogUIViewStateInQueueVideoAndCode,
    SessionStartDialogUIViewStateInSessionAddVideo,
    SessionStartDialogUIViewStateEndSession
};

@protocol SessionStartDialogUIViewDelegate
-(void)sessionStartDialogDidTapAccept:(BOOL)visitorVideoEnabled;
-(void)sessionStartDialogDidTapCancel;
-(void)sessionStartDialogDidTapTerms;
-(void)sessionStartDialogDidEndSession;
-(void)sessionStartDialogDidAddVideo:(BOOL)visitorVideoEnabled;
@end

@interface SessionStartDialogUIView : UIScrollView
@property (nonatomic, readwrite) SessionStartDialogUIViewState state;
@property(nullable,nonatomic,strong) id<SessionStartDialogUIViewDelegate> startDialogDelegate;

-(void)setSessionStartDialogDelegate:(id<SessionStartDialogUIViewDelegate>)delegate;
-(void)setVideoEnabled:(BOOL)videoEnabled;
-(void)setTermsUrl:(NSString *)termsUrl;
-(void)setSessionKey:(NSString *)sessionKey;
-(SessionStartDialogUIViewState)getState;
-(void)setParams:(GlanceDisplayParams*)params;
-(void)setAccessibilityFocus;

-(void)setCapturePreviewView:(nullable AVCaptureSession*)captureSession;
@end

NS_ASSUME_NONNULL_END
