//
//  SessionStartDialogViewController.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/SessionStartDialogUIView.h>

NS_ASSUME_NONNULL_BEGIN

@interface SessionStartDialogViewController : UIViewController
@property (strong) SessionStartDialogUIView* dialogView;
@end

NS_ASSUME_NONNULL_END
