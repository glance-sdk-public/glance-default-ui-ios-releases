//
//  SessionStartDialogWindow.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/SessionStartDialogViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SessionStartDialogWindow : UIWindow
@property (strong) SessionStartDialogViewController* sessionStartDialogViewController;
-(void)show;
-(void)hide:(void (^ __nullable)(BOOL finished))completion NS_SWIFT_DISABLE_ASYNC;
@end

NS_ASSUME_NONNULL_END
