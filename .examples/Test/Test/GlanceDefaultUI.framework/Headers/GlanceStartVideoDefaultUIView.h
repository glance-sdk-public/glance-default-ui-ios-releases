//
//  GlanceStartVideoDefaultUIView.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GlanceStartVideoDefaultUIViewColors) {
    GlanceStartVideoDefaultUIViewDialogLinkColor,
    GlanceStartVideoDefaultUIViewDialogConfirmButtonBackgroundColor,
    GlanceStartVideoDefaultUIViewDialogConfirmButtonTitleColor,
    GlanceStartVideoDefaultUIViewDialogCancelButtonBackgroundColor,
    GlanceStartVideoDefaultUIViewDialogCancelButtonTitleColor,
    GlanceStartVideoDefaultUIViewDialogTextColor
};

@protocol GlanceStartVideoDefaultUIViewDelegate
-(void)glanceStartVideoDefaultUIDidTapCancel;
-(void)glanceStartVideoDefaultUIDidTapAccept;
-(void)glanceStartVideoDefaultUIDidTapTerms;
-(BOOL)glanceStartVideoDefaultUIDisplayTerms;
@end

@interface GlanceStartVideoDefaultUIView : UIScrollView
@property(nullable,nonatomic,weak) id<GlanceStartVideoDefaultUIViewDelegate> glanceStartVideoDefaultUIDelegate;
-(void)setColor:(GlanceStartVideoDefaultUIViewColors)constant color:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
