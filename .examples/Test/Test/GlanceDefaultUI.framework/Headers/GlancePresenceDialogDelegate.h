//
//  GlancePresenceDialogDelegate.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

@protocol GlancePresenceDialogDelegate
-(void)glancePresenceDialogDidTapNo;
-(void)glancePresenceDialogDidTapYes;
-(void)glancePresenceDialogDidTapTerms;
@end
