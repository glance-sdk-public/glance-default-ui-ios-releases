//
//  GlanceDefaultUIFactory.h
//  Glance_iOS

//  Copyright © 2018 Glance Networks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlancePresenceDialogDelegate.h>
#import <GlanceDefaultUI/GlanceUIWindow.h>
#import <GlanceDefaultUI/GlancePresenceDefaultUIWindow.h>
#import <GlanceDefaultUI/GlanceStartDefaultUIWindow.h>
#import <GlanceDefaultUI/GlanceStartDefaultUIView.h>


typedef NS_ENUM(NSInteger, GlanceDefaultUIColors) {
    CancelButtonColor,
    CircleViewColor,
    SessionLabelTextColor,
    SessionKeyBackgroundColor,
    SessionKeyLabelColor,
    CloseButtonBackgroundColor,
    DialogTitleBackgroundColor,
    DialogTitleColor,
    DialogLinkColor,
    DialogConfirmButtonBackgroundColor,
    DialogConfirmButtonTitleColor,
    DialogCancelButtonBackgroundColor,
    DialogCancelButtonTitleColor,
    DialogTextColor,
    DialogMainIconColor,
    DialogIconColor,
    BorderColor
};
@protocol GlanceDefaultUIDelegate <NSObject>
@optional
-(void)glanceDefaultUIDialogAccepted;
-(void)glanceDefaultUIDialogCancelled;
@end

@interface GlanceDefaultUI : NSObject <GlanceUIViewDelegate>
@property (strong) id<GlanceDefaultUIDelegate> delegate;
+ (GlanceDefaultUI*)sharedDefaultUI;
//-(void)sessionStarted;
-(void)presenceSessionStarted;
-(void)sessionConnected:(NSString*)sessionKey;
-(void)sessionConnected:(NSString*)sessionKey visitorId:(NSString*)visitorId;
-(void)sessionAgentConnected;
-(void)sessionEnded;
-(void)error:(NSString*)title message:(NSString*)message;
-(void)setColor:(GlanceDefaultUIColors)constant color:(UIColor*)color;
-(void)showStartDialog:(id<GlanceStartDefaultUIViewDelegate>)delegate;
-(void)hideStartDialog;
-(void)showPresenceDialog:(id<GlancePresenceDialogDelegate>)delegate video:(BOOL)video;
-(void)hidePresenceDialog;
@end


