//
//  GlancePresenceDefaultUIView.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlancePresenceDialogDelegate.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GlancePresenceDefaultUIViewColors) {
    GlancePresenceDefaultUIViewDialogTitleBackgroundColor,
    GlancePresenceDefaultUIViewDialogTitleColor,
    GlancePresenceDefaultUIViewDialogLinkColor,
    GlancePresenceDefaultUIViewDialogConfirmButtonBackgroundColor,
    GlancePresenceDefaultUIViewDialogConfirmButtonTitleColor,
    GlancePresenceDefaultUIViewDialogCancelButtonBackgroundColor,
    GlancePresenceDefaultUIViewDialogCancelButtonTitleColor,
    GlancePresenceDefaultUIViewDialogTextColor
};

@interface GlancePresenceDefaultUIView : UIView
@property(nullable,nonatomic,weak) id<GlancePresenceDialogDelegate> glancePresenceDialogDelegate;
-(void)setColor:(GlancePresenceDefaultUIViewColors)constant color:(UIColor *)color;
-(void)setVideo:(BOOL)video;
@end

NS_ASSUME_NONNULL_END
