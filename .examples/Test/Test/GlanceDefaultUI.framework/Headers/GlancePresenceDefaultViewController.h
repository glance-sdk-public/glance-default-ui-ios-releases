//
//  GlancePresenceDefaultViewController.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlancePresenceDefaultUIView.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlancePresenceDefaultViewController : UIViewController
@property (strong) GlancePresenceDefaultUIView* dialogView;
@end

NS_ASSUME_NONNULL_END
