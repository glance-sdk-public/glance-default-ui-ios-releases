//
//  SessionUIWindow.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/SessionUIView.h>
#import <GlanceDefaultUI/GlanceVideoCapturePreviewView.h>

@interface SessionUIWindow : UIWindow
@property (readonly) SessionUIView* sessionUIView;
-(void)setCapturePreviewView:(GlanceVideoCapturePreviewView*)glanceVideoCapturePreviewView;
-(void)show;
-(void)hide;
@end
