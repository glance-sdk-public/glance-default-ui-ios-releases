//
//  GlanceStartDefaultUIView.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GlanceStartDefaultUIViewColors) {
    GlanceStartDefaultUIViewDialogTitleBackgroundColor,
    GlanceStartDefaultUIViewDialogTitleColor,
    GlanceStartDefaultUIViewDialogLinkColor,
    GlanceStartDefaultUIViewDialogConfirmButtonBackgroundColor,
    GlanceStartDefaultUIViewDialogConfirmButtonTitleColor,
    GlanceStartDefaultUIViewDialogCancelButtonBackgroundColor,
    GlanceStartDefaultUIViewDialogCancelButtonTitleColor,
    GlanceStartDefaultUIViewDialogTextColor,
    GlanceStartDefaultUIViewDialogMainIconColor,
    GlanceStartDefaultUIViewDialogIconColor
};

@protocol GlanceStartDefaultUIViewDelegate  <NSObject>
-(void)glanceStartDefaultUIDidTapCancel;
-(void)glanceStartDefaultUIDidTapAccept;
-(void)glanceStartDefaultUIDidTapTerms;
-(BOOL)glanceStartDefaultUIDisplayPhone;
-(BOOL)glanceStartDefaultUIDisplayHeadset;
-(BOOL)glanceStartDefaultUIDisplayDraw;
-(BOOL)glanceStartDefaultUIDisplayVideo;
-(BOOL)glanceStartDefaultUIDisplayTerms;
@end

@interface GlanceStartDefaultUIView : UIScrollView
@property(nullable,nonatomic,strong) id<GlanceStartDefaultUIViewDelegate> glanceStartDefaultUIDelegate;
-(void)setColor:(GlanceStartDefaultUIViewColors)constant color:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
