//
//  SessionUIViewController.h
//  Glance_iOS

//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/SessionUIView.h>
#import <GlanceDefaultUI/GlanceVideoCapturePreviewView.h>

@interface SessionUIViewController : UIViewController
-(id)initWithGlanceViewAndFrame:(SessionUIView*)sessionUIView frame:(CGRect)frame;
-(void)setCapturePreviewView:(GlanceVideoCapturePreviewView*)glanceVideoCapturePreviewView;
@end
