//
//  GlanceStartDefaultViewController.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlanceStartDefaultUIView.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlanceStartDefaultViewController : UIViewController
@property (strong) GlanceStartDefaultUIView* dialogView;
@end

NS_ASSUME_NONNULL_END
