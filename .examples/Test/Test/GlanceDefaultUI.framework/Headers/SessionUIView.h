//
//  SessionUIView.h
//  Glance_iOS
//
//  Copyright © 2021 Glance Networks, Inc. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <GlanceDefaultUI/GlanceVideoCapturePreviewView.h>


@class GlanceVideoCapturePreviewView;
#define UIColorFromRGB(rgbValue)                                         \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
                    green:((float)((rgbValue & 0x00FF00) >>  8)) / 255.0 \
                     blue:((float)((rgbValue & 0x0000FF) >>  0)) / 255.0 \
                    alpha:1.0]

typedef NS_ENUM(NSInteger, SessionUIViewState) {
    SessionUIViewStateHidden,
    SessionUIViewStateAppShareOnly,
    SessionUIViewStateVideoAgentOnly,
    SessionUIViewStateVideoAgentVisitor,
    SessionUIViewStateVideoFullScreen,
    SessionUIViewStateTab,
};

@protocol SessionUIViewDelegate
@required
- (void)sessionUIViewAgentVisitorViewUpdated:(BOOL)paused;
- (void)sessionUIViewToggleVideoTapped;
- (void)sessionUIViewEndSessionTapped;
- (void)sessionUIViewBackgroundBlurTapped;
- (void)sessionUIViewFlipCameraTapped;
- (void)sessionUIViewLocationUpdated:(NSString *_Nonnull)location;
- (void)sessionUIViewVisibilityUpdated:(NSString *_Nonnull)visibility;
- (void)sessionUIViewAgentVisitorSizeUpdated:(NSString *_Nonnull)size width:(int)width height:(int)height;
- (void)sessionUIViewDeviceRotated:(NSString *_Nonnull)orientation width:(int)width height:(int)height;
- (void)sessionUICameraSwitched:(NSString *_Nonnull)camera width:(int)width height:(int)height;
@end

@interface SessionUIView : UIView
@property (nonatomic, strong) id<SessionUIViewDelegate> _Nullable delegate;
@property (readwrite) SessionUIViewState state;
@property (readwrite) SessionUIViewState oldState;
@property (readonly) CGFloat padding;
- (void)externalTouch;
- (void)reset;

- (void)resetAgentViewer;
- (void)setAgentViewer:(UIView *_Nullable)agentView;
- (void)setAgentViewer:(UIView *_Nullable)agentView aspect:(BOOL)aspect transition:(BOOL)transition;
- (UIDeviceOrientation)orientation;

- (void)setCapturePreviewView:(GlanceVideoCapturePreviewView *)glanceVideoCapturePreviewView;
- (void)setCapturedImage:(nullable UIImage*)image;

- (void)pauseVisitorVideo;
- (void)unpauseVisitorVideo;
- (void)toggleVoiceMute;
- (void)toggleVoiceSpeakerphone;
- (void)toggleBackgroundBlur;
- (void)toggleFlipVisitorCamera:(BOOL)isFront;
- (void)updateLocation:(NSString *)location;
- (void)updateLocation:(NSString *)location sendUserMessage:(BOOL)sendUserMessage;
- (void)updateWidgetVisibility:(NSString *)visibility;
- (void)updateWidgetVisibility:(NSString *)visibility sendUserMessage:(BOOL)sendUserMessage;
- (void)onDragGestureEnd:(CGPoint)viewCenter screenSize:(CGSize)screenSize;
- (void)setOldStateIfNeeded;
- (void)setTabStateIfNeeded:(CGPoint)viewCenter screenSize:(CGSize)screenSize;
- (void)reportSizeChangeToAgent:(NSString *_Nonnull)size;
- (void)flipVisitorCameraIfNeeded;

// accessibility
- (void)setupAccessibility;
- (void)disableAccessibility;
@end
