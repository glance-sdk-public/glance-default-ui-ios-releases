//
//  GlanceVideoDefaultUI.h
//  Glance_iOS

//  Copyright © 2019 Glance Networks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <GlanceDefaultUI/GlanceDefaultUIFactory.h>
#import <GlanceDefaultUI/GlanceStartDefaultUIView.h>
#import <GlanceDefaultUI/GlanceUIWindow.h>
#import <GlanceDefaultUI/GlanceStartVideoDefaultUIWindow.h>


@class GlanceStartParams;
@protocol GlanceVideoDefaultUIDelegate <NSObject>
@optional
-(void)glanceVideoDefaultUIDidError:(NSError*)error;
-(void)glanceVideoDefaultUIDialogAccepted;
-(void)glanceVideoDefaultUIDialogCancelled;
@end

@interface GlanceVideoDefaultUI : NSObject <GlanceUIViewDelegate>
@property (weak) id<GlanceVideoDefaultUIDelegate> delegate;
+(GlanceVideoDefaultUI*)sharedDefaultUI;
-(void)start:(GlanceStartParams*)sparams groupId:(int)groupId server:(NSString*)glanceServer;
-(void)stop;
-(void)sessionStarted;
-(void)sessionConnected:(AVCaptureSession*)captureSession sessionKey:(NSString*)sessionKey;
-(void)sessionConnected:(NSString*)sessionKey;
-(void)sessionConnected:(NSString*)sessionKey visitorId:(NSString*)visitorId;
-(void)sessionAgentConnected;
-(void)sessionEnded;
-(void)error:(NSString*)title message:(NSString*)message;
-(void)showStartVideoDialog:(id<GlanceStartVideoDefaultUIViewDelegate>)delegate;
-(void)hideStartVideoDialog;
-(void)setColor:(GlanceDefaultUIColors)constant color:(UIColor *)color;
@end
