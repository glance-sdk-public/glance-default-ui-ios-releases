//
//  TestApp.swift
//  Test
//
//  Created by Felipe Melo on 20/03/23.
//

import SwiftUI

@main
struct TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    GlanceManager.shared.initVisitor()
                    GlanceManager.shared.setPresence(true)
                }
        }
    }
}
