//
//  GlanceManager.swift
//  glance-sample (iOS)
//
//  Created by Felipe Melo on 23/09/22.
//

import Foundation
import GlanceSDK
import GlanceDefaultUI

let GLANCE_SESSION_KEY = "9199"
let GLANCE_GROUP_ID : Int32 = 21489
let VISITOR_ID = "glanceiOS"

class GlanceManager {
    static let shared = GlanceManager()
    @Published var inSession = false

    func initVisitor(visitorId: String? = nil) {
        let params = GlanceVisitorInitParams()
        params.groupid = GLANCE_GROUP_ID
        params.visitorid = VISITOR_ID
        params.video = true
        
        GlanceDefaultUI.setVisitorParams(params)
    }

    func startRandomSession() {
        GlanceDefaultUI.start()
    }

    func startSession() {
        GlanceDefaultUI.start(key: "CustomKey")
    }

    func endSession() {
        GlanceDefaultUI.endSession()
    }

    func setPresence(_ on: Bool) {
        GlanceDefaultUI.setPresence(on)
    }

    func startVideoCall(camera : String) {
        
    }
}
