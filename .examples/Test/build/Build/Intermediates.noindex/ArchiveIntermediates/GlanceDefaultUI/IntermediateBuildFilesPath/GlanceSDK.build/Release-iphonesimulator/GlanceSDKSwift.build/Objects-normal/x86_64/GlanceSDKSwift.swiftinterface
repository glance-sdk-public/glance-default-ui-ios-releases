// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.8.1 (swiftlang-5.8.0.124.5 clang-1403.0.22.11.100)
// swift-module-flags: -target x86_64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GlanceSDKSwift
import GlanceCore
import Swift
import _Concurrency
import _StringProcessing
@objc public enum Visibility : ObjectiveC.NSInteger {
  case tab = 1
  case full = 2
  public init?(rawValue: ObjectiveC.NSInteger)
  public typealias RawValue = ObjectiveC.NSInteger
  public var rawValue: ObjectiveC.NSInteger {
    get
  }
}
@objc public enum Location : ObjectiveC.NSInteger {
  case topLeft = 1
  case topRight = 2
  case bottomLeft = 3
  case bottomRight = 4
  public init?(rawValue: ObjectiveC.NSInteger)
  public typealias RawValue = ObjectiveC.NSInteger
  public var rawValue: ObjectiveC.NSInteger {
    get
  }
}
@objc public protocol GlanceDelegate {
  @objc func sessionConnected(dismissAction: (() -> Swift.Void)?)
  @objc func presenceConnected(dismissAction: (() -> Swift.Void)?)
  @objc func receivedSessionCode(_ sessionCode: Swift.String)
  @objc func sessionAgentConnected()
  @objc func sessionEnded()
  @objc func receivedVideoCaptureImage(_ image: UIKit.UIImage)
  @objc func didStartVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didStopVideoCapture(_ captureSession: AVFoundation.AVCaptureSession)
  @objc func didPauseVisitorVideo()
  @objc func didStartAgentViewer(_ glanceAgentViewer: UIKit.UIView)
  @objc func didStopAgentViewer()
  @objc func onUpdateVisibility(to visibility: GlanceSDKSwift.Visibility)
  @objc func onUpdateLocation(to location: GlanceSDKSwift.Location)
  @objc func askForVideoSession(confirmAction: @escaping (() -> Swift.Void), cancelAction: (() -> Swift.Void)?)
}
@_inheritsConvenienceInitializers @objc final public class Glance : ObjectiveC.NSObject {
  final public var videoSession: GlanceCore.GlanceVideoSession?
  final public var previewCameraManager: GlanceCore.SessionUICameraManager?
  @objc final public var delegate: (any GlanceSDKSwift.GlanceDelegate)?
  @objc public static var shared: GlanceSDKSwift.Glance
  @objc final public var initParams: GlanceCore.GlanceVisitorInitParams?
  @objc final public var startParams: GlanceCore.GlanceStartParams?
  @objc override dynamic public init()
  public static func initVisitor(groupId: Swift.Int32, token: Swift.String, name: Swift.String, email: Swift.String, phone: Swift.String, visitorId: Swift.String)
  public static func initVisitor(params: GlanceCore.GlanceVisitorInitParams)
  public static func startSession(params: GlanceCore.GlanceStartParams)
  public static func startVideoPreview()
  public static func stopVideoPreview()
  public static func startVideoSession()
  public static func setDelegate(_ delegate: any GlanceSDKSwift.GlanceDelegate)
  public static func endSession()
  public static func setPresence(_ isPresenceOn: Swift.Bool)
  public static func send(message: Swift.String, properties: Swift.String)
  @objc final public func startCaptureSession()
  @objc final public func stopCaptureSession()
  @objc deinit
}
extension GlanceSDKSwift.Glance : GlanceCore.GlanceVisitorDelegate {
  @objc final public func glanceVisitorEvent(_ event: GlanceCore.GlanceEvent!)
}
extension GlanceSDKSwift.Glance : GlanceCore.GlanceVideoSessionDelegate {
  @objc final public func glanceVideoSessionDidStart(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidStartVideoCapture(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidConnectVideoSource(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnectVideoSource(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailToConnectVideoSource(_ session: GlanceCore.GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func sessionUICameraManagerDidCapture(_ image: UIKit.UIImage!)
  @objc final public func glanceVideoSessionDidConnectStreamer(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidDisconnnectStreamer(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStartStreaming(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionWillStopStreaming(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidFailConnectStreamer(_ session: GlanceCore.GlanceVideoSession!, error: (any Swift.Error)!)
  @objc final public func glanceVideoSessionWillChangeQuality(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionDidEnd(_ session: GlanceCore.GlanceVideoSession!)
  @objc final public func glanceVideoSessionInvitation(_ session: GlanceCore.GlanceVideoSession!, sessiontype: Swift.String!, username: Swift.String!, sessionkey sesionkey: Swift.String!)
}
extension GlanceSDKSwift.Glance : GlanceCore.SessionUICameraManagerDelegate {
  @objc final public func sessionUICameraManagerDidStartVideoCapture(_ instance: GlanceCore.SessionUICameraManager)
  @objc final public func sessionUICameraManagerDidStopVideoCapture(_ instance: GlanceCore.SessionUICameraManager)
}
extension GlanceSDKSwift.Glance : GlanceCore.SessionUIDelegate {
  @objc final public func sessionUIVoiceAuthenticationRequired()
  @objc final public func sessionUIVoiceAuthenticationFailed()
  @objc final public func sessionUIDidError(_ error: (any Swift.Error)!)
  @objc final public func sessionUIDialogAccepted()
  @objc final public func sessionUIDialogCancelled()
}
extension GlanceSDKSwift.Glance : GlanceCore.GlanceCustomViewerDelegate {
  @objc final public func glanceViewerDidStart(_ glanceView: UIKit.UIView, size: CoreFoundation.CGSize)
  @objc final public func glanceViewerDidStop(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStopping(_ glanceView: UIKit.UIView)
  @objc final public func glanceViewerIsStarting(_ glanceView: UIKit.UIView)
}
extension GlanceSDKSwift.Visibility : Swift.Equatable {}
extension GlanceSDKSwift.Visibility : Swift.Hashable {}
extension GlanceSDKSwift.Visibility : Swift.RawRepresentable {}
extension GlanceSDKSwift.Location : Swift.Equatable {}
extension GlanceSDKSwift.Location : Swift.Hashable {}
extension GlanceSDKSwift.Location : Swift.RawRepresentable {}
